version 3.0.0 08-23-2018 mp changed following attr and xsl files to get hazardstatements look like ISO standard
--------------------------------------------------------------------------------
* hazard-domain-attr.xsl
* tables-attr.xsl
* commons-attr.xsl
* commons.xsl
* hazard-domain.xsl

version 3.0.0 06-03-2018 avdh removed colored text for released modules and object-id's in DRAFT publication
-----------------------------------------------------------------------------------------------------------
changed:	Task-elements-attrs.xsl
		- disabled the attribute-set 'task'
changed:	Task-elements.xsl
		- removed call for attribute-set task
changed:  	commons.xsl
		- see <!--2018-06-08 avdh: re,oved attributeset for colored text for released topics-->

version 3.0.0 06-03-2018 avdh added colored text for released modules in DRAFT publication
-------------------------------------------------------------------------------------------
changed: 	changed convertToDita.xsl
		- added attribute �props� and get status from the metadata
changed:	Task-elements-attrs.xsl
		- Changed <xsl:attribute-set name="task">
changed: 	Commons-attrs.xsl
		- <xsl:attribute-set name="reference">
		- <xsl:attribute-set name="concept">


version 3.0.0 25-02-2018 avdh added DRAFT publication
------------------------------------------------------
changed: 	infomanager processes
added:		insertparameters.xml
changes:	plugin.xml 
		- <feature extension="dita.conductor.pdf2.param" file="insertParameters.xml"/>
changed:	publication.ini, user_text_EN.ini
changed:	RunDitaOpenToolkit2.bat
		- added DRAFTPUB
changed: 	commons.xsl
		- See: <!--avdh: 2017-12-12 publish objectid when draft publication-->
		- <!--avdh: 2017-12-12 changes to publish with pa-rameter DRAFT from TIMInfomanager -->
changed:	title-numbering.xsl
		- <!--avdh: 2017-12-12 publish objectid when draft publication-->
changed:	static-content.xsl
		- <!--   09-01-2018 avdh: add term DRAFT to the frontpage-->
		frontpage.xsl
		- <!--   09-01-2018 avdh: add term DRAFT to the frontpage-->


version 3.0.0 25-01-2018 avdh link to internal figure- or table title
----------------------------------------------------------------------
changed:    links.xsl
            - added: <xsl:template match="*[contains(@class, ' topic/title ')]" mode="retrieveReferenceTitle">
            - added: <xsl:template name="topicfig">
            - added: <xsl:template name="topictable">

version 3.0.0 24-01-2018 avdh prepare for publication of Right-to-Left languages
-------------------------------------------------------------------------------------
changed:    all occurences of reference to 'left' or 'right' changed to 'start' or 'end'

version 3.0.0 24-01-2018 avdh: added backmatter
------------------------------------------------
changed:    layout-masters.xsl

changed:    added backmatter.xsl to the plugin
            - added backmatter.xsl to custom.xml
            
changed:    added backmatter-attrs.xsl to the plugin
            - added backmatter-attrs.xsl to custom.xml
            
changed:    rootprocessing.xsl
            - added call for 'createBackMatter' only if backmattertopic exists
            
changed:    Commons.attrs.xsl
            - Added to have backpage on an even page:
            <!--last node is not backmatter-->
                <xsl:when test="not(following-sibling::*) and not(descendant::*[contains(@outputclass, 'backmatter')])">                    
                    <xsl:value-of select="'no-force'"/>
                </xsl:when>
<!--                last node is backmatter-->
                <xsl:when test="following-sibling::*[1][descendant::*[contains(@outputclass, 'backmatter')]]">
                    <xsl:value-of select="'end-on-odd'"/>
                </xsl:when> 

 
 version 3.0.0 17-01-2018 avdh: added a choice for preliminary conditions to publish as ul or ol
--------------------------------------------------------------------------------
changed:    lists.xsl
            - See: 17-01-2018 avdh
Add outputclass=�ul� to element <reqconds> in the topic

 
version 3.0.0 17-01-2018 avdh: keep.together for fig with <dl>, poslist
--------------------------------------------------
changed:    commons-attrs.xsl
            -changed attribute-set 'fig' keep-together.within-column
            
version 3.0.0 09-01-2018 avdh: safecond must be published as an unordered list
--------------------------------------------------
changed:    task-elements.xsl
            - see: 09-01-2018 avdh:
            
version 3.0.0 08-01-2018 avdh: added an option to publish object ID's when draft
------------------------------------------------------------------
changed:    [Plugin root]\insertParameters
            - Create the parameter to pass trough to the plugin-stylesheets:
            
changed:    Plugin.xml
            - Create the feature: <feature extension="dita.conductor.pdf2.param" file="insertParameters.xml"/>
            
changed:    Publication.ini
            - Create a new publication process for the DRAFTpublication
            - added processes: GLOBAL_getgraphicnr, XGEN_APP, GLOBAL_RemoveImageinfo,
            
changed:    User_text_EN.ini
            - Create menu-text item for the draft publication
            
changed:    RunDitaOpenToolkit2.bat
            - added scriptting to pass the param=DRAFTPUB to the plugin
            
changed:    Created: TIMXMLPublisher\Global\custom\getgraphicnr.xsl
            Created: TIMXMLPublisher\Global\custom\RemoveImageinfo.xsl
            
changed:    Commons.xsl
            - See: <!--avdh: 2017-12-12 publish objectid when draft publication-->
            
changed:    Static-content.xsl
            - Added a choose see: <!--   09-01-2018 avdh: add term DRAFT to the frontpage-->

changed:    Frontpage.xsl
            - Added a choose see: <!--   09-01-2018 avdh: add term DRAFT to the frontpage-->


            
 version 3.0.0 avdh: note must not break over the page
 ----------------------------------
 changed    commons-attrs.xsl
            added to attribute-set "note__table": attribute "keep-together.within-column"
            
 version 3.0.0 avdh: indentation of the seconline of title and of toc-entries
 -----------------------------------------------------------
 changed    titlenumbering.xsl
            changes the transformstion to create a list instead of inlines
 
 version 3.0.0 avdh: preliminary requirements in a mtask publish to a table
 ---------------------------------------------------------
 changed    task-elements.xsl
            see: avdh: june 2017
            
 version 3.0.0 avdh: set crossreference format; only publish the title number
 --------------------------------------------------------
 changed    links.xsl
            see: 20171004 avdh no title publish in xref
 
 
  Version 2.0.1beta 15-06-2016 Lex
 ------------------------------------------------------------------------------
    Changed xsl/titlenumbering.xsl
        - changed to use better start-indent for a nicer align in TOC

  Version 2.0.0beta 06-06-2016 Lex
 ------------------------------------------------------------------------------
 This version is in beta and already has some changes copied. for comparing the changes, the original is com.etteplan.pdf-TEST, search for lex 06-06-2016 or up
 
    Changed and added to xsl/task-elements.xsl
        - added various subtitles (! COMMENT: comment this for model a, search for prefix.title)
        - added new templates (for supequip, supplies, spares, saftey, safecond, noconds, nosupeq, nosupply, nospares, nosafety), weren't published on it's own before
        - added keep rules for lists and sublists to keep first and last with eachother
        
     Changed lists.xsl
        - added keep rules for lists and sublists to keep first and last with eachother
    
    Changed xsl/toc.xsl
        - send pagenumber with template so pagenumbering can be done in titlenumbering.xsl for the use of list-item in toc
    
    Changed xsl/commons.xsl
        - added param to send pagenumber to titlenumbering.xsl for the use of list-item in toc
        
    Changed xsl/titlenumbering.xsl
        - changed block publication to list-block to get better spacing in TOC and normal titles
        
    Changed attrs/commons-attr.xsl
        - changed spacing for few level headings, with the new structure it changed for titles
        
    Changed xsl/links.xsl
        - added links.xsl for different XREF output
        
    Added to common/vars/en-us.xml and common/vars/en.xml
        - added FigureRef and TableRef variables to these files for XREF ouput


20160411 
 Changes for OT2:
   - cfg/fo/xsl
    - commons.xsl
        Commented <xsl:apply-templates select="." mode="buildRelationships"/>

- cfg/common/vars/en.xml
changed path to icons to:
Customization/OpenTopic/common/artwork/note.eps

tim_server\TIMXMLPublisher\Global\custom
-added insert metadata:

20160408
In commons.xsl

added  as="xs:integer"
<xsl:variable name="maxCharsInShortDesc" as="xs:integer">
        <xsl:call-template name="getMaxCharsForShortdescKeep"/>
    </xsl:variable>

Pad naar attrSetReflection.xsl aangepast naar onze plugin structuur:
<xsl:call-template name="processAttrSetReflection">
                    <xsl:with-param name="attrSet" select="concat('__align__', $imageAlign)"/>
                    <!--<xsl:with-param name="path" select="'../../cfg/fo/attrs/commons-attr.xsl'"/>-->
                    <xsl:with-param name="path" select="'../../fo/attrs/commons-attr.xsl'"/>
                </xsl:call-template>

added attr-set-reflection.xsl to the plugin (attr)
added the file to custom.xsl

