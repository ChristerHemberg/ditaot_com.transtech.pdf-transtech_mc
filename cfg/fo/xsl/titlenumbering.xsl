﻿<?xml version='1.0'?>
<xsl:stylesheet xmlns:xsl="http://www.w3.org/1999/XSL/Transform"
    xmlns:fo="http://www.w3.org/1999/XSL/Format" xmlns:e="AABB"
    xmlns:opentopic="http://www.idiominc.com/opentopic" version="2.0">

    <!--   commons.xsl:  <xsl:key name="map-id" match="opentopic:map//*[@id]" use="@id"/>-->

    <xsl:variable name="e:number-levels" select="(true(), true(), true(), true())"/>

    <xsl:template match="*" mode="getTitle">
        <xsl:variable name="topic" select="ancestor-or-self::*[contains(@class, ' topic/topic ')][1]"/>
        <xsl:variable name="id" select="$topic/@id"/>
        <xsl:variable name="mapTopics" select="key('map-id', $id)"/>

        <fo:block keep-with-next.within-page="100" keep-together.within-column="100" id="{parent::node()/@id}">
  <!--          <fo:float float="start">
                <fo:block end-indent="1mm">
                    <xsl:attribute name="id">
                        <xsl:call-template name="generate-toc-id">
                            <xsl:with-param name="element" select=".."/>
                        </xsl:call-template>
                    </xsl:attribute>
                    <xsl:for-each select="$mapTopics[1]">
                        <xsl:variable name="depth" select="count(ancestor-or-self::*[contains(@class, ' map/topicref')])"/>
                        <xsl:choose>
                            <xsl:when test="parent::opentopic:map and contains(@class, ' bookmap/bookmap ')"/>
                            <xsl:when test="ancestor-or-self::*[contains(@class, ' bookmap/frontmatter ') or contains(@class, ' bookmap/backmatter ')]"/>
                            <xsl:when test="ancestor-or-self::*[contains(@class, ' bookmap/appendix ')] and $e:number-levels[$depth]">
                                <xsl:number count="topicref[contains(@class, ' map/topicref ')][ancestor-or-self::*[contains(@class, ' bookmap/appendix ')]]" format="A.1.1." level="multiple"/>
                            </xsl:when>
                            <xsl:when test="$e:number-levels[$depth]">
                                <xsl:number count="topicref[contains(@class, ' map/topicref ')][not(ancestor-or-self::*[contains(@class, ' bookmap/frontmatter ')])]" format="1.1" level="multiple"/>
                            </xsl:when>
                        </xsl:choose>
                        <xsl:if test="$depth = 1">
                            <xsl:text>.</xsl:text>
                        </xsl:if>
                    </xsl:for-each>
                </fo:block>
            </fo:float>-->
                    <fo:block>
                        <xsl:apply-templates/>
                    </fo:block>
        </fo:block>
    </xsl:template>

    <!--    added this template to control the space after titlenumber-->
    <xsl:template match="*" mode="getTOCTitle">
        <!-- avdh 2017-07-28 lex 06-06-2016: changed to recieve pagenumber with template -->
        <xsl:param name="pagenumber"/>
        <xsl:variable name="topic" select="ancestor-or-self::*[contains(@class, ' topic/topic ')][1]"/>
        <xsl:variable name="id" select="$topic/@id"/>
        <xsl:variable name="mapTopics" select="key('map-id', $id)"/>
        <!-- avdh 2017-07-28 lex 06-06-2016: changed to use list-item for better spacing in TOC -->
 
        <fo:block>
            <xsl:for-each select="$mapTopics[1]">
                <xsl:variable name="depth" select="count(ancestor-or-self::*[contains(@class, ' map/topicref')])"/>
                <xsl:choose>
                    <xsl:when test="parent::opentopic:map and contains(@class, ' bookmap/bookmap ')"/>
                    <xsl:when test="ancestor-or-self::*[contains(@class, ' bookmap/frontmatter ') or contains(@class, ' bookmap/backmatter ')]"/>
                    <xsl:when test="ancestor-or-self::*[contains(@class, ' bookmap/appendix ')] and $e:number-levels[$depth]">
                        <xsl:number count="topicref[contains(@class, ' map/topicref ')][ancestor-or-self::*[contains(@class, ' bookmap/appendix ')]]" format="A.1.1" level="multiple"/>
                    </xsl:when>
                    <xsl:when test="$e:number-levels[$depth]">
                        <xsl:number count="topicref[contains(@class, ' map/topicref ')][not(ancestor-or-self::*[contains(@class, ' bookmap/frontmatter ')])]" format="1.1" level="multiple"/>
                    </xsl:when>
                </xsl:choose>
                <xsl:if test="$depth = 1">
                    <xsl:text>.</xsl:text>
                </xsl:if>
            </xsl:for-each>
            <xsl:text> </xsl:text>
            <fo:inline>
                <xsl:apply-templates/>
            </fo:inline>
            <fo:inline xsl:use-attribute-sets="__toc__page-number">
                <fo:leader xsl:use-attribute-sets="__toc__leader"/>
                <fo:page-number-citation>
                    <xsl:attribute name="ref-id">
                        <!-- avdh 2017-07-28 lex 06-06-2016: pagenumber -->
                        <xsl:value-of select="$pagenumber"/>
                    </xsl:attribute>
                </fo:page-number-citation>
            </fo:inline>
        </fo:block>
        <!--       <fo:list-item>
            <fo:list-item-label end-indent="label-end()">
                <fo:block>
                    <fo:inline>
                        <!-\- width="28mm"-\->
                        <xsl:for-each select="$mapTopics[1]">
                            <xsl:variable name="depth" select="count(ancestor-or-self::*[contains(@class, ' map/topicref')])"/>
                            <xsl:choose>
                                <xsl:when test="parent::opentopic:map and contains(@class, ' bookmap/bookmap ')"/>
                                <xsl:when test="ancestor-or-self::*[contains(@class, ' bookmap/frontmatter ') or contains(@class, ' bookmap/backmatter ')]"/>
                                <xsl:when test="ancestor-or-self::*[contains(@class, ' bookmap/appendix ')] and $e:number-levels[$depth]">
                                    <xsl:number count="topicref[contains(@class, ' map/topicref ')][ancestor-or-self::*[contains(@class, ' bookmap/appendix ')]]" format="A.1.1" level="multiple"/>
                                </xsl:when>
                                <xsl:when test="$e:number-levels[$depth]">
                                    <xsl:number count="topicref[contains(@class, ' map/topicref ')][not(ancestor-or-self::*[contains(@class, ' bookmap/frontmatter ')])]" format="1.1" level="multiple"/>
                                </xsl:when>
                            </xsl:choose>
                        </xsl:for-each>
                        <xsl:text>&#160;</xsl:text>
                    </fo:inline>
                </fo:block>
            </fo:list-item-label>
            <fo:list-item-body text-indent="0mm">
                <!-\-  avdh 2017-07-28 lex 15-06-2016: changed to use better start-indent for a nicer align in TOC -\->
                <xsl:attribute name="start-indent">
                    <xsl:for-each select="$mapTopics[1]">
                        <xsl:variable name="depth" select="count(ancestor-or-self::*[contains(@class, ' map/topicref')])"/>
                        <xsl:choose>
                            <xsl:when test="$depth = 1">body-start() - 3mm</xsl:when>
                            <xsl:otherwise> body-start() - 5mm + <xsl:value-of select="$depth * 1"/>mm </xsl:otherwise>
                        </xsl:choose>
                    </xsl:for-each>
                </xsl:attribute>
                <!-\- end nicer toc -\->
                <fo:block>
                    <fo:inline>
                        <xsl:apply-templates/>
                    </fo:inline>
                    <fo:inline xsl:use-attribute-sets="__toc__page-number">
                        <fo:leader xsl:use-attribute-sets="__toc__leader"/>
                        <fo:page-number-citation>
                            <xsl:attribute name="ref-id">
                                <!-\- avdh 2017-07-28 lex 06-06-2016: pagenumber -\->
                                <xsl:value-of select="$pagenumber"/>
                            </xsl:attribute>
                        </fo:page-number-citation>
                    </fo:inline>
                </fo:block>
            </fo:list-item-body>
        </fo:list-item>-->
    </xsl:template>

    <xsl:attribute-set name="__fo__root" use-attribute-sets="base-font">
        <xsl:attribute name="font-family">sans-serif</xsl:attribute>
        <xsl:attribute name="xml:lang" select="translate($locale, '_', '-')"/>
        <xsl:attribute name="writing-mode" select="$writing-mode"/>
    </xsl:attribute-set>

</xsl:stylesheet>
