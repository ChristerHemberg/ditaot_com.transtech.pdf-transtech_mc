﻿<?xml version='1.0'?>


<xsl:stylesheet xmlns:xsl="http://www.w3.org/1999/XSL/Transform"
    xmlns:fo="http://www.w3.org/1999/XSL/Format" version="2.0">

    <xsl:template
        match="*[contains(@class, ' topic/linklist ')]/*[contains(@class, ' topic/title ')]">
        <fo:block xsl:use-attribute-sets="linklist.title">
            <xsl:apply-templates/>
        </fo:block>
    </xsl:template>

    <!--Lists-->
    <xsl:template match="*[contains(@class, ' topic/ul ')]">
        <xsl:choose>
            <xsl:when test="contains(@class,'taskreq-d') and count(*[contains(@class, ' topic/li ')])&lt;2">
                <!-- special handling of prelreqs lists: dont create list if only one li -->
                <xsl:apply-templates mode="taskreq"/>
            </xsl:when>
			<!-- mpu 2020-06-18: added ul handling in table -->
			<xsl:when test="ancestor::*[contains(@class, 'topic/tbody')]">
				<fo:list-block xsl:use-attribute-sets="tbody.row.entry">
					<xsl:call-template name="commonattributes"/>
					<xsl:apply-templates/>
				</fo:list-block>
			</xsl:when>
            <xsl:otherwise>
                <fo:list-block xsl:use-attribute-sets="ul">
                    <xsl:call-template name="commonattributes"/>
                    <xsl:apply-templates/>
                </fo:list-block>        
            </xsl:otherwise>
        </xsl:choose>
    </xsl:template>
    
    <xsl:template match="*[contains(@class, ' topic/ul ')]/*[contains(@class, ' topic/li ')]" mode="taskreq">
        <xsl:apply-templates/>
    </xsl:template>
    
    <xsl:template match="*[contains(@class, ' topic/ol ')]">
	<!-- mpu 2020-06-18: added ul handling in table -->
		<xsl:choose>
			<xsl:when test="ancestor::*[contains(@class, 'topic/tbody')]">
				<fo:list-block xsl:use-attribute-sets="tbody.row.entry">
					<xsl:call-template name="commonattributes"/>
					<xsl:apply-templates/>
				</fo:list-block>
			</xsl:when>
			<xsl:otherwise>
				<fo:list-block xsl:use-attribute-sets="ol">
					<xsl:call-template name="commonattributes"/>
					<xsl:apply-templates/>
				</fo:list-block>
			</xsl:otherwise>
		</xsl:choose>
    </xsl:template>

    <xsl:template match="*[contains(@class, ' topic/ul ')]/*[contains(@class, ' topic/li ')]">
        <fo:list-item xsl:use-attribute-sets="ul.li">
            <!-- 2017-11-02 avdh: lex 06-06-2016 keep rules -->
            <xsl:choose>
                <xsl:when test="descendant::*[contains(@class, ' topic/ol ') or contains(@class, ' topic/ul ')]"/>
                <xsl:when test="not(preceding-sibling::*[contains(@class, ' topic/li ')]) and not(following-sibling::*[contains(@class, ' topic/li ')])"> </xsl:when>
                <xsl:when test="not(preceding-sibling::*[contains(@class, ' topic/li ')])">
                    <xsl:attribute name="keep-with-next.within-page">500</xsl:attribute>
                </xsl:when>
                <xsl:when test="not(following-sibling::*[contains(@class, ' topic/li ')])">
                    <xsl:attribute name="keep-with-previous.within-page">500</xsl:attribute>
                </xsl:when>
            </xsl:choose>
            <!-- lex 06-06-2016 keep together when it doesn't have a child ol/ul -->
            <xsl:if
                test="not(descendant::*[contains(@class, ' topic/ol ') or contains(@class, ' topic/ul ')])">
                <xsl:attribute name="keep-together.within-column">500</xsl:attribute>
            </xsl:if>

            <fo:list-item-label xsl:use-attribute-sets="ul.li__label">
                <fo:block xsl:use-attribute-sets="ul.li__label__content">
                    <fo:inline>
                        <xsl:call-template name="commonattributes"/>
                    </fo:inline>
                    <xsl:call-template name="insertVariable">
                        <xsl:with-param name="theVariableID" select="'Unordered List bullet'"/>
                    </xsl:call-template>
                </fo:block>
            </fo:list-item-label>

            <fo:list-item-body xsl:use-attribute-sets="ul.li__body">
                <fo:block xsl:use-attribute-sets="ul.li__content">
                    <xsl:apply-templates/>
                </fo:block>
            </fo:list-item-body>

        </fo:list-item>
    </xsl:template>

    <xsl:template match="*[contains(@class, ' topic/ol ')]/*[contains(@class, ' topic/li ')]">

        <fo:list-item xsl:use-attribute-sets="ol.li">
            <!--avdh: june 2017; only the element personnel get a ordered list number, the other items in persreq get no label, only the indentation-->
            <xsl:choose>
                <xsl:when
                    test="contains(@class, 'taskreq-d/perscat') or contains(@class, 'taskreq-d/perskill') or contains(@class, 'taskreq-d/esttime')">
                    <fo:list-item-label xsl:use-attribute-sets="ol.li__label">
                        <fo:block xsl:use-attribute-sets="ol.li__label__content">
                            <fo:inline>
                                <xsl:call-template name="commonattributes"/>
                            </fo:inline>
                        </fo:block>
                    </fo:list-item-label>
                </xsl:when>
                <xsl:otherwise>
                    <!--                17-01-2018 avdh:        added choice option to output ul instead of ol-->
                    <xsl:choose>
                        <xsl:when test="ancestor::*[@outputclass = 'ul']">
                            <fo:list-item-label xsl:use-attribute-sets="ul.li__label">
                                <fo:block xsl:use-attribute-sets="ul.li__label__content">
                                    <fo:inline>
                                        <xsl:call-template name="commonattributes"/>
                                    </fo:inline>
                                    <xsl:call-template name="insertVariable">
                                        <xsl:with-param name="theVariableID"
                                            select="'Unordered List bullet'"/>
                                    </xsl:call-template>
                                </fo:block>
                            </fo:list-item-label>
                        </xsl:when>
                        <xsl:otherwise>
                            <fo:list-item-label xsl:use-attribute-sets="ol.li__label">
                                <fo:block xsl:use-attribute-sets="ol.li__label__content">
                                    <fo:inline>
                                        <xsl:call-template name="commonattributes"/>
                                    </fo:inline>
                                    <xsl:call-template name="insertVariable">
                                        <xsl:with-param name="theVariableID"
                                            select="'Ordered List Number'"/>
                                        <xsl:with-param name="theParameters">
                                            <number>
                                                <xsl:choose>
                                                  <xsl:when
                                                  test="parent::*[contains(@class, ' topic/ol ')]/parent::*[contains(@class, ' topic/li ')]/parent::*[contains(@class, ' topic/ol ')]">
                                                  <xsl:number format="a"/>
                                                  </xsl:when>
                                                  <xsl:otherwise>
                                                  <xsl:number/>
                                                  </xsl:otherwise>
                                                </xsl:choose>
                                            </number>
                                        </xsl:with-param>
                                    </xsl:call-template>
                                </fo:block>
                            </fo:list-item-label>
                        </xsl:otherwise>
                    </xsl:choose>
                </xsl:otherwise>
            </xsl:choose>
            <fo:list-item-body xsl:use-attribute-sets="ol.li__body">
                <fo:block xsl:use-attribute-sets="ol.li__content">
                    <xsl:apply-templates/>
                </fo:block>
            </fo:list-item-body>
        </fo:list-item>
    </xsl:template>

    <xsl:template match="*[contains(@class, ' topic/li ')]/*[contains(@class, ' topic/itemgroup ')]">
        <fo:block xsl:use-attribute-sets="li.itemgroup">
            <xsl:call-template name="commonattributes"/>
            <xsl:apply-templates/>
        </fo:block>
    </xsl:template>

    <xsl:template match="*[contains(@class, ' topic/sl ')]">
        <fo:list-block xsl:use-attribute-sets="sl">
            <xsl:call-template name="commonattributes"/>
            <xsl:apply-templates/>
        </fo:list-block>
    </xsl:template>

    <xsl:template match="*[contains(@class, ' topic/sl ')]/*[contains(@class, ' topic/sli ')]">
        <fo:list-item xsl:use-attribute-sets="sl.sli">
            <fo:list-item-label xsl:use-attribute-sets="sl.sli__label">
                <fo:block xsl:use-attribute-sets="sl.sli__label__content">
                    <fo:inline>
                        <xsl:call-template name="commonattributes"/>
                    </fo:inline>
                </fo:block>
            </fo:list-item-label>

            <fo:list-item-body xsl:use-attribute-sets="sl.sli__body">
                <fo:block xsl:use-attribute-sets="sl.sli__content">
                    <xsl:apply-templates/>
                </fo:block>
            </fo:list-item-body>

        </fo:list-item>
    </xsl:template>

</xsl:stylesheet>
