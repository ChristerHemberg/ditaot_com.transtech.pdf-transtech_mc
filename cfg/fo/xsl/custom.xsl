﻿<?xml version='1.0'?>
<xsl:stylesheet xmlns:xsl="http://www.w3.org/1999/XSL/Transform"
  xmlns:fo="http://www.w3.org/1999/XSL/Format" xmlns:e="AABB"
  xmlns:opentopic="http://www.idiominc.com/opentopic" version="2.0">

  <xsl:include href="commons.xsl"/>
  <xsl:include href="back-matter.xsl"/>
  <xsl:include href="../layout-masters.xsl"/>
  <xsl:include href="front-matter.xsl"/>
  <xsl:include href="static-content.xsl"/>
  <xsl:include href="root-processing.xsl"/>
  <xsl:include href="hi-domain.xsl"/>
  <xsl:include href="toc.xsl"/>
  <xsl:include href="tables.xsl"/>
  <xsl:include href="lists.xsl"/>
  <xsl:include href="links.xsl"/>
  <xsl:include href="task-elements.xsl"/>
  <!--  <xsl:include href="root-processing_axf.xsl"/>-->
  <!--  <xsl:include href="root-processing_fop.xsl"/>-->
  <!--  <xsl:include href="root-processing_xep.xsl"/>-->
  <xsl:include href="hazard-domain.xsl"/>
  <xsl:include href="ui-domain.xsl"/>
  <!--<xsl:include href="headers_footers.xsl"/>-->
  <xsl:include href="titlenumbering.xsl"/>


</xsl:stylesheet>
