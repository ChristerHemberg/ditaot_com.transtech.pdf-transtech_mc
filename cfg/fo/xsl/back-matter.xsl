﻿<?xml version="1.0" encoding="UTF-8"?>
<xsl:stylesheet xmlns:xsl="http://www.w3.org/1999/XSL/Transform"
    xmlns:fo="http://www.w3.org/1999/XSL/Format" xmlns:opentopic="http://www.idiominc.com/opentopic"
    exclude-result-prefixes="opentopic" version="2.0"
    xmlns:ditaarch="http://dita.oasis-open.org/architecture/2005/">

    <xsl:template name="createBackMatter">
        <fo:page-sequence master-reference="back-matter" format="1">
            <fo:flow flow-name="xsl-region-body">
                <xsl:variable name="backmatter"
                    select="//*[contains(@class, ' topic/topic ') and contains(@outputclass, 'backmatter')]"/>
                <xsl:choose>
                    <xsl:when test="$backmatter">
                        <xsl:variable name="newline" select="'&#xA;'"/>
                        <!--  Background image -->
                        <fo:block-container absolute-position="fixed" width="100%" height="100%">
                            <fo:block>
                                <fo:external-graphic content-width="100%" content-height="100%"
                                    src="url(Customization/OpenTopic/common/artwork/Skoda_bkcover.eps)"
                                />
                            </fo:block>
                        </fo:block-container>
                    </xsl:when>
                    <xsl:otherwise>
                        <xsl:variable name="newline" select="'&#xA;'"/>
                        <!--  Background image -->
                        <fo:block-container absolute-position="fixed" width="100%" height="100%">
                            <fo:block>
                                <fo:external-graphic content-width="100%" content-height="100%"
                                    src="url(Customization/OpenTopic/common/artwork/Skoda_bkcover.eps)"
                                />
                            </fo:block>
                        </fo:block-container>
                    </xsl:otherwise>
                </xsl:choose>
                <fo:block id="last-page"/>
            </fo:flow>
        </fo:page-sequence>
    </xsl:template>
</xsl:stylesheet>
