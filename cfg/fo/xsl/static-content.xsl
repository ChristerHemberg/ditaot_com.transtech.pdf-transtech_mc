﻿<?xml version='1.0'?>

<xsl:stylesheet xmlns:xsl="http://www.w3.org/1999/XSL/Transform"
    xmlns:fo="http://www.w3.org/1999/XSL/Format" version="2.0"
    xmlns:opentopic="http://www.idiominc.com/opentopic">

    <xsl:template name="insertBodyStaticContents">
        <xsl:call-template name="insertBodyFootnoteSeparator"/>
        <xsl:call-template name="insertBodyOddFooter">
            <xsl:with-param name="flowname">odd-body-footer</xsl:with-param>
        </xsl:call-template>
        <xsl:call-template name="insertBodyOddFooter">
            <xsl:with-param name="flowname">odd-body-blank-footer</xsl:with-param>
        </xsl:call-template>
        
        <xsl:if test="$mirror-page-margins">
            <xsl:call-template name="insertBodyEvenFooter">
                <xsl:with-param name="flowname">even-body-footer</xsl:with-param>
            </xsl:call-template>
            <xsl:call-template name="insertBodyEvenFooter">
                <xsl:with-param name="flowname">even-body-blank-footer</xsl:with-param>
            </xsl:call-template>
        </xsl:if>
        <xsl:call-template name="insertBodyOddHeader"/>
        <xsl:if test="$mirror-page-margins">
            <xsl:call-template name="insertBodyEvenHeader"/>
        </xsl:if>
        <xsl:call-template name="insertBodyFirstHeader"/>
        <xsl:call-template name="insertBodyFirstFooter"/>
        <xsl:call-template name="insertBodyLastHeader"/>
        <xsl:call-template name="insertBodyLastFooter"/>
    </xsl:template>

    <xsl:template name="insertTocStaticContents">
        <xsl:call-template name="insertTocOddFooter"/>
        <xsl:if test="$mirror-page-margins">
            <xsl:call-template name="insertTocEvenFooter"/>
        </xsl:if>
        <xsl:call-template name="insertTocOddHeader"/>
        <xsl:if test="$mirror-page-margins">
            <xsl:call-template name="insertTocEvenHeader"/>
        </xsl:if>
    </xsl:template>

    <xsl:template name="insertIndexStaticContents">
        <xsl:call-template name="insertIndexOddFooter"/>
        <xsl:if test="$mirror-page-margins">
            <xsl:call-template name="insertIndexEvenFooter"/>
        </xsl:if>
        <xsl:call-template name="insertIndexOddHeader"/>
        <xsl:if test="$mirror-page-margins">
            <xsl:call-template name="insertIndexEvenHeader"/>
        </xsl:if>
    </xsl:template>

    <xsl:template name="insertPrefaceStaticContents">
        <xsl:call-template name="insertPrefaceFootnoteSeparator"/>
        <xsl:call-template name="insertPrefaceOddFooter"/>
        <xsl:if test="$mirror-page-margins">
            <xsl:call-template name="insertPrefaceEvenFooter"/>
        </xsl:if>
        <xsl:call-template name="insertPrefaceOddHeader"/>
        <xsl:if test="$mirror-page-margins">
            <xsl:call-template name="insertPrefaceEvenHeader"/>
        </xsl:if>
        <xsl:call-template name="insertPrefaceFirstHeader"/>
        <xsl:call-template name="insertPrefaceFirstFooter"/>
        <!--        <xsl:call-template name="insertPrefaceLastHeader"/>-->
        <!--        <xsl:call-template name="insertPrefaceLastFooter"/>-->
    </xsl:template>

    <xsl:template name="insertFrontMatterStaticContents">
        <xsl:call-template name="insertFrontMatterFootnoteSeparator"/>
        <xsl:call-template name="insertFrontMatterFirstFooter"/>
        <xsl:call-template name="insertFrontMatterOddFooter"/>
        <xsl:if test="$mirror-page-margins">
            <xsl:call-template name="insertFrontMatterEvenFooter"/>
        </xsl:if>
        <!--        avdh: no header for frontpage-->
        <!--        <xsl:call-template name="insertFrontMatterOddHeader"/>-->
        <xsl:if test="$mirror-page-margins">
            <xsl:call-template name="insertFrontMatterEvenHeader"/>
        </xsl:if>
        <!--        <xsl:call-template name="insertPrefaceLastHeader"/>-->
        <!--        <xsl:call-template name="insertPrefaceLastFooter"/>-->
    </xsl:template>

    <xsl:template name="insertGlossaryStaticContents">
        <xsl:call-template name="insertGlossaryOddFooter"/>
        <xsl:if test="$mirror-page-margins">
            <xsl:call-template name="insertGlossaryEvenFooter"/>
        </xsl:if>
        <xsl:call-template name="insertGlossaryOddHeader"/>
        <xsl:if test="$mirror-page-margins">
            <xsl:call-template name="insertGlossaryEvenHeader"/>
        </xsl:if>
    </xsl:template>

    <xsl:template name="insertBodyOddHeader">
        <xsl:variable name="thisid" select="@id"/>
        <fo:static-content flow-name="odd-body-header">
            <fo:block xsl:use-attribute-sets="__toc__odd__header">
                <fo:table start-indent="20mm" end-indent="10mm" width="100%">
                    <fo:table-column width="33%"/>
                    <fo:table-column width="34%"/>
                    <fo:table-column width="33%"/>
                    <fo:table-body>
                        <fo:table-row>
                            <fo:table-cell text-align="start" display-align="after" start-indent="0mm" width="33%">
                                <fo:block keep-together.within-line="always">
                                    <fo:inline>
                                        <fo:retrieve-marker retrieve-class-name="topicdocno"/>
										<xsl:text>&#160;&#160;&#160;</xsl:text>
                                        <xsl:variable name="topicstatus">
											<!--<fo:retrieve-marker retrieve-class-name="topicstatus"/>-->
										    <!-- Get topicstatus from opentopic/topicmeta with same id as current topic: -->
                                            <xsl:value-of select="//opentopic:map/topicref[@id=$thisid]/topicmeta/othermeta[@name = 'OBJSTATUS']/@content"/>
										</xsl:variable>
										<xsl:variable name="releasestatus">
											<xsl:choose>
												<xsl:when test="$topicstatus='ARC'">Ready</xsl:when>
												<xsl:when test="$topicstatus='AIA'">Ready</xsl:when>
												<xsl:when test="$topicstatus='UG1'">Out of order</xsl:when>
												<xsl:when test="$topicstatus='UG2'">Out of order</xsl:when>
												<xsl:otherwise>Not Ready</xsl:otherwise>
											</xsl:choose>    
										</xsl:variable>
										<fo:inline>
											<xsl:call-template name="insertVariable">
												<xsl:with-param name="theVariableID" select="'Status'"/>
											</xsl:call-template>: <xsl:call-template name="insertVariable">
												<xsl:with-param name="theVariableID" select="$releasestatus"/>
											</xsl:call-template>
										</fo:inline>
                                    </fo:inline>
                                </fo:block>
								<fo:block>
									<xsl:call-template name="insertVariable">
										<xsl:with-param name="theVariableID" select="'ApprovalDate'"/>
									</xsl:call-template>: 
								    <xsl:value-of select="//opentopic:map/topicref[@id=$thisid]/topicmeta/othermeta[@name = 'RELEASED']/@content"/>
								</fo:block>
                            </fo:table-cell>
                            <fo:table-cell text-align="center" display-align="after"
                                start-indent="0mm" width="34%">
                                <fo:block>
                                    <xsl:variable name="cover" select="//*[contains(@class, ' topic/topic ') and contains(@outputclass, 'cover')][1]"/>
                                    <xsl:value-of select="$cover/*[contains(@class, ' topic/title ')][1]"/>
                                </fo:block>
                                <fo:block>
                                    <fo:inline>
                                        <fo:retrieve-marker retrieve-class-name="current-header"/>
                                        <!--<!-\-DOCUMENT TYPE-\->
                                        <xsl:variable name="doctype">
                                            <xsl:value-of select="//opentopic:map/topicmeta/metadata/othermeta[@name = 'DOCTYPE']/@content"/>
                                        </xsl:variable>
                                        <!-\- make doctype smallcase -\->
                                        <xsl:variable name="doctypeLC" select="translate($doctype, $UpperCase, $SmallCase)"/>
                                        <!-\- clean the doctype from divers chars. -\->
                                        <xsl:variable name="doctypeclean" select="translate($doctypeLC, $stopset, '')"/>
                                        <xsl:call-template name="insertVariable">
                                            <xsl:with-param name="theVariableID" select="$doctypeclean"/>
                                        </xsl:call-template>-->
                                    </fo:inline>
                                </fo:block>
                            </fo:table-cell>
                            <fo:table-cell text-align="end" display-align="after" end-indent="0mm" width="33%">
                                <fo:block>
                                    <fo:external-graphic content-height="15mm" padding-bottom="-3mm" src="url(Customization/OpenTopic/common/artwork/logo.eps)"/>
                                </fo:block>
                            </fo:table-cell>
                        </fo:table-row>
                    </fo:table-body>
                </fo:table>
            </fo:block>
        </fo:static-content>
    </xsl:template>

    <xsl:template name="insertBodyEvenHeader">
        <xsl:variable name="thisid2" select="@id"/>
        <fo:static-content flow-name="even-body-header">
            <fo:block xsl:use-attribute-sets="__body__even__header">
                <fo:table start-indent="10mm" end-indent="20mm" width="100%">
                    <fo:table-column width="33%"/>
                    <fo:table-column width="34%"/>
                    <fo:table-column width="33%"/>
                    <fo:table-body>
                        <fo:table-row>
                            <fo:table-cell text-align="start" display-align="after"
                                start-indent="0mm" width="33%">
                                <fo:block>
                                    <fo:external-graphic content-height="15mm" padding-bottom="-3mm" src="url(Customization/OpenTopic/common/artwork/logo.eps)"/>
                                </fo:block>
                            </fo:table-cell>
                            <fo:table-cell text-align="center" display-align="after"
                                end-indent="0mm" width="34%">
                                <fo:block>
                                    <xsl:variable name="cover" select="//*[contains(@class, ' topic/topic ') and contains(@outputclass, 'cover')][1]"/>
                                    <xsl:value-of select="$cover/*[contains(@class, ' topic/title ')][1]"/>
                                </fo:block>
								<fo:block>
                                    <fo:inline>
                                        <fo:retrieve-marker retrieve-class-name="current-header"/>
                                    </fo:inline>
                                </fo:block>
                            </fo:table-cell>
                            <fo:table-cell text-align="end" display-align="after" end-indent="0mm" width="33%">
                                <fo:block keep-together.within-line="always">
                                    <fo:inline>									
                                        <xsl:variable name="topicstatus">
											<!--<fo:retrieve-marker retrieve-class-name="topicstatus"/>-->
										    <!-- Get topicstatus from opentopic/topicmeta with same id as current topic: -->
                                            <xsl:value-of select="//opentopic:map/topicref[@id=$thisid2]/topicmeta/othermeta[@name = 'OBJSTATUS']/@content"/>
										</xsl:variable>
										<xsl:variable name="releasestatus">
											<xsl:choose>
												<xsl:when test="$topicstatus='ARC'">Ready</xsl:when>
												<xsl:when test="$topicstatus='AIA'">Ready</xsl:when>
												<xsl:when test="$topicstatus='UG1'">Out of order</xsl:when>
												<xsl:when test="$topicstatus='UG2'">Out of order</xsl:when>
												<xsl:otherwise>Not Ready</xsl:otherwise>
											</xsl:choose>    
										</xsl:variable>

									
										<fo:inline>
											<xsl:call-template name="insertVariable">
												<xsl:with-param name="theVariableID" select="'Status'"/>
											</xsl:call-template>: <xsl:call-template name="insertVariable">
												<xsl:with-param name="theVariableID" select="$releasestatus"/>
											</xsl:call-template>
										</fo:inline>
										<xsl:text>&#160;&#160;&#160;</xsl:text>
										<fo:retrieve-marker retrieve-class-name="topicdocno"/>
                                    </fo:inline>
                                </fo:block>
								
								<fo:block>
									<xsl:call-template name="insertVariable">
										<xsl:with-param name="theVariableID" select="'ApprovalDate'"/>
									</xsl:call-template>: 
								    <xsl:value-of select="//opentopic:map/topicref[@id=$thisid2]/topicmeta/othermeta[@name = 'RELEASED']/@content"/>
								</fo:block>

								<!--
								<fo:block>
									<xsl:call-template name="insertVariable">
										<xsl:with-param name="theVariableID" select="'ApprovalDate'"/>
									</xsl:call-template>: 
									<xsl:value-of select="opentopic:topic/topicmeta/metadata/othermeta[@name = 'RELEASED']/@content"/>
								</fo:block>
								-->
                            </fo:table-cell>
                        </fo:table-row>
                    </fo:table-body>
                </fo:table>
            </fo:block>
        </fo:static-content>
    </xsl:template>

    <xsl:template name="insertBodyFirstHeader">
        <fo:static-content flow-name="first-body-header">
            <fo:block xsl:use-attribute-sets="__body__first__header">
                <fo:table start-indent="10mm" end-indent="20mm" width="100%">
                    <fo:table-column width="33%"/>
                    <fo:table-column width="34%"/>
                    <fo:table-column width="33%"/>
                    <fo:table-body>
                        <fo:table-row>
                            <fo:table-cell text-align="start" display-align="after"
                                start-indent="0mm" width="33%">
                                <fo:block>
                                    <fo:external-graphic content-height="15mm" padding-bottom="-3mm" src="url(Customization/OpenTopic/common/artwork/logo.eps)"/>
                                </fo:block>
                            </fo:table-cell>
                            <fo:table-cell text-align="center" display-align="after"
                                start-indent="0mm" width="34%">
                                <fo:block>								
                                    <xsl:variable name="cover" select="//*[contains(@class, ' topic/topic ') and contains(@outputclass, 'cover')][1]"/>
                                    <xsl:value-of select="$cover/*[contains(@class, ' topic/title ')][1]"/>
								<!--
                                    <xsl:choose>
                                        <xsl:when
                                            test="$map/*[contains(@class, ' topic/title ')][1]">
                                            <xsl:apply-templates
                                                select="$map/*[contains(@class, ' topic/title ')][1]"
                                            />
                                        </xsl:when>
                                        <xsl:when
                                            test="$map//*[contains(@class, ' bookmap/mainbooktitle ')][1]">
                                            <xsl:apply-templates
                                                select="$map//*[contains(@class, ' bookmap/mainbooktitle ')][1]"
                                            />
                                        </xsl:when>
                                        <xsl:when test="//*[contains(@class, ' map/map ')]/@title">
                                            <xsl:value-of
                                                select="//*[contains(@class, ' map/map ')]/@title"/>
                                        </xsl:when>
                                        <xsl:otherwise>
                                            <xsl:value-of
                                                select="/descendant::*[contains(@class, ' topic/topic ')][1]/*[contains(@class, ' topic/title ')]"
                                            />
                                        </xsl:otherwise>
                                    </xsl:choose>
								-->
                                </fo:block>
                                <fo:block>
                                    <fo:inline>
                                        <!--DOCUMENT TYPE-->
                                        <xsl:variable name="doctype">
                                            <xsl:value-of select="//opentopic:map/topicmeta/metadata/othermeta[@name = 'DOCTYPE']/@content"/>
                                        </xsl:variable>
                                        <!-- make doctype smallcase -->
                                        <xsl:variable name="doctypeLC" select="translate($doctype, $UpperCase, $SmallCase)"/>
                                        <!-- clean the doctype from divers chars. -->
                                        <xsl:variable name="doctypeclean" select="translate($doctypeLC, $stopset, '')"/>
                                        <xsl:call-template name="insertVariable">
                                            <xsl:with-param name="theVariableID" select="$doctypeclean"/>
                                        </xsl:call-template>
                                    </fo:inline>
                                </fo:block>
                            </fo:table-cell>
                            <fo:table-cell text-align="end" display-align="after" end-indent="0mm"
                                width="33%">
                                <fo:block>
                                    <fo:inline>
                                        <xsl:value-of
                                            select="ancestor::*[contains(@class, ' map/map ')]/opentopic:map/topicmeta/metadata/othermeta[@name = 'DOCNR']/@content"
                                        />
                                    </fo:inline>
                                </fo:block>
                            </fo:table-cell>
                        </fo:table-row>
                    </fo:table-body>
                </fo:table>
            </fo:block>
        </fo:static-content>
    </xsl:template>

    <xsl:template name="insertBodyFirstFooter">
        <fo:static-content flow-name="first-body-footer">
            <fo:block xsl:use-attribute-sets="__body__first__footer">
                <xsl:call-template name="insertVariable">
                    <xsl:with-param name="theVariableID" select="'Body first footer'"/>
                    <xsl:with-param name="theParameters">
                        <heading>
                            <fo:inline xsl:use-attribute-sets="__body__first__footer__heading">
                                <fo:retrieve-marker retrieve-class-name="current-header"/>
                            </fo:inline>
                        </heading>
                        <pagenum>
                            <fo:inline xsl:use-attribute-sets="__body__first__footer__pagenum">
                                <fo:page-number/>
                            </fo:inline>
                        </pagenum>
                    </xsl:with-param>
                </xsl:call-template>
            </fo:block>
        </fo:static-content>
    </xsl:template>

    <xsl:template name="insertBodyLastHeader">
        <fo:static-content flow-name="last-body-header">
            <fo:block xsl:use-attribute-sets="__body__last__header"> </fo:block>
        </fo:static-content>
    </xsl:template>

    <xsl:template name="insertBodyLastFooter">
        <fo:static-content flow-name="last-body-footer">
            <fo:block xsl:use-attribute-sets="__body__last__footer"> </fo:block>
        </fo:static-content>
    </xsl:template>

    <xsl:template name="insertBodyFootnoteSeparator">
        <fo:static-content flow-name="xsl-footnote-separator">
            <fo:block>
                <fo:leader xsl:use-attribute-sets="__body__footnote__separator"/>
            </fo:block>
        </fo:static-content>
    </xsl:template>

    <xsl:template name="insertBodyOddFooter">
        <xsl:param name="flowname"/>
        <fo:static-content flow-name="{$flowname}">
            <fo:block xsl:use-attribute-sets="__body__odd__footer">
                <fo:table start-indent="20mm" end-indent="10mm" width="100%">
                    <fo:table-column width="40%"/>
                    <fo:table-column width="20%"/>
                    <fo:table-column width="40%"/>
                    <fo:table-body>
                        <fo:table-row>
                            <fo:table-cell text-align="start" start-indent="0mm" width="40%">
                                <fo:block>
                                    <xsl:call-template name="insertcopyright"/>
                                    <!--   09-01-2018 avdh: add term DRAFT to the frontpage-->
                                    <xsl:choose>
                                        <xsl:when test="$DRAFTPUB">
                                            <fo:inline> - DRAFT</fo:inline>
                                        </xsl:when>
                                    </xsl:choose>
                                </fo:block>
                            </fo:table-cell>
                            <fo:table-cell text-align="center" width="20%">
                                <xsl:if test="not(contains($flowname,'blank'))">
                                    <fo:block keep-together.within-line="always">
                                        <xsl:call-template name="insertVariable">
                                            <xsl:with-param name="theVariableID"
                                                select="'Body odd footer'"/>
                                            <xsl:with-param name="theParameters">
                                                <pagenum>
                                                    <fo:inline
                                                        xsl:use-attribute-sets="__body__odd__footer__pagenum">
                                                        <fo:page-number/>
                                                    </fo:inline>
                                                </pagenum>
                                            </xsl:with-param>
                                        </xsl:call-template>
                                        <xsl:text> / </xsl:text><fo:retrieve-marker retrieve-class-name="chapnum"/>
                                    </fo:block>
                                </xsl:if>
                            </fo:table-cell>
                            <fo:table-cell text-align="end" end-indent="0mm" width="40%">
                                <fo:block>
                                    <fo:inline>
                                        <fo:retrieve-marker retrieve-class-name="productcat1"/>
                                    </fo:inline>
                                </fo:block>
                                <fo:block>
                                    <fo:inline>
                                        <fo:retrieve-marker retrieve-class-name="productcat2"/>
                                    </fo:inline>
                                </fo:block>
                            </fo:table-cell>
                        </fo:table-row>
                    </fo:table-body>
                </fo:table>
            </fo:block>
        </fo:static-content>
    </xsl:template>

    <xsl:template name="insertBodyEvenFooter">
        <xsl:param name="flowname"/>
        <fo:static-content flow-name="{$flowname}">
            <fo:block xsl:use-attribute-sets="__body__even__footer">
                <fo:table start-indent="10mm" end-indent="20mm" width="100%">
                    <fo:table-column width="40%"/>
                    <fo:table-column width="20%"/>
                    <fo:table-column width="40%"/>
                    <fo:table-body>
                        <fo:table-row>
                            <fo:table-cell text-align="start" start-indent="0mm" width="40%">
                                <fo:block>
                                    <fo:inline>
                                        <fo:retrieve-marker retrieve-class-name="productcat1"/>
                                    </fo:inline>
                                </fo:block>
                                <fo:block>
                                    <fo:inline>
                                        <fo:retrieve-marker retrieve-class-name="productcat2"/>
                                    </fo:inline>
                                </fo:block>
                            </fo:table-cell>
                            <fo:table-cell text-align="center" start-indent="0mm" width="20%">
                                <xsl:if test="not(contains($flowname,'blank'))">
                                <fo:block keep-together.within-line="always">
                                    <xsl:variable name="thistopicid">
                                        <fo:retrieve-marker retrieve-class-name="topicid"/>
                                    </xsl:variable>
                                    <xsl:variable name="endchapid">
                                        endchap_<fo:retrieve-marker retrieve-class-name="topicid"/>
                                    </xsl:variable>
                                    <xsl:call-template name="insertVariable">
                                        <xsl:with-param name="theVariableID"
                                            select="'Body odd footer'"/>
                                        <xsl:with-param name="theParameters">
                                            <pagenum>
                                                <fo:inline
                                                    xsl:use-attribute-sets="__body__odd__footer__pagenum">
                                                    <fo:page-number/>
                                                </fo:inline>
                                            </pagenum>
                                        </xsl:with-param>
                                    </xsl:call-template>
                                    <xsl:text> / </xsl:text><fo:retrieve-marker retrieve-class-name="chapnum"/>
                                </fo:block>
                                </xsl:if>
                            </fo:table-cell>
                            <fo:table-cell text-align="end" end-indent="0mm" width="40%">
                                <fo:block>
                                    <xsl:call-template name="insertcopyright"/>
                                    <!--   09-01-2018 avdh: add term DRAFT to the frontpage-->
                                    <xsl:choose>
                                        <xsl:when test="$DRAFTPUB">
                                            <fo:inline> - DRAFT</fo:inline>
                                        </xsl:when>
                                    </xsl:choose>
                                </fo:block>
                            </fo:table-cell>
                        </fo:table-row>
                    </fo:table-body>
                </fo:table>
            </fo:block>
        </fo:static-content>
    </xsl:template>

    <xsl:template name="insertTocOddHeader">
        <fo:static-content flow-name="odd-toc-header">
            <fo:block xsl:use-attribute-sets="__toc__odd__header">
                <fo:table start-indent="20mm" end-indent="10mm" width="100%">
                    <fo:table-column width="33%"/>
                    <fo:table-column width="34%"/>
                    <fo:table-column width="33%"/>
                    <fo:table-body>
                        <fo:table-row>
                            <fo:table-cell text-align="start" display-align="after"
                                start-indent="0mm" width="33%">
                                <fo:block>
                                    <fo:inline>
                                        <xsl:value-of select="descendant::*[contains(@class, ' map/map ')]/opentopic:map/topicmeta/metadata/othermeta[@name = 'DOCNR']/@content"/> 
                                        <xsl:text> </xsl:text><xsl:call-template name="insertVariable">
                                            <xsl:with-param name="theVariableID" select="'Version'"/>
                                        </xsl:call-template><xsl:text> </xsl:text>
                                        <xsl:value-of select="descendant::*[contains(@class, ' map/map ')]/opentopic:map/topicmeta/metadata/othermeta[@name = 'VERSION']/@content"/>
                                    </fo:inline>
                                </fo:block>
                            </fo:table-cell>
                            <fo:table-cell text-align="center" display-align="after"
                                start-indent="0mm" width="34%">
                                <fo:block>
                                    <xsl:variable name="cover" select="//*[contains(@class, ' topic/topic ') and contains(@outputclass, 'cover')][1]"/>
                                    <xsl:value-of select="$cover/*[contains(@class, ' topic/title ')][1]"/>
                                  <!--  <xsl:choose>
                                        <xsl:when
                                            test="$map/*[contains(@class, ' topic/title ')][1]">
                                            <xsl:apply-templates
                                                select="$map/*[contains(@class, ' topic/title ')][1]"
                                            />
                                        </xsl:when>
                                        <xsl:when
                                            test="$map//*[contains(@class, ' bookmap/mainbooktitle ')][1]">
                                            <xsl:apply-templates
                                                select="$map//*[contains(@class, ' bookmap/mainbooktitle ')][1]"
                                            />
                                        </xsl:when>
                                        <xsl:when test="//*[contains(@class, ' map/map ')]/@title">
                                            <xsl:value-of
                                                select="//*[contains(@class, ' map/map ')]/@title"/>
                                        </xsl:when>
                                        <xsl:otherwise>
                                            <xsl:value-of
                                                select="/descendant::*[contains(@class, ' topic/topic ')][1]/*[contains(@class, ' topic/title ')]"
                                            />
                                        </xsl:otherwise>
                                    </xsl:choose>-->
                                </fo:block>
                                <fo:block>
                                    <fo:inline>
                                        <!--DOCUMENT TYPE-->
                                        <xsl:variable name="doctype">
                                            <xsl:value-of select="//opentopic:map/topicmeta/metadata/othermeta[@name = 'DOCTYPE']/@content"/>
                                        </xsl:variable>
                                        <!-- make doctype smallcase -->
                                        <xsl:variable name="doctypeLC" select="translate($doctype, $UpperCase, $SmallCase)"/>
                                        <!-- clean the doctype from divers chars. -->
                                        <xsl:variable name="doctypeclean" select="translate($doctypeLC, $stopset, '')"/>
                                        <xsl:call-template name="insertVariable">
                                            <xsl:with-param name="theVariableID" select="$doctypeclean"/>
                                        </xsl:call-template>
                                    </fo:inline>
                                </fo:block>
                            </fo:table-cell>
                            <fo:table-cell text-align="end" display-align="after" end-indent="0mm" width="33%">
                                <fo:block>
                                    <fo:external-graphic content-height="15mm" padding-bottom="-3mm" src="url(Customization/OpenTopic/common/artwork/logo.eps)"/>
                                </fo:block>
                            </fo:table-cell>
                        </fo:table-row>
                    </fo:table-body>
                </fo:table>
            </fo:block>
        </fo:static-content>
    </xsl:template>

    <xsl:template name="insertTocEvenHeader">
        <fo:static-content flow-name="even-toc-header">
            <fo:block xsl:use-attribute-sets="__body__even__header">
                <fo:table start-indent="10mm" end-indent="20mm" width="100%">
                    <fo:table-column width="33%"/>
                    <fo:table-column width="34%"/>
                    <fo:table-column width="33%"/>
                    <fo:table-body>
                        <fo:table-row>
                            <fo:table-cell text-align="start" display-align="after"
                                start-indent="0mm" width="33%">
                                <fo:block>
                                    <fo:external-graphic content-height="15mm" padding-bottom="-3mm" src="url(Customization/OpenTopic/common/artwork/logo.eps)"/>
                                </fo:block>
                            </fo:table-cell>
                            <fo:table-cell text-align="center" display-align="after"
                                end-indent="0mm" width="34%">
                                <fo:block>
                                    <xsl:variable name="cover" select="//*[contains(@class, ' topic/topic ') and contains(@outputclass, 'cover')][1]"/>
                                    <xsl:value-of select="$cover/*[contains(@class, ' topic/title ')][1]"/>
                                </fo:block>
                                <fo:block>
                                    <fo:inline>
                                        <!--DOCUMENT TYPE-->
                                        <xsl:variable name="doctype">
                                            <xsl:value-of select="//opentopic:map/topicmeta/metadata/othermeta[@name = 'DOCTYPE']/@content"/>
                                        </xsl:variable>
                                        <!-- make doctype smallcase -->
                                        <xsl:variable name="doctypeLC" select="translate($doctype, $UpperCase, $SmallCase)"/>
                                        <!-- clean the doctype from divers chars. -->
                                        <xsl:variable name="doctypeclean" select="translate($doctypeLC, $stopset, '')"/>
                                        <xsl:call-template name="insertVariable">
                                            <xsl:with-param name="theVariableID" select="$doctypeclean"/>
                                        </xsl:call-template>
                                    </fo:inline>
                                </fo:block>
                            </fo:table-cell>
                            <fo:table-cell text-align="end" display-align="after" end-indent="0mm"
                                width="33%">
                                <fo:block>
                                    <fo:inline>
                                        <xsl:value-of select="descendant::*[contains(@class, ' map/map ')]/opentopic:map/topicmeta/metadata/othermeta[@name = 'DOCNR']/@content"/> 
                                        <xsl:text> </xsl:text><xsl:call-template name="insertVariable">
                                            <xsl:with-param name="theVariableID" select="'Version'"/>
                                        </xsl:call-template><xsl:text> </xsl:text>
                                        <xsl:value-of select="descendant::*[contains(@class, ' map/map ')]/opentopic:map/topicmeta/metadata/othermeta[@name = 'VERSION']/@content"/>
                                    </fo:inline>
                                </fo:block>
                            </fo:table-cell>
                        </fo:table-row>
                    </fo:table-body>
                </fo:table>
            </fo:block>
        </fo:static-content>
    </xsl:template>

    <xsl:template name="insertTocOddFooter">
        <fo:static-content flow-name="odd-toc-footer">
            <fo:block xsl:use-attribute-sets="__toc__odd__footer">
                <fo:table start-indent="20mm" end-indent="10mm" width="100%">
                    <fo:table-column width="40%"/>
                    <fo:table-column width="20%"/>
                    <fo:table-column width="40%"/>
                    <fo:table-body>
                        <fo:table-row>
                            <fo:table-cell text-align="start" start-indent="0mm" width="40%">
                                <fo:block keep-together.within-line="always">
                                    <xsl:call-template name="insertcopyright"/>
                                </fo:block>
                            </fo:table-cell>
                            <fo:table-cell text-align="center" display-align="after" start-indent="0mm" end-indent="0mm" width="20%">
                                <fo:block>
                                    <xsl:call-template name="insertVariable">
                                        <xsl:with-param name="theVariableID"
                                            select="'Body odd footer'"/>
                                        <xsl:with-param name="theParameters">
                                            <pagenum>
                                                <fo:inline
                                                    xsl:use-attribute-sets="__body__odd__footer__pagenum">
                                                    <fo:page-number/>
                                                </fo:inline>
                                            </pagenum>
                                        </xsl:with-param>
                                    </xsl:call-template>
                                    <xsl:text> / </xsl:text><fo:page-number-citation ref-id="endchap"/>
                                </fo:block>
                            </fo:table-cell>
                            <fo:table-cell text-align="end" end-indent="0mm" width="40%">
                                <fo:block/>
                              </fo:table-cell>
                        </fo:table-row>
                    </fo:table-body>
                </fo:table>
            </fo:block>
        </fo:static-content>
    </xsl:template>

    <xsl:template name="insertTocEvenFooter">
        <fo:static-content flow-name="even-toc-footer">
        <fo:block xsl:use-attribute-sets="__body__even__footer">
            <fo:table start-indent="20mm" end-indent="10mm" width="100%">
                <fo:table-column width="33%"/>
                <fo:table-column width="34%"/>
                <fo:table-column width="33%"/>
                <fo:table-body>
                    <fo:table-row>
                        <fo:table-cell text-align="start" start-indent="0mm" width="33%">
                            <fo:block>
                                <!--<fo:inline>Text aligned to left</fo:inline>-->
                            </fo:block>
                        </fo:table-cell>
                        <fo:table-cell text-align="center" width="34%" start-indent="0mm">
                            <fo:block>
                                <xsl:call-template name="insertVariable">
                                    <xsl:with-param name="theVariableID"
                                        select="'Body odd footer'"/>
                                    <xsl:with-param name="theParameters">
                                        <pagenum>
                                            <fo:inline
                                                xsl:use-attribute-sets="__body__odd__footer__pagenum">
                                                <fo:page-number/>
                                            </fo:inline>
                                        </pagenum>
                                    </xsl:with-param>
                                </xsl:call-template>
                                <xsl:text> / </xsl:text><fo:page-number-citation ref-id="last-page"/>
                            </fo:block>
                        </fo:table-cell>
                        <fo:table-cell text-align="end" width="33%">
                            <!--end-indent="0mm"-->
                            <fo:block keep-together.within-line="always">
                                <xsl:call-template name="insertcopyright"/>
                            </fo:block>
                        </fo:table-cell>
                    </fo:table-row>
                </fo:table-body>
            </fo:table>
        </fo:block>
        </fo:static-content>
        </xsl:template>

    <xsl:template name="insertIndexOddHeader">
        <fo:static-content flow-name="odd-index-header">
            <fo:block xsl:use-attribute-sets="__index__odd__header">
                <fo:table start-indent="20mm" end-indent="10mm" width="100%">
                    <fo:table-column width="33%"/>
                    <fo:table-column width="34%"/>
                    <fo:table-column width="33%"/>
                    <fo:table-body>
                        <fo:table-row>
                            <fo:table-cell text-align="start" display-align="after"
                                start-indent="0mm" width="33%">
                                <fo:block>
                                    <fo:inline>
                                        <xsl:value-of
                                            select="descendant::*[contains(@class, ' map/map ')]/opentopic:map/topicmeta/metadata/othermeta[@name = 'DOCNR']/@content"
                                        />
                                    </fo:inline>
                                </fo:block>
                            </fo:table-cell>
                            <fo:table-cell text-align="center" display-align="after"
                                start-indent="0mm" width="34%">
                                <fo:block>
                                    <xsl:choose>
                                        <xsl:when
                                            test="$map/*[contains(@class, ' topic/title ')][1]">
                                            <xsl:apply-templates
                                                select="$map/*[contains(@class, ' topic/title ')][1]"
                                            />
                                        </xsl:when>
                                        <xsl:when
                                            test="$map//*[contains(@class, ' bookmap/mainbooktitle ')][1]">
                                            <xsl:apply-templates
                                                select="$map//*[contains(@class, ' bookmap/mainbooktitle ')][1]"
                                            />
                                        </xsl:when>
                                        <xsl:when test="//*[contains(@class, ' map/map ')]/@title">
                                            <xsl:value-of
                                                select="//*[contains(@class, ' map/map ')]/@title"/>
                                        </xsl:when>
                                        <xsl:otherwise>
                                            <xsl:value-of
                                                select="/descendant::*[contains(@class, ' topic/topic ')][1]/*[contains(@class, ' topic/title ')]"
                                            />
                                        </xsl:otherwise>
                                    </xsl:choose>
                                </fo:block>
                                <fo:block>
                                    <fo:inline>
                                        <xsl:value-of
                                            select="descendant::*[contains(@class, ' map/map ')]/opentopic:map/topicmeta/metadata/othermeta[@name = 'DOCTYPE']/@content"
                                        />
                                    </fo:inline>
                                </fo:block>
                            </fo:table-cell>
                            <fo:table-cell text-align="end" display-align="after" end-indent="0mm"
                                width="33%">
                                <fo:block>
                                    <fo:external-graphic content-height="15mm" padding-bottom="-3mm" src="url(Customization/OpenTopic/common/artwork/logo.eps)"/>
                                </fo:block>
                            </fo:table-cell>
                        </fo:table-row>
                    </fo:table-body>
                </fo:table>
            </fo:block>
        </fo:static-content>
    </xsl:template>

    <xsl:template name="insertIndexEvenHeader">
        <fo:static-content flow-name="even-index-header">
            <fo:block xsl:use-attribute-sets="__index__even__header">
                <fo:table start-indent="10mm" end-indent="20mm" width="100%">
                    <fo:table-column width="33%"/>
                    <fo:table-column width="34%"/>
                    <fo:table-column width="33%"/>
                    <fo:table-body>
                        <fo:table-row>
                            <fo:table-cell text-align="start" display-align="after"
                                start-indent="0mm" width="33%">
                                <fo:block>
                                    <fo:external-graphic content-height="15mm" padding-bottom="-3mm" src="url(Customization/OpenTopic/common/artwork/logo.eps)"/>
                                </fo:block>
                            </fo:table-cell>
                            <fo:table-cell text-align="center" display-align="after"
                                end-indent="0mm" width="34%">
                                <fo:block>
                                    <xsl:choose>
                                        <xsl:when
                                            test="$map/*[contains(@class, ' topic/title ')][1]">
                                            <xsl:apply-templates
                                                select="$map/*[contains(@class, ' topic/title ')][1]"
                                            />
                                        </xsl:when>
                                        <xsl:when
                                            test="$map//*[contains(@class, ' bookmap/mainbooktitle ')][1]">
                                            <xsl:apply-templates
                                                select="$map//*[contains(@class, ' bookmap/mainbooktitle ')][1]"
                                            />
                                        </xsl:when>
                                        <xsl:when test="//*[contains(@class, ' map/map ')]/@title">
                                            <xsl:value-of
                                                select="//*[contains(@class, ' map/map ')]/@title"/>
                                        </xsl:when>
                                        <xsl:otherwise>
                                            <xsl:value-of
                                                select="/descendant::*[contains(@class, ' topic/topic ')][1]/*[contains(@class, ' topic/title ')]"
                                            />
                                        </xsl:otherwise>
                                    </xsl:choose>
                                </fo:block>
                                <fo:block>
                                    <fo:inline>
                                        <xsl:value-of
                                            select="descendant::*[contains(@class, ' map/map ')]/opentopic:map/topicmeta/metadata/othermeta[@name = 'DOCTYPE']/@content"
                                        />
                                    </fo:inline>
                                </fo:block>
                            </fo:table-cell>
                            <fo:table-cell text-align="end" display-align="after" end-indent="0mm"
                                width="33%">
                                <fo:block>
                                    <fo:inline>
                                        <xsl:value-of
                                            select="descendant::*[contains(@class, ' map/map ')]/opentopic:map/topicmeta/metadata/othermeta[@name = 'DOCNR']/@content"
                                        />
                                    </fo:inline>
                                </fo:block>
                            </fo:table-cell>
                        </fo:table-row>
                    </fo:table-body>
                </fo:table>
            </fo:block>
        </fo:static-content>
    </xsl:template>

    <xsl:template name="insertIndexOddFooter">
        <fo:static-content flow-name="odd-index-footer">
            <fo:block xsl:use-attribute-sets="__index__odd__footer">
                <fo:table start-indent="20mm" end-indent="20mm" width="100%">
                    <fo:table-column width="50%"/>
                    <fo:table-column width="50%"/>
                    <fo:table-body>
                        <fo:table-row>
                            <fo:table-cell text-align="start" start-indent="0mm">
                                <fo:block>
                                    <fo:inline>
                                        <xsl:value-of
                                            select="ancestor::*[contains(@class, ' map/map ')]/opentopic:map/topicmeta/metadata/othermeta[@name = 'OBJECTNR']/@content"
                                        />
                                    </fo:inline>
                                    <fo:inline> - </fo:inline>
                                    <fo:inline>
                                        <xsl:value-of
                                            select="ancestor::*[contains(@class, ' map/map ')]/opentopic:map/topicmeta/metadata/othermeta[@name = 'VERSION']/@content"
                                        />
                                    </fo:inline>
                                    <fo:inline> - </fo:inline>
                                    <fo:inline>
                                        <xsl:value-of
                                            select="ancestor::*[contains(@class, ' map/map ')]/opentopic:map/topicmeta/metadata/othermeta[@name = 'RELEASED']/@content"
                                        />
                                    </fo:inline>
                                    <!--   09-01-2018 avdh: add term DRAFT to the frontpage-->
                                    <xsl:choose>
                                        <xsl:when test="$DRAFTPUB">
                                            <fo:inline> - DRAFT</fo:inline>
                                        </xsl:when>
                                    </xsl:choose>
                                </fo:block>
                            </fo:table-cell>
                            <fo:table-cell text-align="end" end-indent="0mm">
                                <fo:block>
                                    <xsl:call-template name="insertVariable">
                                        <xsl:with-param name="theVariableID"
                                            select="'Body odd footer'"/>
                                        <xsl:with-param name="theParameters">
                                            <pagenum>
                                                <fo:inline
                                                  xsl:use-attribute-sets="__body__odd__footer__pagenum">
                                                  <fo:page-number/>
                                                </fo:inline>
                                            </pagenum>
                                        </xsl:with-param>
                                    </xsl:call-template>
                                </fo:block>
                            </fo:table-cell>
                        </fo:table-row>
                    </fo:table-body>
                </fo:table>
            </fo:block>
        </fo:static-content>
    </xsl:template>

    <xsl:template name="insertIndexEvenFooter">
        <fo:static-content flow-name="even-index-footer">
            <fo:block xsl:use-attribute-sets="__index__even__footer">
                <fo:table start-indent="20mm" end-indent="20mm" width="100%">
                    <fo:table-column width="50%"/>
                    <fo:table-column width="50%"/>
                    <fo:table-body>
                        <fo:table-row>
                            <fo:table-cell text-align="start" start-indent="0mm">
                                <fo:block>
                                    <xsl:call-template name="insertVariable">
                                        <xsl:with-param name="theVariableID"
                                            select="'Body odd footer'"/>
                                        <xsl:with-param name="theParameters">
                                            <pagenum>
                                                <fo:inline
                                                  xsl:use-attribute-sets="__body__odd__footer__pagenum">
                                                  <fo:page-number/>
                                                </fo:inline>
                                            </pagenum>
                                        </xsl:with-param>
                                    </xsl:call-template>
                                </fo:block>
                            </fo:table-cell>
                            <fo:table-cell text-align="end" end-indent="0mm">
                                <fo:block>
                                    <fo:inline>
                                        <xsl:value-of
                                            select="ancestor::*[contains(@class, ' map/map ')]/opentopic:map/topicmeta/metadata/othermeta[@name = 'OBJECTNR']/@content"
                                        />
                                    </fo:inline>
                                    <fo:inline> - </fo:inline>
                                    <fo:inline>
                                        <xsl:value-of
                                            select="ancestor::*[contains(@class, ' map/map ')]/opentopic:map/topicmeta/metadata/othermeta[@name = 'VERSION']/@content"
                                        />
                                    </fo:inline>
                                    <fo:inline> - </fo:inline>
                                    <fo:inline>
                                        <xsl:value-of
                                            select="ancestor::*[contains(@class, ' map/map ')]/opentopic:map/topicmeta/metadata/othermeta[@name = 'RELEASED']/@content"
                                        />
                                    </fo:inline>
                                    <!--   09-01-2018 avdh: add term DRAFT to the frontpage-->
                                    <xsl:choose>
                                        <xsl:when test="$DRAFTPUB">
                                            <fo:inline> - DRAFT</fo:inline>
                                        </xsl:when>
                                    </xsl:choose>
                                </fo:block>
                            </fo:table-cell>
                        </fo:table-row>
                    </fo:table-body>
                </fo:table>
            </fo:block>
        </fo:static-content>
    </xsl:template>

    <xsl:template name="insertPrefaceOddHeader">
        <fo:static-content flow-name="odd-body-header">
            <fo:block xsl:use-attribute-sets="__body__odd__header">
                <fo:table start-indent="20mm" end-indent="10mm" width="100%">
                    <fo:table-column width="33%"/>
                    <fo:table-column width="34%"/>
                    <fo:table-column width="33%"/>
                    <fo:table-body>
                        <fo:table-row>
                            <fo:table-cell text-align="start" display-align="after"
                                start-indent="0mm" width="33%">
                                <fo:block>
                                    <fo:inline>
                                        <xsl:value-of select="ancestor::*[contains(@class, ' map/map ')]/opentopic:map/topicmeta/metadata/othermeta[@name = 'DOCNR']/@content"/> 
                                        <xsl:text> </xsl:text><xsl:call-template name="insertVariable">
                                            <xsl:with-param name="theVariableID" select="'Version'"/>
                                        </xsl:call-template><xsl:text> </xsl:text>
                                        <xsl:value-of select="ancestor::*[contains(@class, ' map/map ')]/opentopic:map/topicmeta/metadata/othermeta[@name = 'VERSION']/@content"/>
                                        <!--                                        <xsl:value-of select="descendant::*[contains(@class, ' map/map ')]/opentopic:map/topicmeta/metadata/othermeta[@name = 'DOCNR']/@content"/>-->
                                    </fo:inline>
                                </fo:block>
                            </fo:table-cell>
                            <fo:table-cell text-align="center" display-align="after"
                                start-indent="0mm" width="34%">
                                <fo:block>
                                    <xsl:variable name="cover" select="//*[contains(@class, ' topic/topic ') and contains(@outputclass, 'cover')][1]"/>
                                    <xsl:value-of select="$cover/*[contains(@class, ' topic/title ')][1]"/>
                                    <!--  <xsl:choose>
                                        <xsl:when
                                            test="$map/*[contains(@class, ' topic/title ')][1]">
                                            <xsl:apply-templates
                                                select="$map/*[contains(@class, ' topic/title ')][1]"
                                            />
                                        </xsl:when>
                                        <xsl:when
                                            test="$map//*[contains(@class, ' bookmap/mainbooktitle ')][1]">
                                            <xsl:apply-templates
                                                select="$map//*[contains(@class, ' bookmap/mainbooktitle ')][1]"
                                            />
                                        </xsl:when>
                                        <xsl:when test="//*[contains(@class, ' map/map ')]/@title">
                                            <xsl:value-of
                                                select="//*[contains(@class, ' map/map ')]/@title"/>
                                        </xsl:when>
                                        <xsl:otherwise>
                                            <xsl:value-of
                                                select="/descendant::*[contains(@class, ' topic/topic ')][1]/*[contains(@class, ' topic/title ')]"
                                            />
                                        </xsl:otherwise>
                                    </xsl:choose>-->
                                </fo:block>
                                <fo:block>
                                    <fo:inline>
                                        <!--DOCUMENT TYPE-->
                                        <xsl:variable name="doctype">
                                            <xsl:value-of select="//opentopic:map/topicmeta/metadata/othermeta[@name = 'DOCTYPE']/@content"/>
                                        </xsl:variable>
                                        <!-- make doctype smallcase -->
                                        <xsl:variable name="doctypeLC" select="translate($doctype, $UpperCase, $SmallCase)"/>
                                        <!-- clean the doctype from divers chars. -->
                                        <xsl:variable name="doctypeclean" select="translate($doctypeLC, $stopset, '')"/>
                                        <xsl:call-template name="insertVariable">
                                            <xsl:with-param name="theVariableID" select="$doctypeclean"/>
                                        </xsl:call-template>
                                    </fo:inline>
                                </fo:block>
                            </fo:table-cell>
                            <fo:table-cell text-align="end" display-align="after" end-indent="0mm" width="33%">
                                <fo:block>
                                    <fo:external-graphic content-height="15mm" padding-bottom="-3mm" src="url(Customization/OpenTopic/common/artwork/logo.eps)"/>
                                </fo:block>
                            </fo:table-cell>
                        </fo:table-row>
                    </fo:table-body>
                </fo:table>
            </fo:block>
        </fo:static-content>
    </xsl:template>

    <xsl:template name="insertPrefaceEvenHeader">
        <fo:static-content flow-name="even-body-header">
            <fo:block xsl:use-attribute-sets="__body__even__header">
                <fo:table start-indent="10mm" end-indent="20mm" width="100%">
                    <fo:table-column width="33%"/>
                    <fo:table-column width="34%"/>
                    <fo:table-column width="33%"/>
                    <fo:table-body>
                        <fo:table-row>
                            <fo:table-cell text-align="start" display-align="after"
                                start-indent="0mm" width="33%">
                                <fo:block>
                                    <fo:external-graphic content-height="15mm" padding-bottom="-3mm" src="url(Customization/OpenTopic/common/artwork/logo.eps)"/>
                                </fo:block>
                            </fo:table-cell>
                            <fo:table-cell text-align="center" display-align="after"
                                end-indent="0mm" width="34%">
                                <fo:block>
                                    <xsl:variable name="cover" select="//*[contains(@class, ' topic/topic ') and contains(@outputclass, 'cover')][1]"/>
                                    <xsl:value-of select="$cover/*[contains(@class, ' topic/title ')][1]"/>								
								<!--
                                    <xsl:choose>
                                        <xsl:when
                                            test="$map/*[contains(@class, ' topic/title ')][1]">
                                            <xsl:apply-templates
                                                select="$map/*[contains(@class, ' topic/title ')][1]"
                                            />
                                        </xsl:when>
                                        <xsl:when
                                            test="$map//*[contains(@class, ' bookmap/mainbooktitle ')][1]">
                                            <xsl:apply-templates
                                                select="$map//*[contains(@class, ' bookmap/mainbooktitle ')][1]"
                                            />
                                        </xsl:when>
                                        <xsl:when test="//*[contains(@class, ' map/map ')]/@title">
                                            <xsl:value-of
                                                select="//*[contains(@class, ' map/map ')]/@title"/>
                                        </xsl:when>
                                        <xsl:otherwise>
                                            <xsl:value-of
                                                select="/descendant::*[contains(@class, ' topic/topic ')][1]/*[contains(@class, ' topic/title ')]"
                                            />
                                        </xsl:otherwise>
                                    </xsl:choose>
								-->
                                </fo:block>
								<fo:block>
                                    <fo:inline>
                                        <!--DOCUMENT TYPE-->
                                        <xsl:variable name="doctype">
                                            <xsl:value-of select="//opentopic:map/topicmeta/metadata/othermeta[@name = 'DOCTYPE']/@content"/>
                                        </xsl:variable>
                                        <!-- make doctype smallcase -->
                                        <xsl:variable name="doctypeLC" select="translate($doctype, $UpperCase, $SmallCase)"/>
                                        <!-- clean the doctype from divers chars. -->
                                        <xsl:variable name="doctypeclean" select="translate($doctypeLC, $stopset, '')"/>
                                        <xsl:call-template name="insertVariable">
                                            <xsl:with-param name="theVariableID" select="$doctypeclean"/>
                                        </xsl:call-template>
                                    </fo:inline>
                                </fo:block>
                            </fo:table-cell>
                            <fo:table-cell text-align="end" display-align="after" end-indent="0mm"
                                width="33%">
                                <fo:block>
                                    <fo:inline>
                                        <xsl:value-of
                                            select="ancestor::*[contains(@class, ' map/map ')]/opentopic:map/topicmeta/metadata/othermeta[@name = 'DOCNR']/@content"
                                        />
                                    </fo:inline>
                                </fo:block>
                            </fo:table-cell>
                        </fo:table-row>
                    </fo:table-body>
                </fo:table>
            </fo:block>
        </fo:static-content>
    </xsl:template>

    <xsl:template name="insertPrefaceFirstHeader">
        <fo:static-content flow-name="first-body-header">
            <fo:block xsl:use-attribute-sets="__body__first__header">
                <fo:table start-indent="10mm" end-indent="20mm" width="100%">
                    <fo:table-column width="33%"/>
                    <fo:table-column width="34%"/>
                    <fo:table-column width="33%"/>
                    <fo:table-body>
                        <fo:table-row>
                            <fo:table-cell text-align="start" display-align="after"
                                start-indent="0mm" width="33%">
                                <fo:block>
                                    <fo:inline>
                                        <xsl:value-of select="ancestor::*[contains(@class, ' map/map ')]/opentopic:map/topicmeta/metadata/othermeta[@name = 'DOCNR']/@content"/> 
                                        <xsl:text> </xsl:text><xsl:call-template name="insertVariable">
                                            <xsl:with-param name="theVariableID" select="'Version'"/>
                                        </xsl:call-template><xsl:text> </xsl:text>
                                        <xsl:value-of select="ancestor::*[contains(@class, ' map/map ')]/opentopic:map/topicmeta/metadata/othermeta[@name = 'VERSION']/@content"/>
                                        <!--                                        <xsl:value-of select="descendant::*[contains(@class, ' map/map ')]/opentopic:map/topicmeta/metadata/othermeta[@name = 'DOCNR']/@content"/>-->
                                    </fo:inline>
                                </fo:block>
                            </fo:table-cell>
                            <fo:table-cell text-align="center" display-align="after"
                                start-indent="0mm" width="34%">
                                <fo:block>
                                    <xsl:variable name="cover" select="//*[contains(@class, ' topic/topic ') and contains(@outputclass, 'cover')][1]"/>
                                    <xsl:value-of select="$cover/*[contains(@class, ' topic/title ')][1]"/>
                                    <!--  <xsl:choose>
                                        <xsl:when
                                            test="$map/*[contains(@class, ' topic/title ')][1]">
                                            <xsl:apply-templates
                                                select="$map/*[contains(@class, ' topic/title ')][1]"
                                            />
                                        </xsl:when>
                                        <xsl:when
                                            test="$map//*[contains(@class, ' bookmap/mainbooktitle ')][1]">
                                            <xsl:apply-templates
                                                select="$map//*[contains(@class, ' bookmap/mainbooktitle ')][1]"
                                            />
                                        </xsl:when>
                                        <xsl:when test="//*[contains(@class, ' map/map ')]/@title">
                                            <xsl:value-of
                                                select="//*[contains(@class, ' map/map ')]/@title"/>
                                        </xsl:when>
                                        <xsl:otherwise>
                                            <xsl:value-of
                                                select="/descendant::*[contains(@class, ' topic/topic ')][1]/*[contains(@class, ' topic/title ')]"
                                            />
                                        </xsl:otherwise>
                                    </xsl:choose>-->
                                </fo:block>
                                <fo:block>
                                    <fo:inline>
                                        <!--DOCUMENT TYPE-->
                                        <xsl:variable name="doctype">
                                            <xsl:value-of select="//opentopic:map/topicmeta/metadata/othermeta[@name = 'DOCTYPE']/@content"/>
                                        </xsl:variable>
                                        <!-- make doctype smallcase -->
                                        <xsl:variable name="doctypeLC" select="translate($doctype, $UpperCase, $SmallCase)"/>
                                        <!-- clean the doctype from divers chars. -->
                                        <xsl:variable name="doctypeclean" select="translate($doctypeLC, $stopset, '')"/>
                                        <xsl:call-template name="insertVariable">
                                            <xsl:with-param name="theVariableID" select="$doctypeclean"/>
                                        </xsl:call-template>
                                    </fo:inline>
                                </fo:block>
                            </fo:table-cell>
                            <fo:table-cell text-align="end" display-align="after" end-indent="0mm" width="33%">
                                <fo:block>
                                    <fo:external-graphic content-height="15mm" padding-bottom="-3mm" src="url(Customization/OpenTopic/common/artwork/logo.eps)"/>
                                </fo:block>
                            </fo:table-cell>
                        </fo:table-row>
                    </fo:table-body>
				</fo:table>
            </fo:block>
        </fo:static-content>
    </xsl:template>

    <xsl:template name="insertPrefaceFirstFooter">
        <fo:static-content flow-name="first-body-footer">
            <fo:block xsl:use-attribute-sets="__body__first__footer">
                <xsl:call-template name="insertVariable">
                    <xsl:with-param name="theVariableID" select="'Preface first footer'"/>
                    <xsl:with-param name="theParameters">
                        <heading>
                            <fo:inline xsl:use-attribute-sets="__body__first__footer__heading">
                                <fo:retrieve-marker retrieve-class-name="current-header"/>
                            </fo:inline>
                        </heading>
                        <pagenum>
                            <fo:inline xsl:use-attribute-sets="__body__first__footer__pagenum">
                                <fo:page-number/>
                            </fo:inline>
                        </pagenum>
                    </xsl:with-param>
                </xsl:call-template>
            </fo:block>
        </fo:static-content>
    </xsl:template>

    <xsl:template name="insertPrefaceLastHeader">
        <fo:static-content flow-name="last-body-header">
            <fo:block xsl:use-attribute-sets="__body__last__header"> </fo:block>
        </fo:static-content>
    </xsl:template>

    <xsl:template name="insertPrefaceLastFooter">
        <fo:static-content flow-name="last-body-footer">
            <fo:block xsl:use-attribute-sets="__body__last__footer"> </fo:block>
        </fo:static-content>
    </xsl:template>

    <xsl:template name="insertPrefaceFootnoteSeparator">
        <fo:static-content flow-name="xsl-footnote-separator">
            <fo:block>
                <fo:leader xsl:use-attribute-sets="__body__footnote__separator"/>
            </fo:block>
        </fo:static-content>
    </xsl:template>

    <xsl:template name="insertPrefaceOddFooter">
        <fo:static-content flow-name="odd-body-footer">
            <fo:block xsl:use-attribute-sets="__body__odd__footer">
                <fo:table start-indent="20mm" end-indent="20mm" width="100%">
                    <fo:table-column width="50%"/>
                    <fo:table-column width="50%"/>
                    <fo:table-body>
                        <fo:table-row>
                            <fo:table-cell text-align="start" start-indent="0mm">
                                <fo:block>
                                    <fo:inline>
                                        <xsl:value-of
                                            select="ancestor::*[contains(@class, ' map/map ')]/opentopic:map/topicmeta/metadata/othermeta[@name = 'OBJECTNR']/@content"
                                        />
                                    </fo:inline>
                                    <fo:inline> - </fo:inline>
                                    <fo:inline>
                                        <xsl:value-of
                                            select="ancestor::*[contains(@class, ' map/map ')]/opentopic:map/topicmeta/metadata/othermeta[@name = 'VERSION']/@content"
                                        />
                                    </fo:inline>
                                    <fo:inline> - </fo:inline>
                                    <fo:inline>
                                        <xsl:value-of
                                            select="ancestor::*[contains(@class, ' map/map ')]/opentopic:map/topicmeta/metadata/othermeta[@name = 'RELEASED']/@content"
                                        />
                                    </fo:inline>
                                    <!--   09-01-2018 avdh: add term DRAFT to the frontpage-->
                                    <xsl:choose>
                                        <xsl:when test="$DRAFTPUB">
                                            <fo:inline> - DRAFT</fo:inline>
                                        </xsl:when>
                                    </xsl:choose>
                                </fo:block>
                            </fo:table-cell>
                            <fo:table-cell text-align="end" end-indent="0mm">
                                <fo:block>
                                    <xsl:call-template name="insertVariable">
                                        <xsl:with-param name="theVariableID"
                                            select="'Body odd footer'"/>
                                        <xsl:with-param name="theParameters">
                                            <pagenum>
                                                <fo:inline
                                                  xsl:use-attribute-sets="__body__odd__footer__pagenum">
                                                  <fo:page-number/>
                                                </fo:inline>
                                            </pagenum>
                                        </xsl:with-param>
                                    </xsl:call-template>
                                </fo:block>
                            </fo:table-cell>
                        </fo:table-row>
                    </fo:table-body>
                </fo:table>
            </fo:block>
        </fo:static-content>
    </xsl:template>

    <xsl:template name="insertPrefaceEvenFooter">
        <fo:static-content flow-name="even-body-footer">
            <fo:block xsl:use-attribute-sets="__body__even__footer">

                <fo:table start-indent="20mm" end-indent="20mm" width="100%">
                    <fo:table-column width="50%"/>
                    <fo:table-column width="50%"/>
                    <fo:table-body>
                        <fo:table-row>
                            <fo:table-cell text-align="start" start-indent="0mm">
                                <fo:block>
                                    <xsl:call-template name="insertVariable">
                                        <xsl:with-param name="theVariableID"
                                            select="'Body odd footer'"/>
                                        <xsl:with-param name="theParameters">
                                            <pagenum>
                                                <fo:inline
                                                  xsl:use-attribute-sets="__body__odd__footer__pagenum">
                                                  <fo:page-number/>
                                                </fo:inline>
                                            </pagenum>
                                        </xsl:with-param>
                                    </xsl:call-template>
                                </fo:block>
                            </fo:table-cell>
                            <fo:table-cell text-align="end" end-indent="0mm">
                                <fo:block>
                                    <fo:inline>
                                        <xsl:value-of
                                            select="ancestor::*[contains(@class, ' map/map ')]/opentopic:map/topicmeta/metadata/othermeta[@name = 'OBJECTNR']/@content"
                                        />
                                    </fo:inline>
                                    <fo:inline> - </fo:inline>
                                    <fo:inline>
                                        <xsl:value-of
                                            select="ancestor::*[contains(@class, ' map/map ')]/opentopic:map/topicmeta/metadata/othermeta[@name = 'VERSION']/@content"
                                        />
                                    </fo:inline>
                                    <fo:inline> - </fo:inline>
                                    <fo:inline>
                                        <xsl:value-of
                                            select="ancestor::*[contains(@class, ' map/map ')]/opentopic:map/topicmeta/metadata/othermeta[@name = 'RELEASED']/@content"
                                        />
                                    </fo:inline>
                                    <!--   09-01-2018 avdh: add term DRAFT to the frontpage-->
                                    <xsl:choose>
                                        <xsl:when test="$DRAFTPUB">
                                            <fo:inline> - DRAFT</fo:inline>
                                        </xsl:when>
                                    </xsl:choose>
                                </fo:block>
                            </fo:table-cell>

                        </fo:table-row>
                    </fo:table-body>
                </fo:table>
            </fo:block>
        </fo:static-content>
    </xsl:template>

    <xsl:template name="insertFrontMatterOddHeader">
        <fo:static-content flow-name="odd-frontmatter-header">
            <fo:block xsl:use-attribute-sets="__body__odd__header">
                <fo:table start-indent="20mm" end-indent="10mm" width="100%">
                    <fo:table-column width="33%"/>
                    <fo:table-column width="34%"/>
                    <fo:table-column width="33%"/>
                    <fo:table-body>
                        <fo:table-row>
                            <fo:table-cell text-align="start" display-align="after"
                                start-indent="0mm" width="33%">
                                <fo:block>
                                    <fo:inline>
                                        <xsl:value-of select="ancestor::*[contains(@class, ' map/map ')]/opentopic:map/topicmeta/metadata/othermeta[@name = 'DOCNR']/@content"/> 
                                        <xsl:text> </xsl:text><xsl:call-template name="insertVariable">
                                            <xsl:with-param name="theVariableID" select="'Version'"/>
                                        </xsl:call-template><xsl:text> </xsl:text>
                                        <xsl:value-of select="ancestor::*[contains(@class, ' map/map ')]/opentopic:map/topicmeta/metadata/othermeta[@name = 'VERSION']/@content"/>
                                        <!--                                        <xsl:value-of select="descendant::*[contains(@class, ' map/map ')]/opentopic:map/topicmeta/metadata/othermeta[@name = 'DOCNR']/@content"/>-->
                                    </fo:inline>
                                </fo:block>
                            </fo:table-cell>
                            <fo:table-cell text-align="center" display-align="after"
                                start-indent="0mm" width="34%">
                                <fo:block>
                                    <xsl:variable name="cover" select="//*[contains(@class, ' topic/topic ') and contains(@outputclass, 'cover')][1]"/>
                                    <xsl:value-of select="$cover/*[contains(@class, ' topic/title ')][1]"/>
                                    <!--  <xsl:choose>
                                        <xsl:when
                                            test="$map/*[contains(@class, ' topic/title ')][1]">
                                            <xsl:apply-templates
                                                select="$map/*[contains(@class, ' topic/title ')][1]"
                                            />
                                        </xsl:when>
                                        <xsl:when
                                            test="$map//*[contains(@class, ' bookmap/mainbooktitle ')][1]">
                                            <xsl:apply-templates
                                                select="$map//*[contains(@class, ' bookmap/mainbooktitle ')][1]"
                                            />
                                        </xsl:when>
                                        <xsl:when test="//*[contains(@class, ' map/map ')]/@title">
                                            <xsl:value-of
                                                select="//*[contains(@class, ' map/map ')]/@title"/>
                                        </xsl:when>
                                        <xsl:otherwise>
                                            <xsl:value-of
                                                select="/descendant::*[contains(@class, ' topic/topic ')][1]/*[contains(@class, ' topic/title ')]"
                                            />
                                        </xsl:otherwise>
                                    </xsl:choose>-->
                                </fo:block>
                                <fo:block>
                                    <fo:inline>
                                        <!--DOCUMENT TYPE-->
                                        <xsl:variable name="doctype">
                                            <xsl:value-of select="//opentopic:map/topicmeta/metadata/othermeta[@name = 'DOCTYPE']/@content"/>
                                        </xsl:variable>
                                        <!-- make doctype smallcase -->
                                        <xsl:variable name="doctypeLC" select="translate($doctype, $UpperCase, $SmallCase)"/>
                                        <!-- clean the doctype from divers chars. -->
                                        <xsl:variable name="doctypeclean" select="translate($doctypeLC, $stopset, '')"/>
                                        <xsl:call-template name="insertVariable">
                                            <xsl:with-param name="theVariableID" select="$doctypeclean"/>
                                        </xsl:call-template>
                                    </fo:inline>
                                </fo:block>
                            </fo:table-cell>
                            <fo:table-cell text-align="end" display-align="after" end-indent="0mm" width="33%">
                                <fo:block>
                                    <fo:external-graphic content-height="15mm" padding-bottom="-3mm" src="url(Customization/OpenTopic/common/artwork/logo.eps)"/>
                                </fo:block>
                            </fo:table-cell>
                        </fo:table-row>
                    </fo:table-body>
                </fo:table>
            </fo:block>
        </fo:static-content>
    </xsl:template>

    <xsl:template name="insertFrontMatterEvenHeader">
        <fo:static-content flow-name="even-frontmatter-header">
            <fo:block xsl:use-attribute-sets="__body__even__header">
                <fo:table start-indent="10mm" end-indent="20mm" width="100%">
                    <fo:table-column width="33%"/>
                    <fo:table-column width="34%"/>
                    <fo:table-column width="33%"/>
                    <fo:table-body>
                        <fo:table-row>
                            <fo:table-cell text-align="start" display-align="after"
                                start-indent="0mm" width="33%">
                                <fo:block>
                                    <fo:external-graphic content-height="15mm" padding-bottom="-3mm" src="url(Customization/OpenTopic/common/artwork/logo.eps)"/>
                                </fo:block>
                            </fo:table-cell>
                            <fo:table-cell text-align="center" display-align="after"
                                end-indent="0mm" width="34%">
                                <fo:block>
                                    <xsl:variable name="cover" select="//*[contains(@class, ' topic/topic ') and contains(@outputclass, 'cover')][1]"/>
                                    <xsl:value-of select="$cover/*[contains(@class, ' topic/title ')][1]"/>								
								<!--
                                    <xsl:choose>
                                        <xsl:when
                                            test="$map/*[contains(@class, ' topic/title ')][1]">
                                            <xsl:apply-templates
                                                select="$map/*[contains(@class, ' topic/title ')][1]"
                                            />
                                        </xsl:when>
                                        <xsl:when
                                            test="$map//*[contains(@class, ' bookmap/mainbooktitle ')][1]">
                                            <xsl:apply-templates
                                                select="$map//*[contains(@class, ' bookmap/mainbooktitle ')][1]"
                                            />
                                        </xsl:when>
                                        <xsl:when test="//*[contains(@class, ' map/map ')]/@title">
                                            <xsl:value-of
                                                select="//*[contains(@class, ' map/map ')]/@title"/>
                                        </xsl:when>
                                        <xsl:otherwise>
                                            <xsl:value-of
                                                select="/descendant::*[contains(@class, ' topic/topic ')][1]/*[contains(@class, ' topic/title ')]"
                                            />
                                        </xsl:otherwise>
                                    </xsl:choose>
								-->
                                </fo:block>
								<fo:block keep-together.within-line="always">
                                    <fo:inline>
                                        <!--DOCUMENT TYPE-->
                                        <xsl:variable name="doctype">
                                            <xsl:value-of select="//opentopic:map/topicmeta/metadata/othermeta[@name = 'DOCTYPE']/@content"/>
                                        </xsl:variable>
                                        <!-- make doctype smallcase -->
                                        <xsl:variable name="doctypeLC" select="translate($doctype, $UpperCase, $SmallCase)"/>
                                        <!-- clean the doctype from divers chars. -->
                                        <xsl:variable name="doctypeclean" select="translate($doctypeLC, $stopset, '')"/>
                                        <xsl:call-template name="insertVariable">
                                            <xsl:with-param name="theVariableID" select="$doctypeclean"/>
                                        </xsl:call-template>
                                    </fo:inline>
                                </fo:block>
                            </fo:table-cell>
                            <fo:table-cell text-align="end" display-align="after" end-indent="0mm"
                                width="33%">
                                <fo:block>
                                    <fo:inline>
                                        <xsl:value-of
                                            select="ancestor::*[contains(@class, ' map/map ')]/opentopic:map/topicmeta/metadata/othermeta[@name = 'DOCNR']/@content"
                                        />
                                    </fo:inline>
                                </fo:block>
                            </fo:table-cell>
                        </fo:table-row>
                    </fo:table-body>
                </fo:table>
            </fo:block>
        </fo:static-content>
    </xsl:template>

    <xsl:template name="insertFrontMatterLastHeader">
        <fo:static-content flow-name="last-frontmatter-header">
            <fo:block xsl:use-attribute-sets="__body__even__header">
                <xsl:call-template name="insertVariable">
                    <xsl:with-param name="theVariableID" select="'Preface even header'"/>
                    <xsl:with-param name="theParameters">
                        <prodname>
                            <xsl:value-of select="$productName"/>
                        </prodname>
                        <heading>
                            <fo:inline xsl:use-attribute-sets="__body__even__header__heading">
                                <fo:retrieve-marker retrieve-class-name="current-header"/>
                            </fo:inline>
                        </heading>
                        <pagenum>
                            <fo:inline xsl:use-attribute-sets="__body__even__header__pagenum">
                                <fo:page-number/>
                            </fo:inline>
                        </pagenum>
                    </xsl:with-param>
                </xsl:call-template>
            </fo:block>
        </fo:static-content>
    </xsl:template>

    <xsl:template name="insertFrontMatterFootnoteSeparator">
        <fo:static-content flow-name="xsl-footnote-separator">
            <fo:block>
                <fo:leader xsl:use-attribute-sets="__body__footnote__separator"/>
            </fo:block>
        </fo:static-content>
    </xsl:template>

    <xsl:template name="insertFrontMatterFirstFooter">
        <fo:static-content flow-name="first-frontmatter-footer">
            <fo:block xsl:use-attribute-sets="frontpage__footer">
                <fo:block><xsl:value-of select="descendant::*[contains(@class, ' map/map ')]/opentopic:map/topicmeta/metadata/othermeta[@name = 'DOCNR']/@content"/> 
                    <xsl:text> </xsl:text><xsl:call-template name="insertVariable">
                        <xsl:with-param name="theVariableID" select="'Version'"/>
                    </xsl:call-template>
                    <xsl:text> </xsl:text><xsl:value-of select="descendant::*[contains(@class, ' map/map ')]/opentopic:map/topicmeta/metadata/othermeta[@name = 'VERSION']/@content"/> 
                </fo:block>
                <fo:block><xsl:call-template name="insertVariable">
                    <xsl:with-param name="theVariableID" select="'ApprovalDate'"/>
                </xsl:call-template>: <xsl:value-of select="descendant::*[contains(@class, ' map/map ')]/opentopic:map/topicmeta/metadata/othermeta[@name = 'RELEASED']/@content"/>
                    
                </fo:block>
                <xsl:variable name="objstatus" select="descendant::*[contains(@class, ' map/map ')]/opentopic:map/topicmeta/metadata/othermeta[@name = 'OBJSTATUS']/@content"/>
                <xsl:variable name="docstatus">
                    <xsl:choose>
                        <xsl:when test="$objstatus='ARC'">Ready</xsl:when>
						<xsl:when test="$objstatus='AIA'">Ready</xsl:when>
                        <xsl:when test="$objstatus='UG1'">Out of order</xsl:when>
                        <xsl:when test="$objstatus='UG2'">Out of order</xsl:when>
                        <xsl:otherwise>Not Ready</xsl:otherwise>
                    </xsl:choose>    
                </xsl:variable>
                <fo:block><xsl:call-template name="insertVariable">
                    <xsl:with-param name="theVariableID" select="'Status'"/>
                </xsl:call-template>: <xsl:call-template name="insertVariable">
                    <xsl:with-param name="theVariableID" select="$docstatus"/>
                </xsl:call-template>
                    <!--<xsl:value-of select="$docstatus"/>-->
                </fo:block>
            </fo:block>
        </fo:static-content>
    </xsl:template>
    
    <xsl:template name="insertFrontMatterOddFooter">
        <fo:static-content flow-name="odd-frontmatter-footer">
            <fo:block xsl:use-attribute-sets="__body__odd__footer">
                <xsl:call-template name="insertVariable">
                    <xsl:with-param name="theVariableID" select="'Preface odd footer'"/>
                    <xsl:with-param name="theParameters">
                        <heading>
                            <fo:inline xsl:use-attribute-sets="__body__odd__footer__heading">
                                <fo:retrieve-marker retrieve-class-name="current-header"/>
                            </fo:inline>
                        </heading>
                        <pagenum>
                            <fo:inline xsl:use-attribute-sets="__body__odd__footer__pagenum">
                                <fo:page-number/>
                            </fo:inline>
                        </pagenum>
                    </xsl:with-param>
                </xsl:call-template>
            </fo:block>
        </fo:static-content>
    </xsl:template>

    <xsl:template name="insertFrontMatterEvenFooter">
        <fo:static-content flow-name="even-frontmatter-footer">
            <fo:block xsl:use-attribute-sets="__body__even__footer">
                <fo:table start-indent="20mm" end-indent="10mm" width="100%">
                    <fo:table-column width="33%"/>
                    <fo:table-column width="34%"/>
                    <fo:table-column width="33%"/>
                    <fo:table-body>
                        <fo:table-row>
                            <fo:table-cell text-align="start" start-indent="0mm" width="33%">
                                <fo:block>
                                    <!--<fo:inline>Text aligned to left</fo:inline>-->
                                </fo:block>
                            </fo:table-cell>
                            <fo:table-cell text-align="center" width="34%" start-indent="0mm">
                                <fo:block>
                                    <xsl:call-template name="insertVariable">
                                        <xsl:with-param name="theVariableID"
                                            select="'Body odd footer'"/>
                                        <xsl:with-param name="theParameters">
                                            <pagenum>
                                                <fo:inline
                                                  xsl:use-attribute-sets="__body__odd__footer__pagenum">
                                                  <fo:page-number/>
                                                </fo:inline>
                                            </pagenum>
                                        </xsl:with-param>
                                    </xsl:call-template>
                                    <xsl:text> / </xsl:text><fo:page-number-citation ref-id="last-page"/>
                                </fo:block>
                            </fo:table-cell>
                            <fo:table-cell text-align="end" width="33%">
                                <!--end-indent="0mm"-->
                                <fo:block keep-together.within-line="always">
                                    <xsl:call-template name="insertcopyright"/>
                                </fo:block>
                            </fo:table-cell>
                        </fo:table-row>
                    </fo:table-body>
                </fo:table>
            </fo:block>
        </fo:static-content>
    </xsl:template>

    <xsl:template name="insertFrontMatterLastFooter">
        <fo:static-content flow-name="last-frontmatter-footer">
            <fo:block xsl:use-attribute-sets="__body__last__footer"> </fo:block>
        </fo:static-content>
    </xsl:template>

    <xsl:template name="insertGlossaryOddHeader">
        <fo:static-content flow-name="odd-glossary-header">
            <fo:block xsl:use-attribute-sets="__glossary__odd__header">
                <fo:table start-indent="20mm" end-indent="10mm" width="100%">
                    <fo:table-column width="33%"/>
                    <fo:table-column width="34%"/>
                    <fo:table-column width="33%"/>
                    <fo:table-body>
                        <fo:table-row>
                            <fo:table-cell text-align="start" display-align="after"
                                start-indent="0mm" width="33%">
                                <fo:block>
                                    <fo:inline>
                                        <xsl:value-of
                                            select="descendant::*[contains(@class, ' map/map ')]/opentopic:map/topicmeta/metadata/othermeta[@name = 'DOCNR']/@content"
                                        />
                                    </fo:inline>
                                </fo:block>
                            </fo:table-cell>
                            <fo:table-cell text-align="center" display-align="after"
                                start-indent="0mm" width="34%">
                                <fo:block>
                                    <xsl:choose>
                                        <xsl:when
                                            test="$map/*[contains(@class, ' topic/title ')][1]">
                                            <xsl:apply-templates
                                                select="$map/*[contains(@class, ' topic/title ')][1]"
                                            />
                                        </xsl:when>
                                        <xsl:when
                                            test="$map//*[contains(@class, ' bookmap/mainbooktitle ')][1]">
                                            <xsl:apply-templates
                                                select="$map//*[contains(@class, ' bookmap/mainbooktitle ')][1]"
                                            />
                                        </xsl:when>
                                        <xsl:when test="//*[contains(@class, ' map/map ')]/@title">
                                            <xsl:value-of
                                                select="//*[contains(@class, ' map/map ')]/@title"/>
                                        </xsl:when>
                                        <xsl:otherwise>
                                            <xsl:value-of
                                                select="/descendant::*[contains(@class, ' topic/topic ')][1]/*[contains(@class, ' topic/title ')]"
                                            />
                                        </xsl:otherwise>
                                    </xsl:choose>
                                </fo:block>
                                <fo:block>
                                    <fo:inline>
                                        <xsl:value-of
                                            select="descendant::*[contains(@class, ' map/map ')]/opentopic:map/topicmeta/metadata/othermeta[@name = 'DOCTYPE']/@content"
                                        />
                                    </fo:inline>
                                </fo:block>
                            </fo:table-cell>
                            <fo:table-cell text-align="end" display-align="after" end-indent="0mm"
                                width="33%">
                                <fo:block>
                                    <fo:external-graphic content-height="15mm" padding-bottom="-3mm" src="url(Customization/OpenTopic/common/artwork/logo.eps)"/>
                                </fo:block>
                            </fo:table-cell>
                        </fo:table-row>
                    </fo:table-body>
                </fo:table>
            </fo:block>
        </fo:static-content>
    </xsl:template>

    <xsl:template name="insertGlossaryEvenHeader">
        <fo:static-content flow-name="even-glossary-header">
            <fo:block xsl:use-attribute-sets="__glossary__even__header">
                <fo:table start-indent="10mm" end-indent="20mm" width="100%">
                    <fo:table-column width="33%"/>
                    <fo:table-column width="34%"/>
                    <fo:table-column width="33%"/>
                    <fo:table-body>
                        <fo:table-row>
                            <fo:table-cell text-align="start" display-align="after"
                                start-indent="0mm" width="33%">
                                <fo:block>
                                    <fo:external-graphic content-height="15mm" padding-bottom="-3mm" src="url(Customization/OpenTopic/common/artwork/logo.eps)"/>
                                </fo:block>
                            </fo:table-cell>
                            <fo:table-cell text-align="center" display-align="after"
                                end-indent="0mm" width="34%">
                                <fo:block>
                                    <xsl:choose>
                                        <xsl:when
                                            test="$map/*[contains(@class, ' topic/title ')][1]">
                                            <xsl:apply-templates
                                                select="$map/*[contains(@class, ' topic/title ')][1]"
                                            />
                                        </xsl:when>
                                        <xsl:when
                                            test="$map//*[contains(@class, ' bookmap/mainbooktitle ')][1]">
                                            <xsl:apply-templates
                                                select="$map//*[contains(@class, ' bookmap/mainbooktitle ')][1]"
                                            />
                                        </xsl:when>
                                        <xsl:when test="//*[contains(@class, ' map/map ')]/@title">
                                            <xsl:value-of
                                                select="//*[contains(@class, ' map/map ')]/@title"/>
                                        </xsl:when>
                                        <xsl:otherwise>
                                            <xsl:value-of
                                                select="/descendant::*[contains(@class, ' topic/topic ')][1]/*[contains(@class, ' topic/title ')]"
                                            />
                                        </xsl:otherwise>
                                    </xsl:choose>
                                </fo:block>
                                <fo:block>
                                    <fo:inline>
                                        <xsl:value-of
                                            select="descendant::*[contains(@class, ' map/map ')]/opentopic:map/topicmeta/metadata/othermeta[@name = 'DOCTYPE']/@content"
                                        />
                                    </fo:inline>
                                </fo:block>
                            </fo:table-cell>
                            <fo:table-cell text-align="end" display-align="after" end-indent="0mm"
                                width="33%">
                                <fo:block>
                                    <fo:inline>
                                        <xsl:value-of
                                            select="descendant::*[contains(@class, ' map/map ')]/opentopic:map/topicmeta/metadata/othermeta[@name = 'DOCNR']/@content"
                                        />
                                    </fo:inline>
                                </fo:block>
                            </fo:table-cell>
                        </fo:table-row>
                    </fo:table-body>
                </fo:table>
            </fo:block>
        </fo:static-content>
    </xsl:template>

    <xsl:template name="insertGlossaryOddFooter">
        <fo:static-content flow-name="odd-glossary-footer">
            <fo:block xsl:use-attribute-sets="__glossary__odd__footer">
                <fo:table start-indent="20mm" end-indent="20mm" width="100%">
                    <fo:table-column width="50%"/>
                    <fo:table-column width="50%"/>
                    <fo:table-body>
                        <fo:table-row>
                            <fo:table-cell text-align="start" start-indent="0mm">
                                <fo:block>
                                    <fo:inline>
                                        <xsl:value-of
                                            select="ancestor::*[contains(@class, ' map/map ')]/opentopic:map/topicmeta/metadata/othermeta[@name = 'OBJECTNR']/@content"
                                        />
                                    </fo:inline>
                                    <fo:inline> - </fo:inline>
                                    <fo:inline>
                                        <xsl:value-of
                                            select="ancestor::*[contains(@class, ' map/map ')]/opentopic:map/topicmeta/metadata/othermeta[@name = 'VERSION']/@content"
                                        />
                                    </fo:inline>
                                    <fo:inline> - </fo:inline>
                                    <fo:inline>
                                        <xsl:value-of
                                            select="ancestor::*[contains(@class, ' map/map ')]/opentopic:map/topicmeta/metadata/othermeta[@name = 'RELEASED']/@content"
                                        />
                                    </fo:inline>
                                    <!--   09-01-2018 avdh: add term DRAFT to the frontpage-->
                                    <xsl:choose>
                                        <xsl:when test="$DRAFTPUB">
                                            <fo:inline> - DRAFT</fo:inline>
                                        </xsl:when>
                                    </xsl:choose>
                                </fo:block>
                            </fo:table-cell>
                            <fo:table-cell text-align="end" end-indent="0mm">
                                <fo:block>
                                    <xsl:call-template name="insertVariable">
                                        <xsl:with-param name="theVariableID"
                                            select="'Body odd footer'"/>
                                        <xsl:with-param name="theParameters">
                                            <pagenum>
                                                <fo:inline
                                                  xsl:use-attribute-sets="__body__odd__footer__pagenum">
                                                  <fo:page-number/>
                                                </fo:inline>
                                            </pagenum>
                                        </xsl:with-param>
                                    </xsl:call-template>
                                </fo:block>
                            </fo:table-cell>
                        </fo:table-row>
                    </fo:table-body>
                </fo:table>
            </fo:block>
        </fo:static-content>
    </xsl:template>

    <xsl:template name="insertGlossaryEvenFooter">
        <fo:static-content flow-name="even-glossary-footer">
            <fo:block xsl:use-attribute-sets="__glossary__even__footer">
                <fo:table start-indent="20mm" end-indent="20mm" width="100%">
                    <fo:table-column width="50%"/>
                    <fo:table-column width="50%"/>
                    <fo:table-body>
                        <fo:table-row>
                            <fo:table-cell text-align="start" start-indent="0mm">
                                <fo:block>
                                    <xsl:call-template name="insertVariable">
                                        <xsl:with-param name="theVariableID"
                                            select="'Body odd footer'"/>
                                        <xsl:with-param name="theParameters">
                                            <pagenum>
                                                <fo:inline
                                                  xsl:use-attribute-sets="__body__odd__footer__pagenum">
                                                  <fo:page-number/>
                                                </fo:inline>
                                            </pagenum>
                                        </xsl:with-param>
                                    </xsl:call-template>
                                </fo:block>
                            </fo:table-cell>
                            <fo:table-cell text-align="end" end-indent="0mm">
                                <fo:block>
                                    <fo:inline>
                                        <xsl:value-of
                                            select="ancestor::*[contains(@class, ' map/map ')]/opentopic:map/topicmeta/metadata/othermeta[@name = 'OBJECTNR']/@content"
                                        />
                                    </fo:inline>
                                    <fo:inline> - </fo:inline>
                                    <fo:inline>
                                        <xsl:value-of
                                            select="ancestor::*[contains(@class, ' map/map ')]/opentopic:map/topicmeta/metadata/othermeta[@name = 'VERSION']/@content"
                                        />
                                    </fo:inline>
                                    <fo:inline> - </fo:inline>
                                    <fo:inline>
                                        <xsl:value-of
                                            select="ancestor::*[contains(@class, ' map/map ')]/opentopic:map/topicmeta/metadata/othermeta[@name = 'RELEASED']/@content"
                                        />
                                    </fo:inline>
                                    <!--   09-01-2018 avdh: add term DRAFT to the frontpage-->
                                    <xsl:choose>
                                        <xsl:when test="$DRAFTPUB">
                                            <fo:inline> - DRAFT</fo:inline>
                                        </xsl:when>
                                    </xsl:choose>
                                </fo:block>
                            </fo:table-cell>
                        </fo:table-row>
                    </fo:table-body>
                </fo:table>
            </fo:block>
        </fo:static-content>
    </xsl:template>

<xsl:template name="insertcopyright">
    <fo:inline text-align="left">Copyright © ŠKODA TRANSTECH Oy</fo:inline>
    <xsl:text> </xsl:text>
    <!-- Get copyright year from Released metadata, if not exist use created data. -->
    <!-- Metadata is descendant for covers and ascendant for topic headers --> 
    <xsl:variable name="released">
        <xsl:value-of select="descendant::*[contains(@class, ' map/map ')]/opentopic:map/topicmeta/metadata/othermeta[@name = 'RELEASED']/@content"/>    
    </xsl:variable>
    <xsl:variable name="released2">
        <xsl:value-of select="ancestor::*[contains(@class, ' map/map ')]/opentopic:map/topicmeta/metadata/othermeta[@name = 'RELEASED']/@content"/>    
    </xsl:variable>
    <xsl:variable name="created">
        <xsl:value-of select="descendant::*[contains(@class, ' map/map ')]/opentopic:map/topicmeta/critdates/created/@date"/>    
    </xsl:variable>
    <xsl:variable name="created2">
        <xsl:value-of select="ancestor::*[contains(@class, ' map/map ')]/opentopic:map/topicmeta/critdates/created/@date"/>    
    </xsl:variable>
    <xsl:variable name="datetxt">
        <xsl:choose>
            <xsl:when test="string-length($released)>0">
                <xsl:value-of select="$released"/>
            </xsl:when>
            <xsl:when test="string-length($released2)>0">
                <xsl:value-of select="$released2"/>
            </xsl:when>
            <xsl:when test="string-length($created)>0">
                <xsl:value-of select="$created"/>
            </xsl:when>
            <xsl:when test="string-length($created2)>0">
                <xsl:value-of select="$created2"/>
            </xsl:when>
            <xsl:otherwise>
            </xsl:otherwise>
        </xsl:choose>
    </xsl:variable>
    <xsl:message>static-content.xsl, date in=<xsl:value-of select="$datetxt"/></xsl:message>
    <xsl:choose>
        <xsl:when test="string-length($datetxt)=0"></xsl:when>
        <xsl:when test="substring($datetxt,3,1)='.'">
            <!-- This is format DD.MM.YYYY, get last 4 as year -->
            <xsl:value-of select="substring($datetxt,string-length($datetxt)-3,4)"/>
            <xsl:message>static-content.xsl, fix date in=<xsl:value-of select="$datetxt"/>, out=<xsl:value-of select="substring($datetxt,string-length($datetxt)-3,4)"/></xsl:message>
        </xsl:when>
        <xsl:otherwise><xsl:value-of select="year-from-date($datetxt)"/></xsl:otherwise>
    </xsl:choose>    

</xsl:template>
    
</xsl:stylesheet>

