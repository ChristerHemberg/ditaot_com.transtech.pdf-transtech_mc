﻿<?xml version='1.0'?>
<xsl:stylesheet xmlns:xsl="http://www.w3.org/1999/XSL/Transform"
    xmlns:fo="http://www.w3.org/1999/XSL/Format" version="2.0">

    <xsl:template name="getNoteType">
        <xsl:choose>
            <!--<xsl:when test="@type = 'other' and @othertype">
                  <xsl:value-of select="@othertype"/>
              </xsl:when>-->
            <xsl:when test="@type">
                <xsl:value-of select="@type"/>
            </xsl:when>
            <xsl:otherwise>
                <xsl:value-of select="'note'"/>
            </xsl:otherwise>
        </xsl:choose>
    </xsl:template>

    <!-- Lex: Added uppercaseFirst template -->
    <xsl:template name="uppercaseFirst">
        <xsl:param name="text"/>
        <xsl:value-of select="concat(upper-case(substring($text, 1, 1)),substring($text, 2),' '[not(last())])"/>
    </xsl:template>

    <xsl:template match="*[contains(@class, ' hazard-d/hazardstatement ')]">
        <xsl:variable name="noteImagePath">
            <xsl:apply-templates select="." mode="setNoteImagePath"/>
        </xsl:variable>
        <xsl:variable name="hazardImagePath">
            <xsl:value-of select="*[contains(@class, ' hazard-d/hazardsymbol ')]/@href"/>
        </xsl:variable>

        <!-- Lex: added call to noteType for checking danger, caution or warning later -->
        <xsl:variable name="noteType">
            <xsl:call-template name="getNoteType"/>
        </xsl:variable>
        <!-- Lex: added noteTypeUpperCase to change the first letter to uppercase -->
        <xsl:variable name="noteTypeUpperCase">
            <xsl:call-template name="uppercaseFirst">
                <xsl:with-param name="text" select="$noteType"/>
            </xsl:call-template>
        </xsl:variable>
        <xsl:choose>
            <xsl:when test="not($hazardImagePath = '') or not($noteImagePath = '')">
            <fo:table xsl:use-attribute-sets="hazard__table">
            <fo:table-column xsl:use-attribute-sets="note__image__column"/>
            <fo:table-column xsl:use-attribute-sets="hazard__text__column"/>
            <fo:table-body>
                <fo:table-row>
                    <!--<fo:table-cell> </fo:table-cell>-->
                    <!--tests for each hazard type danger, warning caution-->
                    <xsl:if test="$noteType = 'warning'">
                        <fo:table-cell xsl:use-attribute-sets="tbody.row.entry.warning" number-columns-spanned="2">
                            <xsl:attribute name="start-indent">0pt</xsl:attribute>
                            <xsl:attribute name="font-weight">bold</xsl:attribute>
                            <!-- Lex: dynamicly get the color according to $noteType -->
                            <xsl:call-template name="processAttrSetReflection">
                                <xsl:with-param name="attrSet" select="concat('tbody.row.entry.', $noteType)"/>
                            </xsl:call-template>
                            <fo:block xsl:use-attribute-sets="tbody.row.entry.center">
                                <xsl:attribute name="padding-top">2pt</xsl:attribute>
                                <xsl:call-template name="insertVariable">
                                    <xsl:with-param name="theVariableID"
                                        select="$noteTypeUpperCase"/>
                                </xsl:call-template>
                            </fo:block>
                        </fo:table-cell>
                    </xsl:if>
                    <xsl:if test="$noteType = 'danger'">
                        <fo:table-cell xsl:use-attribute-sets="tbody.row.entry.danger" number-columns-spanned="2">
                            <xsl:attribute name="start-indent">0pt</xsl:attribute>
                            <xsl:attribute name="font-weight">bold</xsl:attribute>
                            <!-- Lex: dynamicly get the color according to $noteType -->
                            <xsl:call-template name="processAttrSetReflection">
                                <xsl:with-param name="attrSet"
                                    select="concat('tbody.row.entry.', $noteType)"/>
                            </xsl:call-template>
                            <fo:block xsl:use-attribute-sets="tbody.row.entry.center">
                                <xsl:attribute name="padding-top">2pt</xsl:attribute>
                                <xsl:call-template name="insertVariable">
                                    <xsl:with-param name="theVariableID"
                                        select="$noteTypeUpperCase"/>
                                </xsl:call-template>
                            </fo:block>
                        </fo:table-cell>
                    </xsl:if>
                    <xsl:if test="$noteType = 'caution'">
                        <fo:table-cell xsl:use-attribute-sets="tbody.row.entry.caution" number-columns-spanned="2">
                            <xsl:attribute name="start-indent">0pt</xsl:attribute>
                            <xsl:attribute name="font-weight">bold</xsl:attribute>
                            <!-- Lex: dynamicly get the color according to $noteType -->
                            <xsl:call-template name="processAttrSetReflection">
                                <xsl:with-param name="attrSet"
                                    select="concat('tbody.row.entry.', $noteType)"/>
                            </xsl:call-template>
                            <fo:block xsl:use-attribute-sets="tbody.row.entry.center">
                                <xsl:attribute name="padding-top">2pt</xsl:attribute>
                                <xsl:call-template name="insertVariable">
                                    <xsl:with-param name="theVariableID"
                                        select="$noteTypeUpperCase"/>
                                </xsl:call-template>
                            </fo:block>
                        </fo:table-cell>
                    </xsl:if>
                </fo:table-row>
                <fo:table-row>
                    <fo:table-cell xsl:use-attribute-sets="hazard__image__entry">
                        <fo:block>
                            <xsl:choose>
                                <xsl:when test="not($hazardImagePath = '')">
                                    <fo:external-graphic src="url({if (contains($hazardImagePath, '://')) then $hazardImagePath else concat($input.dir.url, $hazardImagePath)})" xsl:use-attribute-sets="image hazard__image"/>
                                </xsl:when>
                                <xsl:otherwise>
                                    <fo:external-graphic src="url({$noteImagePath})" xsl:use-attribute-sets="image"/>        
                                </xsl:otherwise>
                            </xsl:choose>
                        </fo:block>
                    </fo:table-cell>
                    <fo:table-cell xsl:use-attribute-sets="hazard__text__entry">
                        <!--<xsl:apply-templates select="." mode="placeNoteContent"/>-->
                        <xsl:apply-templates select="." mode="placeHazardContent"/>
                    </fo:table-cell>
                </fo:table-row>
            </fo:table-body>
        </fo:table>
            </xsl:when>
            <xsl:otherwise>
                    <xsl:apply-templates select="." mode="placeNoteContent"/>
            </xsl:otherwise>
        </xsl:choose>
    </xsl:template>

    <xsl:template match="*[contains(@class, ' hazard-d/hazardsymbol ')]"/>

    <xsl:template match="*[contains(@class, ' hazard-d/typeofhazard ')]" mode="placeHazardContent">
        <fo:block>
            <xsl:apply-templates/>
        </fo:block>
    </xsl:template>

    <xsl:template match="*[contains(@class, ' hazard-d/consequence ')]" mode="placeHazardContent">
        <fo:block>
            <xsl:apply-templates/>
        </fo:block>
    </xsl:template>

    <xsl:template match="*[contains(@class, ' hazard-d/howtoavoid ')]" mode="placeHazardContent">
        <fo:block>
            <xsl:apply-templates/>
        </fo:block>
    </xsl:template>

</xsl:stylesheet>
