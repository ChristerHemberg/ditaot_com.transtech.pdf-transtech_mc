﻿<?xml version='1.0'?>

<xsl:stylesheet xmlns:xsl="http://www.w3.org/1999/XSL/Transform"
    xmlns:fo="http://www.w3.org/1999/XSL/Format"
    xmlns:dita2xslfo="http://dita-ot.sourceforge.net/ns/200910/dita2xslfo"
    xmlns:opentopic="http://www.idiominc.com/opentopic" xmlns:exsl="http://exslt.org/common"
    xmlns:opentopic-index="http://www.idiominc.com/opentopic/index"
    extension-element-prefixes="exsl"
    exclude-result-prefixes="opentopic exsl opentopic-index dita2xslfo" version="2.0">

    <!-- Determines whether to generate titles for task sections. Values are YES and NO. -->
    <xsl:param name="GENERATE-TASK-LABELS">
        <xsl:choose>
            <xsl:when test="$antArgsGenerateTaskLabels = 'YES'">
                <xsl:value-of select="$antArgsGenerateTaskLabels"/>
            </xsl:when>
            <xsl:otherwise>NO</xsl:otherwise>
        </xsl:choose>
    </xsl:param>

    <xsl:template match="*" mode="processTask">
        <!--      lex 28-01-2015: added: 20151210  avdh: added attribute clear=both to publish the task below a float-->
        <!-- 2018-06-08 avdh removed tghe \attributeset, no colored text for released modules-->
        <fo:block clear="both">
            <!-- Insert page break for tasks, if not previous sibling is an empty conbody (title topic) -->
            <xsl:if test="not(contains(preceding-sibling::*[1]/@class,'concept/conbody') and string-length(preceding-sibling::*[1])=0)">
                <xsl:attribute name="keep-together">100</xsl:attribute><!--100-->
            </xsl:if>
            <xsl:apply-templates select="." mode="commonTopicProcessing"/>
        </fo:block>
    </xsl:template>

    <xsl:template match="*[contains(@class, ' task/taskbody ')]">
        <fo:block xsl:use-attribute-sets="taskbody">
            <xsl:call-template name="commonattributes"/>
            <xsl:apply-templates/>
        </fo:block>
        <!-- lex 10-02-2016: clear float after the block -->
        <fo:block-container clear="both">
            <fo:block clear="both"/>
        </fo:block-container>
    </xsl:template>

    <!-- avdh: june 2017 create a table to publish the preliminary requirements-->
    <xsl:template match="*[contains(@class, ' taskreq-d/prelreqs ')]">
        <xsl:variable name="prelreqamount">
            <xsl:value-of select="count(child::*)"/>
        </xsl:variable>
        <xsl:variable name="rows" select="ceiling($prelreqamount div 2)"/>
        <fo:block>
            <fo:block xsl:use-attribute-sets="prefix.title">
                <xsl:call-template name="insertVariable">
                    <xsl:with-param name="theVariableID" select="'prelreq'"/>
                </xsl:call-template>
            </fo:block>
            <xsl:apply-templates/>
        </fo:block>
    </xsl:template>

	<!-- mpu 20-02-10: added handling for reqconds -->
    <xsl:template match="*[contains(@class, ' taskreq-d/reqconds ')]">
        <xsl:if test="ancestor::*[contains(@class,' taskreq-d/prelreqs ')]">
            <fo:block xsl:use-attribute-sets="task.tag">
                <xsl:call-template name="insertVariable">
                    <xsl:with-param name="theVariableID" select="'prelreq reqconds'"/>
                </xsl:call-template>
            </fo:block>    
        </xsl:if>
        <fo:block xsl:use-attribute-sets="reqconds">
            <fo:block xsl:use-attribute-sets="reqconds__content">
                <xsl:apply-templates/>
            </fo:block>
        </fo:block>
    </xsl:template>
	
    <xsl:template match="*[contains(@class, ' taskreq-d/reqcond ')]">
        <fo:block xsl:use-attribute-sets="reqcond">
            <fo:block xsl:use-attribute-sets="reqcond__content">
                <xsl:apply-templates/>
            </fo:block>
        </fo:block>
    </xsl:template>

    <xsl:template match="*[contains(@class, ' taskreq-d/supequip ')]">
        <fo:block xsl:use-attribute-sets="task.tag">
            <xsl:call-template name="insertVariable">
                <xsl:with-param name="theVariableID" select="'prelreq supequip'"/>
            </xsl:call-template>
        </fo:block>
        <fo:block xsl:use-attribute-sets="supequip">
            <fo:block xsl:use-attribute-sets="supequip__content">
                <xsl:apply-templates/>
            </fo:block>
        </fo:block>
    </xsl:template>

    <xsl:template match="*[contains(@class, ' taskreq-d/supplies ')]">
        <fo:block xsl:use-attribute-sets="task.tag">
            <xsl:call-template name="insertVariable">
                <xsl:with-param name="theVariableID" select="'prelreq supplies'"/>
            </xsl:call-template>
        </fo:block>
        <fo:block xsl:use-attribute-sets="supplies">
            <fo:block xsl:use-attribute-sets="supplies__content">
                <xsl:apply-templates/>
            </fo:block>
        </fo:block>
    </xsl:template>

    <xsl:template match="*[contains(@class, ' taskreq-d/spares ')]">
        <fo:block xsl:use-attribute-sets="task.tag">
            <xsl:call-template name="insertVariable">
                <xsl:with-param name="theVariableID" select="'prelreq spares'"/>
            </xsl:call-template>
        </fo:block>
        <fo:block xsl:use-attribute-sets="spares">
            <fo:block xsl:use-attribute-sets="spares__content">
                <xsl:apply-templates/>
            </fo:block>
        </fo:block>
    </xsl:template>

    <xsl:template match="*[contains(@class, ' taskreq-d/safety ')]">
        <fo:block xsl:use-attribute-sets="task.tag">
            <xsl:call-template name="insertVariable">
                <xsl:with-param name="theVariableID" select="'prelreq safety'"/>
            </xsl:call-template>
        </fo:block>
        <fo:list-block xsl:use-attribute-sets="ol">
            <xsl:call-template name="commonattributes"/>
            <xsl:apply-templates/>
        </fo:list-block>
    </xsl:template>

    <xsl:template match="*[contains(@class, ' taskreq-d/safety ')]/*[contains(@class, ' taskreq-d/safecond ')]">
        <xsl:choose>
            <xsl:when test="child::*[contains(@class, ' topic/note ') or contains(@class, ' hazard-d/hazardstatement ')]">
                <fo:list-item space-after="3mm">
                    <fo:list-item-label>
                        <fo:inline>
                            <xsl:call-template name="commonattributes"/>
                        </fo:inline>
                    </fo:list-item-label>

                    <fo:list-item-body>
                        <fo:block>
                            <fo:block>
                                <xsl:apply-templates/>
                            </fo:block>
                        </fo:block>
                    </fo:list-item-body>
                </fo:list-item>
            </xsl:when>
            <xsl:otherwise>
                <!--09-01-2018 avdh: safecond must be published as an unordered list-->
                <fo:list-item xsl:use-attribute-sets="ul.li">
                    <fo:list-item-label xsl:use-attribute-sets="ul.li__label">
                        <fo:block xsl:use-attribute-sets="ul.li__label__content">
                            <fo:inline>
                                <xsl:call-template name="commonattributes"/>
                            </fo:inline>
                            <xsl:call-template name="insertVariable">
                                <xsl:with-param name="theVariableID"
                                    select="'Unordered List bullet'"/>
                            </xsl:call-template>
                        </fo:block>
                    </fo:list-item-label>

                    <!--2018 avdh: error here, repair-->
                    <fo:list-item-body xsl:use-attribute-sets="ul.li__body">
                        <fo:block xsl:use-attribute-sets="ul.li__content">
                            <xsl:apply-templates/>
                        </fo:block>
                    </fo:list-item-body>
                </fo:list-item>
            </xsl:otherwise>
        </xsl:choose>
    </xsl:template>

    <xsl:template match="*[contains(@class, ' task/context ')]">
        <fo:block xsl:use-attribute-sets="context">
            <xsl:call-template name="commonattributes"/>
            <!--<xsl:apply-templates select="." mode="dita2xslfo:task-heading">
                <xsl:with-param name="use-label">
                    <xsl:call-template name="insertVariable">
                        <xsl:with-param name="theVariableID" select="'Task Context'"/>
                    </xsl:call-template>
                </xsl:with-param>
            </xsl:apply-templates>-->
            <fo:block xsl:use-attribute-sets="context__content">
                <xsl:apply-templates/>
            </fo:block>
        </fo:block>
    </xsl:template>

    <xsl:template match="*[contains(@class, ' task/cmd ')]" priority="1">
        <fo:block xsl:use-attribute-sets="cmd">
            <xsl:call-template name="commonattributes"/>
            <xsl:if test="../@importance = 'optional'">
                <xsl:call-template name="insertVariable">
                    <xsl:with-param name="theVariableID" select="'Optional Step'"/>
                </xsl:call-template>
                <xsl:text> </xsl:text>
            </xsl:if>
            <xsl:apply-templates/>
        </fo:block>
    </xsl:template>

    <xsl:template match="*[contains(@class, ' task/info ')]">
        <fo:block xsl:use-attribute-sets="stepelement">
            <xsl:call-template name="commonattributes"/>
            <!--avdh  2017-11-23 added prefixtitle for stepinfo-->
            <!--23-01-2018 decision not to use these titles-->
            <!--<fo:block xsl:use-attribute-sets="prefix.title">
                <xsl:call-template name="insertVariable">
                    <xsl:with-param name="theVariableID" select="'StepInfo'"/>
                </xsl:call-template>
            </fo:block>-->
            <xsl:apply-templates/>
        </fo:block>
    </xsl:template>

    <xsl:template match="*[contains(@class, ' task/tutorialinfo ')]">
        <fo:block xsl:use-attribute-sets="stepelement">
            <xsl:call-template name="commonattributes"/>
            <xsl:apply-templates/>
        </fo:block>
    </xsl:template>

    <xsl:template match="*[contains(@class, ' task/stepresult ')]">
        <fo:block xsl:use-attribute-sets="stepelement">
            <xsl:call-template name="commonattributes"/>
            <!--avdh  2017-11-23 added prefixtitle for stepresult-->
            <!--23-01-2018 decision not to use these titles-->
            <!--<fo:block xsl:use-attribute-sets="prefix.title">
                <xsl:call-template name="insertVariable">
                    <xsl:with-param name="theVariableID" select="'StepResult'"/>
                </xsl:call-template>
            </fo:block>-->
            <xsl:apply-templates/>
        </fo:block>
    </xsl:template>

    <xsl:template match="*[contains(@class, ' task/result ')]">
        <fo:block xsl:use-attribute-sets="result">
            <xsl:call-template name="commonattributes"/>
            <xsl:apply-templates select="." mode="dita2xslfo:task-heading">
                <xsl:with-param name="use-label">
                    <xsl:call-template name="insertVariable">
                        <xsl:with-param name="theVariableID" select="'Task Result'"/>
                    </xsl:call-template>
                </xsl:with-param>
            </xsl:apply-templates>
            <fo:block xsl:use-attribute-sets="result__content">
                <xsl:apply-templates/>
            </fo:block>
        </fo:block>
    </xsl:template>

    <!-- If example has a title, process it first; otherwise, create default title (if needed) -->
    <xsl:template
        match="*[contains(@class, ' task/taskbody ')]/*[contains(@class, ' topic/example ')]">
        <fo:block xsl:use-attribute-sets="task.example">
            <xsl:call-template name="commonattributes"/>
            <xsl:choose>
                <xsl:when test="*[contains(@class, ' topic/title ')]">
                    <xsl:apply-templates select="*[contains(@class, ' topic/title ')]"/>
                </xsl:when>
                <xsl:otherwise>
                    <xsl:apply-templates select="." mode="dita2xslfo:task-heading">
                        <xsl:with-param name="use-label">
                            <xsl:call-template name="insertVariable">
                                <xsl:with-param name="theVariableID" select="'Task Example'"/>
                            </xsl:call-template>
                        </xsl:with-param>
                    </xsl:apply-templates>
                </xsl:otherwise>
            </xsl:choose>
            <fo:block xsl:use-attribute-sets="task.example__content">
                <!--<fo:block xsl:use-attribute-sets="prefix.title">
                    <xsl:call-template name="insertVariable">
                        <xsl:with-param name="theVariableID" select="'Example'"/>
                    </xsl:call-template>
                </fo:block>-->
                <xsl:apply-templates select="*[not(contains(@class, ' topic/title '))] | text() | processing-instruction()"/>
            </fo:block>
        </fo:block>
    </xsl:template>

    <xsl:template match="*[contains(@class, ' task/postreq ')]">
        <fo:block xsl:use-attribute-sets="postreq">
            <xsl:call-template name="commonattributes"/>
            <fo:block xsl:use-attribute-sets="prefix.title">
                <xsl:call-template name="insertVariable">
                    <xsl:with-param name="theVariableID" select="'prelreq closereqs'"/>
                </xsl:call-template>
            </fo:block>
            <!--<xsl:apply-templates select="." mode="dita2xslfo:task-heading">
                <xsl:with-param name="use-label">
                    <xsl:call-template name="insertVariable">
                        <xsl:with-param name="theVariableID" select="'prelreq closereqs'"/>
                    </xsl:call-template>
                </xsl:with-param>
            </xsl:apply-templates>-->
            <fo:block xsl:use-attribute-sets="postreq__content">
                <xsl:apply-templates/>
            </fo:block>
        </fo:block>
    </xsl:template>

    <xsl:template match="*[contains(@class, ' task/stepxmp ')]">
        <fo:block xsl:use-attribute-sets="stepelement">
            <xsl:call-template name="commonattributes"/>
            <!--avdh 2017-11-23 added prefixtitle for stepxmp-->
            <!--23-01-2018 decision not to use these titles-->
            <!--<fo:block xsl:use-attribute-sets="prefix.title">
                <xsl:call-template name="insertVariable">
                    <xsl:with-param name="theVariableID" select="'StepExample'"/>
                </xsl:call-template>
            </fo:block>-->
            <xsl:apply-templates/>
        </fo:block>
    </xsl:template>

    <!--Steps-->
    <xsl:template match="*[contains(@class, ' task/steps ')]">
        <!--avdh: 20170607, only publish the title when prelreqs are in the task-->
        <xsl:choose>
            <xsl:when
                test="ancestor::*[contains(@class, 'task/task')][descendant::*[contains(@class, 'taskreq-d/prelreqs')]]">
                <fo:block xsl:use-attribute-sets="prefix.title">
                    <xsl:call-template name="insertVariable">
                        <xsl:with-param name="theVariableID" select="'steps'"/>
                    </xsl:call-template>
                </fo:block>
            </xsl:when>
            <xsl:otherwise/>
        </xsl:choose>
        <xsl:choose>
            <xsl:when test="$GENERATE-TASK-LABELS = 'YES'">
                <fo:block>
                    <xsl:apply-templates select="." mode="dita2xslfo:task-heading">
                        <xsl:with-param name="use-label">
                            <xsl:call-template name="insertVariable">
                                <xsl:with-param name="theVariableID" select="'Task Steps'"/>
                            </xsl:call-template>
                        </xsl:with-param>
                    </xsl:apply-templates>
                    <fo:list-block xsl:use-attribute-sets="steps">
                        <xsl:call-template name="commonattributes"/>
                        <xsl:apply-templates/>
                    </fo:list-block>
                </fo:block>
            </xsl:when>
            <xsl:otherwise>
                <xsl:comment>LABELS</xsl:comment>
                <fo:list-block xsl:use-attribute-sets="steps">
                    <xsl:call-template name="commonattributes"/>
                    <xsl:apply-templates/>
                </fo:list-block>
            </xsl:otherwise>
        </xsl:choose>
    </xsl:template>

    <xsl:template match="*[contains(@class, ' task/steps-unordered ')]">
        <xsl:choose>
            <xsl:when test="$GENERATE-TASK-LABELS = 'YES'">
                <fo:block>
                    <xsl:apply-templates select="." mode="dita2xslfo:task-heading">
                        <xsl:with-param name="use-label">
                            <xsl:call-template name="insertVariable">
                                <xsl:with-param name="theVariableID"
                                    select="'#steps-unordered-label'"/>
                            </xsl:call-template>
                        </xsl:with-param>
                    </xsl:apply-templates>
                    <fo:list-block xsl:use-attribute-sets="steps-unordered">
                        <xsl:call-template name="commonattributes"/>
                        <xsl:apply-templates/>
                    </fo:list-block>
                </fo:block>
            </xsl:when>
            <xsl:otherwise>
                <fo:list-block xsl:use-attribute-sets="steps-unordered">
                    <xsl:call-template name="commonattributes"/>
                    <xsl:apply-templates/>
                </fo:list-block>
            </xsl:otherwise>
        </xsl:choose>
    </xsl:template>

    <xsl:template match="*[contains(@class, ' task/steps ')]/*[contains(@class, ' task/step ')]">
        <xsl:variable name="actual-step-count" select="number(count(preceding-sibling::*[contains(@class, ' task/step ')]) + 1)"/>
        <fo:list-item xsl:use-attribute-sets="steps.step">
            <!-- 20171025 avdh: lex 06-06-2016: keep rules -->
            <xsl:choose>
                <!--                lex 21-07-2016: commented to keep the step always together
                    <xsl:when
                    test="descendant::*[contains(@class, ' topic/ol ') or contains(@class, ' topic/ul ')]"/>-->
                <xsl:when
                    test="not(preceding-sibling::*[contains(@class, ' task/step ')]) and not(following-sibling::*[contains(@class, ' task/step ')])">
				</xsl:when>
                <!--31-01-2018 avdh: only keep step with next when following sibling is not a stepsection with an image element
                TODO!!!!!!!!!!!!!!!!-->
                <xsl:when
                    test="not(preceding-sibling::*[contains(@class, ' task/step ')]) and not(following-sibling::*[contains(@class, 'task/stepsection')])">
                    <xsl:attribute name="keep-with-next.within-page">100</xsl:attribute>
                </xsl:when>
                <xsl:when test="not(following-sibling::*[contains(@class, ' task/step ')])">
                    <xsl:attribute name="keep-with-previous.within-page">100</xsl:attribute>
                </xsl:when>
            </xsl:choose>
            <!-- lex 06-06-2016: keep together when it doesn't have a child ol/ul -->
            <!--            <xsl:if
                test="not(descendant::*[contains(@class, ' topic/ol ') or contains(@class, ' topic/ul ')])">
                <xsl:attribute name="keep-together.within-column">500</xsl:attribute>
            </xsl:if>-->

            <fo:list-item-label xsl:use-attribute-sets="steps.step__label">
                <fo:block xsl:use-attribute-sets="steps.step__label__content">
                    <fo:inline>
                        <xsl:call-template name="commonattributes"/>
                    </fo:inline>
                    <xsl:call-template name="insertVariable">
                        <xsl:with-param name="theVariableID" select="'Ordered List Number'"/>
                        <xsl:with-param name="theParameters">
                            <number>
                                <xsl:value-of select="$actual-step-count"/>
                            </number>
                        </xsl:with-param>
                    </xsl:call-template>
                </fo:block>
            </fo:list-item-label>
            <fo:list-item-body xsl:use-attribute-sets="steps.step__body">
                <fo:block xsl:use-attribute-sets="steps.step__content">
                    <xsl:apply-templates/>
                </fo:block>
            </fo:list-item-body>
        </fo:list-item>
    </xsl:template>

    <!--<xsl:template match="*[contains(@class, ' task/steps ')]/*[contains(@class, ' task/step ')]">
        <!-\- Switch to variable for the count rather than xsl:number, so that step specializations are also counted -\->
        
        <xsl:variable name="actual-step-count" select="number(count(preceding-sibling::*[contains(@class, ' task/step ')])+1)"/>
        
        <fo:list-item xsl:use-attribute-sets="steps.step">
		
		            <!-\- lex 06-06-2016: keep rules -\->
            <xsl:choose>
<!-\-                lex 21-07-2016: commented to keep the step always together
                    <xsl:when
                    test="descendant::*[contains(@class, ' topic/ol ') or contains(@class, ' topic/ul ')]"/>-\->
                <xsl:when
                    test="not(preceding-sibling::*[contains(@class, ' task/step ')]) and not(following-sibling::*[contains(@class, ' task/step ')])">
                </xsl:when>
                <xsl:when test="not(preceding-sibling::*[contains(@class, ' task/step ')])">
                    <xsl:attribute name="keep-with-next.within-page">500</xsl:attribute>
                </xsl:when>
                <xsl:when test="not(following-sibling::*[contains(@class, ' task/step ')])">
                    <xsl:attribute name="keep-with-previous.within-page">500</xsl:attribute>
                </xsl:when>
            </xsl:choose>
		
		
            <fo:list-item-label xsl:use-attribute-sets="steps.step__label">
                <fo:block xsl:use-attribute-sets="steps.step__label__content">
                    <fo:inline>
                        <xsl:call-template name="commonattributes"/>
                    </fo:inline>
                 <!-\-   <!-\\-20170301, avdh: added choose, when only 1 step in an ordered task, publich dash and no number-\\->
                    <xsl:choose>
                        <xsl:when test="preceding-sibling::*[contains(@class, ' task/step ')] | following-sibling::*[contains(@class, ' task/step ')]">                            
                            <xsl:call-template name="insertVariable">
                                <xsl:with-param name="theVariableID" select="'Ordered List Number'"/>
                                <xsl:with-param name="theParameters">
                                    <number>
                                        <xsl:value-of select="$actual-step-count"/>
                                    </number>
                                </xsl:with-param>
                            </xsl:call-template>
                        </xsl:when>
                        <xsl:otherwise>
                            <xsl:call-template name="insertVariable">
                                <xsl:with-param name="theVariableID" select="'Unordered List bullet'"/>
                            </xsl:call-template>
                        </xsl:otherwise>
                    </xsl:choose>-\->
                    
                    <fo:list-item-label xsl:use-attribute-sets="steps.step__label">
                        <fo:block xsl:use-attribute-sets="steps.step__label__content">
                            <fo:inline>
                                <xsl:call-template name="commonattributes"/>
                            </fo:inline>
                            <xsl:call-template name="insertVariable">
                                <xsl:with-param name="theVariableID" select="'Ordered List Number'"/>
                                <xsl:with-param name="theParameters">
                                    <number>
                                        <xsl:value-of select="$actual-step-count"/>
                                    </number>
                                </xsl:with-param>
                            </xsl:call-template>
                        </fo:block>
                    </fo:list-item-label>
                </fo:block>
            </fo:list-item-label>

            <fo:list-item-body xsl:use-attribute-sets="steps.step__body">
                <fo:block xsl:use-attribute-sets="steps.step__content">
                    <xsl:apply-templates/>
                </fo:block>
            </fo:list-item-body>

        </fo:list-item>
    </xsl:template>-->

    <xsl:template
        match="*[contains(@class, ' task/steps-unordered ')]/*[contains(@class, ' task/step ')]">
        <fo:list-item xsl:use-attribute-sets="steps-unordered.step">
            <fo:list-item-label xsl:use-attribute-sets="steps-unordered.step__label">
                <fo:block xsl:use-attribute-sets="steps-unordered.step__label__content">
                    <fo:inline>
                        <xsl:call-template name="commonattributes"/>
                    </fo:inline>
                    <xsl:call-template name="insertVariable">
                        <xsl:with-param name="theVariableID" select="'Unordered List bullet'"/>
                    </xsl:call-template>
                </fo:block>
            </fo:list-item-label>

            <fo:list-item-body xsl:use-attribute-sets="steps-unordered.step__body">
                <fo:block xsl:use-attribute-sets="steps-unordered.step__content">
                    <xsl:apply-templates/>
                </fo:block>
            </fo:list-item-body>

        </fo:list-item>
    </xsl:template>

    <xsl:template match="*[contains(@class, ' task/stepsection ')]">
        <fo:list-item xsl:use-attribute-sets="stepsection">
            <!-- lex 28-01-2016: changed handeling label -->
            <fo:list-item-label xsl:use-attribute-sets="stepsection__label"/>

            <fo:list-item-body xsl:use-attribute-sets="stepsection__body">
			<!-- lex 28-01-2015: added:  20151210 avdh: 
                to publish a graphic next to the text of the steps,  a stepsection holds the graphic that supports the step(s). 
                When there is nog graphic to support the step, the stepsection is empty, text is published full page-->

                <xsl:choose>
                    <xsl:when test="child::image">
                        <fo:float float="end" space-before="0pt" clear="both">
                            <fo:block>
                                <xsl:apply-templates select="*[contains(@class, ' topic/image ')]"/>
                            </fo:block>
                        </fo:float>
                    </xsl:when>
                </xsl:choose>
<!--
                <xsl:apply-templates select="*[not(contains(@class, ' topic/image '))]"/>
-->
				<fo:block xsl:use-attribute-sets="stepsection__content">
                    <xsl:apply-templates/>
                </fo:block>
            </fo:list-item-body>
        </fo:list-item>
    </xsl:template>

    <!--Substeps-->
    <xsl:template match="*[contains(@class, ' task/substeps ')]">
        <fo:list-block xsl:use-attribute-sets="substeps">
            <xsl:call-template name="commonattributes"/>
            <xsl:apply-templates/>
        </fo:list-block>
    </xsl:template>

    <xsl:template
        match="*[contains(@class, ' task/substeps ')]/*[contains(@class, ' task/substep ')]">
        <fo:list-item xsl:use-attribute-sets="substeps.substep">
            <fo:list-item-label xsl:use-attribute-sets="substeps.substep__label">
                <fo:block xsl:use-attribute-sets="substeps.substep__label__content">
                    <fo:inline>
                        <xsl:call-template name="commonattributes"/>
                    </fo:inline>
                    <xsl:number format="a. "/>
                </fo:block>
            </fo:list-item-label>
            <fo:list-item-body xsl:use-attribute-sets="substeps.substep__body">
                <fo:block xsl:use-attribute-sets="substeps.substep__content">
                    <xsl:apply-templates/>
                </fo:block>
            </fo:list-item-body>
        </fo:list-item>
    </xsl:template>

    <!--Choices-->
    <xsl:template match="*[contains(@class, ' task/choices ')]">
        <fo:block xsl:use-attribute-sets="stepelement">
            <xsl:call-template name="commonattributes"/>
            <!--23-01-2018 decision not to use these titles-->
            <!--<fo:block xsl:use-attribute-sets="prefix.title">
            <xsl:call-template name="insertVariable">
                <xsl:with-param name="theVariableID" select="'Choices'"/>
        </xsl:call-template>
    </fo:block>-->
            <fo:list-block xsl:use-attribute-sets="choices">
                <xsl:call-template name="commonattributes"/>
                <xsl:apply-templates/>
            </fo:list-block>
        </fo:block>
    </xsl:template>

    <xsl:template match="*[contains(@class, ' task/choice ')]">
        <fo:list-item xsl:use-attribute-sets="choices.choice">
            <fo:list-item-label xsl:use-attribute-sets="choices.choice__label">
                <fo:block xsl:use-attribute-sets="choices.choice__label__content">
                    <fo:inline>
                        <xsl:call-template name="commonattributes"/>
                    </fo:inline>
                    <xsl:call-template name="insertVariable">
                        <xsl:with-param name="theVariableID" select="'Unordered List bullet'"/>
                    </xsl:call-template>
                </fo:block>
            </fo:list-item-label>
            <fo:list-item-body xsl:use-attribute-sets="choices.choice__body">
                <fo:block xsl:use-attribute-sets="choices.choice__content">
                    <xsl:apply-templates/>
                </fo:block>
            </fo:list-item-body>
        </fo:list-item>
    </xsl:template>

    <!-- Choice tables -->
    <xsl:template match="*[contains(@class, ' task/choicetable ')]">
        <fo:block xsl:use-attribute-sets="stepelement">
            <xsl:call-template name="commonattributes"/>

            <!--23-01-2018 decision not to use these titles-->
            <!--<fo:block xsl:use-attribute-sets="prefix.title">
          <xsl:call-template name="insertVariable">
              <xsl:with-param name="theVariableID" select="'ChoiceTable'"/>
          </xsl:call-template>
      </fo:block>-->
            <fo:table xsl:use-attribute-sets="choicetable">
                <xsl:call-template name="commonattributes"/>
                <xsl:call-template name="univAttrs"/>
                <xsl:call-template name="globalAtts"/>

                <xsl:call-template name="displayAtts">
                    <xsl:with-param name="element" select=".."/>
                </xsl:call-template>

                <xsl:if test="@relcolwidth">
                    <xsl:variable name="fix-relcolwidth">
                        <xsl:apply-templates select="." mode="fix-relcolwidth"/>
                    </xsl:variable>
                    <xsl:call-template name="createSimpleTableColumns">
                        <xsl:with-param name="theColumnWidthes" select="$fix-relcolwidth"/>
                    </xsl:call-template>
                </xsl:if>

                <xsl:choose>
                    <xsl:when test="*[contains(@class, ' task/chhead ')]">
                        <xsl:apply-templates select="*[contains(@class, ' task/chhead ')]"/>
                    </xsl:when>
                    <xsl:otherwise>
                        <fo:table-header xsl:use-attribute-sets="chhead">
                            <fo:table-row xsl:use-attribute-sets="chhead__row">
                                <fo:table-cell xsl:use-attribute-sets="chhead.choptionhd">
                                    <fo:block xsl:use-attribute-sets="chhead.choptionhd__content">
                                        <xsl:call-template name="insertVariable">
                                            <xsl:with-param name="theVariableID" select="'Option'"/>
                                        </xsl:call-template>
                                    </fo:block>
                                </fo:table-cell>
                                <fo:table-cell xsl:use-attribute-sets="chhead.chdeschd">
                                    <fo:block xsl:use-attribute-sets="chhead.chdeschd__content">
                                        <xsl:call-template name="insertVariable">
                                            <xsl:with-param name="theVariableID"
                                                select="'Description'"/>
                                        </xsl:call-template>
                                    </fo:block>
                                </fo:table-cell>
                            </fo:table-row>
                        </fo:table-header>
                    </xsl:otherwise>
                </xsl:choose>

                <fo:table-body xsl:use-attribute-sets="choicetable__body">
                    <xsl:apply-templates select="*[contains(@class, ' task/chrow ')]"/>
                </fo:table-body>

            </fo:table>
        </fo:block>
    </xsl:template>

    <xsl:template match="*[contains(@class, ' task/chhead ')]">
        <fo:table-header xsl:use-attribute-sets="chhead">
            <xsl:call-template name="commonattributes"/>
            <fo:table-row xsl:use-attribute-sets="chhead__row">
                <xsl:apply-templates/>
            </fo:table-row>
        </fo:table-header>
    </xsl:template>

    <xsl:template match="*[contains(@class, ' task/chrow ')]">
        <fo:table-row xsl:use-attribute-sets="chrow">
            <xsl:call-template name="commonattributes"/>
            <xsl:apply-templates/>
        </fo:table-row>
    </xsl:template>

    <xsl:template
        match="*[contains(@class, ' task/chhead ')]/*[contains(@class, ' task/choptionhd ')]">
        <fo:table-cell xsl:use-attribute-sets="chhead.choptionhd">
            <xsl:call-template name="commonattributes"/>
            <fo:block xsl:use-attribute-sets="chhead.choptionhd__content">
                <xsl:apply-templates/>
            </fo:block>
        </fo:table-cell>
    </xsl:template>

    <xsl:template
        match="*[contains(@class, ' task/chhead ')]/*[contains(@class, ' task/chdeschd ')]">
        <fo:table-cell xsl:use-attribute-sets="chhead.chdeschd">
            <xsl:call-template name="commonattributes"/>
            <fo:block xsl:use-attribute-sets="chhead.chdeschd__content">
                <xsl:apply-templates/>
            </fo:block>
        </fo:table-cell>
    </xsl:template>

    <xsl:template match="*[contains(@class, ' task/chrow ')]/*[contains(@class, ' task/choption ')]">
        <xsl:variable name="keyCol"
            select="ancestor::*[contains(@class, ' task/choicetable ')][1]/@keycol"/>
        <fo:table-cell xsl:use-attribute-sets="chrow.choption">
            <xsl:call-template name="commonattributes"/>
            <xsl:choose>
                <xsl:when test="$keyCol = 1">
                    <fo:block xsl:use-attribute-sets="chrow.choption__keycol-content">
                        <xsl:apply-templates/>
                    </fo:block>
                </xsl:when>
                <xsl:otherwise>
                    <fo:block xsl:use-attribute-sets="chrow.choption__content">
                        <xsl:apply-templates/>
                    </fo:block>
                </xsl:otherwise>
            </xsl:choose>
        </fo:table-cell>
    </xsl:template>

    <xsl:template match="*[contains(@class, ' task/chrow ')]/*[contains(@class, ' task/chdesc ')]">
        <xsl:variable name="keyCol"
            select="number(ancestor::*[contains(@class, ' task/choicetable ')][1]/@keycol)"/>
        <fo:table-cell xsl:use-attribute-sets="chrow.chdesc">
            <xsl:call-template name="commonattributes"/>
            <xsl:choose>
                <xsl:when test="$keyCol = 2">
                    <fo:block xsl:use-attribute-sets="chrow.chdesc__keycol-content">
                        <xsl:apply-templates/>
                    </fo:block>
                </xsl:when>
                <xsl:otherwise>
                    <fo:block xsl:use-attribute-sets="chrow.chdesc__content">
                        <xsl:apply-templates/>
                    </fo:block>
                </xsl:otherwise>
            </xsl:choose>
        </fo:table-cell>
    </xsl:template>

    <!-- Example -->
    <!--
  <xsl:template match="*[contains(@class, ' topic/example ')]" mode="dita2xslfo:task-heading">
    <xsl:param name="use-label"/>
    <xsl:if test="$GENERATE-TASK-LABELS='YES'">
<!-\- lex 28-01-2016: changed example.title to prefix.title from model A -\->
        <fo:block xsl:use-attribute-sets="prefix.title">
        <fo:inline>
          <xsl:copy-of select="$use-label"/>
        </fo:inline>
      </fo:block>
    </xsl:if>
  </xsl:template>
-->
    <xsl:template match="*" mode="dita2xslfo:task-heading">
        <xsl:param name="use-label"/>
        <xsl:if test="$GENERATE-TASK-LABELS = 'YES'">
            <fo:block xsl:use-attribute-sets="section.title">
                <fo:inline>
                    <xsl:copy-of select="$use-label"/>
                </fo:inline>
            </fo:block>
        </xsl:if>
    </xsl:template>
	
	<!-- mpu 2020-02-20: added handling for noconds -->
    <xsl:template match="*[contains(@class, ' taskreq-d/noconds ')]">
		<fo:block>
			<xsl:call-template name="insertVariable">
				<xsl:with-param name="theVariableID" select="'prelreq noconds'"/>
			</xsl:call-template>
		</fo:block>
    </xsl:template>	
	
	<!-- mpu 2020-02-20: added handling for nosupeq -->
    <xsl:template match="*[contains(@class, ' taskreq-d/nosupeq ')]">
		<fo:block>
			<xsl:call-template name="insertVariable">
				<xsl:with-param name="theVariableID" select="'prelreq nosupeq'"/>
			</xsl:call-template>
		</fo:block>
    </xsl:template>
	
	<!-- mpu 2020-02-20: added handling for nosupply -->
    <xsl:template match="*[contains(@class, ' taskreq-d/nosupply ')]">
		<fo:block>
			<xsl:call-template name="insertVariable">
				<xsl:with-param name="theVariableID" select="'prelreq nosupply'"/>
			</xsl:call-template>
		</fo:block>
    </xsl:template>

	<!-- mpu 2020-02-20: added handling for nospares -->
    <xsl:template match="*[contains(@class, ' taskreq-d/nospares ')]">
		<fo:block>
			<xsl:call-template name="insertVariable">
				<xsl:with-param name="theVariableID" select="'prelreq nospares'"/>
			</xsl:call-template>
		</fo:block>
    </xsl:template>
	
	<!-- mpu 2020-02-20: added handling for nosafety -->
    <xsl:template match="*[contains(@class, ' taskreq-d/nosafety ')]">
        <fo:list-item>
            <fo:list-item-label>
                <fo:block/>
            </fo:list-item-label>
            <fo:list-item-body>
                <fo:block>
                    <fo:block>
                        <xsl:call-template name="insertVariable">
                            <xsl:with-param name="theVariableID" select="'prelreq nosafety'"/>
                        </xsl:call-template>
                    </fo:block>
                </fo:block>
            </fo:list-item-body>
        </fo:list-item>
    </xsl:template>

</xsl:stylesheet>
