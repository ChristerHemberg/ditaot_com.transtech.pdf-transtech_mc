﻿<?xml version='1.0'?>

<xsl:stylesheet xmlns:xsl="http://www.w3.org/1999/XSL/Transform"
	xmlns:fo="http://www.w3.org/1999/XSL/Format" xmlns:opentopic="http://www.idiominc.com/opentopic"
	exclude-result-prefixes="opentopic" version="2.0"
	xmlns:ditaarch="http://dita.oasis-open.org/architecture/2005/">

	<xsl:variable name="SmallCase"
		select="'abcdefghijklmnopqrstuvwxyzàáâãäåæçèéêëìíîïðñòóôõöøùúûüýþÿžšœ'"/>
	<xsl:variable name="UpperCase"
		select="'ABCDEFGHIJKLMNOPQRSTUVWXYZÀÁÂÃÄÅÆÇÈÉÊËÌÍÎÏÐÑÒÓÔÕÖØÙÚÛÜÝÞŸŽŠŒ'"/>
	<xsl:variable name="stopset" select='"()*%$#@!~&lt;&gt;&apos;,.?[]=-+/\ "'/>

	<xsl:template match="*[contains(@class, ' map/topicmeta ')]">
		<fo:block-container xsl:use-attribute-sets="__frontmatter__owner__container">
			<xsl:apply-templates/>
		</fo:block-container>
	</xsl:template>

	<xsl:template match="*[contains(@class, ' topic/author ')]">
		<fo:block xsl:use-attribute-sets="author">
			<xsl:apply-templates/>
		</fo:block>
	</xsl:template>

	<xsl:template match="*[contains(@class, ' topic/publisher ')]">
		<fo:block xsl:use-attribute-sets="publisher">
			<xsl:apply-templates/>
		</fo:block>
	</xsl:template>

	<xsl:template match="*[contains(@class, ' topic/copyright ')]">
		<fo:block xsl:use-attribute-sets="copyright">
			<xsl:apply-templates/>
		</fo:block>
	</xsl:template>

	<xsl:template match="*[contains(@class, ' topic/copyryear ')]">
		<fo:inline xsl:use-attribute-sets="copyryear">
			<xsl:value-of select="@year"/>
			<xsl:text> </xsl:text>
		</fo:inline>
	</xsl:template>

	<xsl:template match="*[contains(@class, ' topic/copyrholder ')]">
		<fo:inline xsl:use-attribute-sets="copyrholder">
			<xsl:apply-templates/>
		</fo:inline>
	</xsl:template>

	<xsl:template name="processCopyrigth">
		<xsl:apply-templates select="/bookmap/*[contains(@class, ' topic/topic ')]"
			mode="process-preface"/>
	</xsl:template>

	<xsl:template match="*[contains(@class, ' topic/topic ')]" mode="process-preface">
		<xsl:param name="include" select="'true'"/>
		<xsl:variable name="topicType">
			<xsl:call-template name="determineTopicType"/>
		</xsl:variable>
	</xsl:template>

	<xsl:param name="ditaVersion"
		select="number(/*[contains(@class, ' map/map ')]/@ditaarch:DITAArchVersion)"/>

	<xsl:template name="createFrontMatter">
		<fo:page-sequence master-reference="front-matter" force-page-count="even">
			<!--xsl:use-attribute-sets="__force__page__count"-->
			<xsl:call-template name="insertFrontMatterStaticContents"/>
			<fo:flow flow-name="xsl-region-body">
				<xsl:variable name="cover"
					select="//*[contains(@class, ' topic/topic ') and contains(@outputclass, 'cover')][1]"/>
				<xsl:choose>
					<xsl:when test="$cover">
						<!-- Add logo. -->
						<fo:block text-align="center">
							<fo:external-graphic content-height="30mm"
								src="url(Customization/OpenTopic/common/artwork/logo.eps)"/>
						</fo:block>

						<!-- Process CoverTopic. -->
						<fo:block xsl:use-attribute-sets="__frontmatter">
							<xsl:apply-templates select="$cover/*[contains(@class, ' topic/title ')]" mode="fctcover"/>
							<!--<xsl:apply-templates select="$cover" mode="fctcover"/>-->
						</fo:block>

						<!-- Add document type and draft label. -->
						<fo:block-container xsl:use-attribute-sets="doctype">
							<fo:block>
								<fo:inline>
									<!--DOCUMENT TYPE-->
									<xsl:variable name="doctype">
										<xsl:value-of select="//opentopic:map/topicmeta/metadata/othermeta[@name = 'DOCTYPE']/@content"/>
									</xsl:variable>
									<!-- make doctype smallcase -->
									<xsl:variable name="doctypeLC" select="translate($doctype, $UpperCase, $SmallCase)"/>
									<!-- clean the doctype from divers chars. -->
									<xsl:variable name="doctypeclean" select="translate($doctypeLC, $stopset, '')"/>
									<xsl:call-template name="insertVariable">
										<xsl:with-param name="theVariableID" select="$doctypeclean"/>
									</xsl:call-template>
								</fo:inline>
								<xsl:apply-templates select="$cover/*[contains(@class, ' topic/body ')]/*[@outputclass = 'subtitle1']" mode="fctcover"/>
								<!--   09-01-2018 avdh: add term DRAFT to the frontpage-->
								<xsl:choose>
									<xsl:when test="$DRAFTPUB">
										<fo:inline> - DRAFT</fo:inline>
									</xsl:when>
								</xsl:choose>
							</fo:block>
						</fo:block-container>

						<!-- Add imprint. -->
						<xsl:for-each
							select="//*[contains(@class, ' topic/topic ') and not(contains(@outputclass, 'backmatter')) and contains(@outputclass, 'cover')][position() > 1]">
							<fo:block xsl:use-attribute-sets="__frontmatter__imprint">
								<xsl:apply-templates select="." mode="fctimprint"/>
							</fo:block>
						</xsl:for-each>
					</xsl:when>

					<xsl:otherwise>
						<!-- Add logo. -->
						<fo:block text-align="center">
							<fo:external-graphic content-height="30mm"
								src="url(Customization/OpenTopic/common/artwork/logo.eps)"/>
						</fo:block>

						<!-- Set front matter content. -->
						<fo:block xsl:use-attribute-sets="__frontmatter">
							<!-- Set the title. -->
							<fo:block xsl:use-attribute-sets="__frontmatter__title">
								<xsl:choose>
									<xsl:when test="$map/*[contains(@class, ' topic/title ')][1]">
										<xsl:apply-templates select="$map/*[contains(@class, ' topic/title ')][1]"/>
									</xsl:when>
									<xsl:when
										test="$map//*[contains(@class, ' bookmap/mainbooktitle ')][1]">
										<xsl:apply-templates select="$map//*[contains(@class, ' bookmap/mainbooktitle ')][1]"
										/>
									</xsl:when>
									<xsl:when test="//*[contains(@class, ' map/map ')]/@title">
										<xsl:value-of select="//*[contains(@class, ' map/map ')]/@title"/>
									</xsl:when>
									<xsl:otherwise>
										<xsl:value-of select="/descendant::*[contains(@class, ' topic/topic ')][1]/*[contains(@class, ' topic/title ')]"
										/>
									</xsl:otherwise>
								</xsl:choose>
							</fo:block>

							<!-- Set the document type. -->
							<fo:block-container xsl:use-attribute-sets="doctype">
								<fo:block>
									<fo:inline>
										<xsl:value-of
											select="//opentopic:map/topicmeta/metadata/othermeta[@name = 'DOCTYPE']/@content"
										/>
									</fo:inline>
									<!--   09-01-2018 avdh: add term DRAFT to the frontpage-->
									<xsl:choose>
										<xsl:when test="$DRAFTPUB">
											<fo:inline> - DRAFT</fo:inline>
										</xsl:when>
									</xsl:choose>
								</fo:block>
							</fo:block-container>

							<xsl:apply-templates
								select="$map//*[contains(@class, ' bookmap/booktitlealt ')]"/>
							<fo:block xsl:use-attribute-sets="__frontmatter__owner">
								<xsl:apply-templates
									select="$map//*[contains(@class, ' bookmap/bookmeta ')]"/>
							</fo:block>
						</fo:block>
					</xsl:otherwise>
				</xsl:choose>
			</fo:flow>
		</fo:page-sequence>

		<xsl:if test="not($retain-bookmap-order)">
			<xsl:call-template name="createNotices"/>
		</xsl:if>
	</xsl:template>

	<xsl:template match="//*[contains(@class, ' topic/topic ') and contains(@outputclass, 'cover')]"/>

	<!--COVERTOPIC-->
	<xsl:template match="*[contains(@class, ' topic/title ')]" mode="fctcover">
		<fo:block xsl:use-attribute-sets="__frontmatter__title">
			<xsl:apply-templates/>
		</fo:block>
	</xsl:template>

	<xsl:template match="*[contains(@class, ' topic/body ')]/*[@outputclass = 'subtitle1']" mode="fctcover" priority="1">
		<fo:block xsl:use-attribute-sets="__frontmatter__subtitle1">
			<xsl:apply-templates/>
		</fo:block>
	</xsl:template>

	<!--
	<xsl:template match="*[contains(@class, ' topic/body ')]/*[@outputclass='subtitle2']" mode="fctcover" priority="1">
		<fo:block xsl:use-attribute-sets="__frontmatter__subtitle2">
			<xsl:apply-templates/>
		</fo:block>
	</xsl:template>
    <xsl:template match="*[contains(@class, ' topic/body ')]/*[@outputclass='subtitle3']" mode="fctcover" priority="1">
		<fo:block xsl:use-attribute-sets="__frontmatter__subtitle3">
			<xsl:apply-templates/>
		</fo:block>
	</xsl:template>
    <xsl:template match="*[contains(@class, ' topic/body ')]/*[@outputclass='subtitle4']" mode="fctcover" priority="1">
		<fo:block xsl:use-attribute-sets="__frontmatter__subtitle4">
			<xsl:apply-templates/>
		</fo:block>
	</xsl:template>
	-->

	<xsl:template match="*[contains(@class, ' topic/body ')]/*" mode="fctcover">
		<fo:block xsl:use-attribute-sets="__frontmatter__para">
			<xsl:apply-templates/>
		</fo:block>
	</xsl:template>
	<xsl:template match="*[contains(@class, ' topic/prolog ')]" mode="fctcover"/>

	<xsl:template match="*[contains(@class, ' topic/title ')]" mode="fctimprint">
		<fo:block xsl:use-attribute-sets="__frontmatter__imprint_title">
			<xsl:apply-templates/>
		</fo:block>
	</xsl:template>
	<xsl:template match="*[contains(@class, ' topic/body ')]/*" mode="fctimprint">
		<fo:block xsl:use-attribute-sets="__frontmatter__imprint_para">
			<xsl:apply-templates/>
		</fo:block>
	</xsl:template>
	<xsl:template match="*[contains(@class, ' topic/prolog ')]" mode="fctimprint"/>
	<xsl:template match="*[contains(@class, ' bookmap/bookmeta ')]" priority="1">
		<fo:block-container xsl:use-attribute-sets="__frontmatter__owner__container">
			<fo:block>
				<xsl:apply-templates/>
			</fo:block>
		</fo:block-container>
	</xsl:template>

	<xsl:template match="*[contains(@class, ' bookmap/booktitlealt ')]" priority="2">
		<fo:block xsl:use-attribute-sets="__frontmatter__subtitle">
			<xsl:apply-templates/>
		</fo:block>
	</xsl:template>

	<xsl:template match="*[contains(@class, ' bookmap/booktitle ')]" priority="2">
		<fo:block xsl:use-attribute-sets="__frontmatter__booklibrary">
			<xsl:apply-templates select="*[contains(@class, ' bookmap/booklibrary ')]"/>
		</fo:block>
		<fo:block xsl:use-attribute-sets="__frontmatter__mainbooktitle">
			<xsl:apply-templates select="*[contains(@class, ' bookmap/mainbooktitle ')]"/>
		</fo:block>
	</xsl:template>

	<xsl:template match="*[contains(@class, ' xnal-d/namedetails ')]">
		<fo:block>
			<xsl:apply-templates/>
		</fo:block>
	</xsl:template>

	<xsl:template match="*[contains(@class, ' xnal-d/addressdetails ')]">
		<fo:block>
			<xsl:apply-templates/>
		</fo:block>
	</xsl:template>

	<xsl:template match="*[contains(@class, ' xnal-d/contactnumbers ')]">
		<fo:block>
			<xsl:apply-templates/>
		</fo:block>
	</xsl:template>

	<xsl:template match="*[contains(@class, ' bookmap/bookowner ')]">
		<fo:block xsl:use-attribute-sets="author">
			<xsl:apply-templates/>
		</fo:block>
	</xsl:template>

	<xsl:template match="*[contains(@class, ' bookmap/summary ')]">
		<fo:block xsl:use-attribute-sets="bookmap.summary">
			<xsl:apply-templates/>
		</fo:block>
	</xsl:template>

	<xsl:template name="createNotices">
		<xsl:apply-templates select="/bookmap/*[contains(@class, ' topic/topic ')]"
			mode="process-notices"/>
	</xsl:template>

	<xsl:template match="*[contains(@class, ' topic/topic ')]" mode="process-notices">
		<xsl:variable name="topicType">
			<xsl:call-template name="determineTopicType"/>
		</xsl:variable>
		<xsl:if test="$topicType = 'topicNotices'">
			<xsl:call-template name="processTopicNotices"/>
		</xsl:if>
	</xsl:template>

</xsl:stylesheet>
