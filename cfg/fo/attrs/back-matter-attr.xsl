﻿<?xml version='1.0'?>
<xsl:stylesheet xmlns:xsl="http://www.w3.org/1999/XSL/Transform"
    xmlns:fo="http://www.w3.org/1999/XSL/Format" version="2.0">

    <xsl:attribute-set name="__backmatter">
        <xsl:attribute name="text-align">center</xsl:attribute>
        <xsl:attribute name="absolute-position">fixed</xsl:attribute>
        <xsl:attribute name="top">200mm</xsl:attribute>
    </xsl:attribute-set>

</xsl:stylesheet>
