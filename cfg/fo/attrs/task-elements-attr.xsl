﻿<?xml version='1.0'?>

<!--
Copyright © 2004-2006 by Idiom Technologies, Inc. All rights reserved.
IDIOM is a registered trademark of Idiom Technologies, Inc. and WORLDSERVER
and WORLDSTART are trademarks of Idiom Technologies, Inc. All other
trademarks are the property of their respective owners.

IDIOM TECHNOLOGIES, INC. IS DELIVERING THE SOFTWARE "AS IS," WITH
ABSOLUTELY NO WARRANTIES WHATSOEVER, WHETHER EXPRESS OR IMPLIED,  AND IDIOM
TECHNOLOGIES, INC. DISCLAIMS ALL WARRANTIES, EXPRESS OR IMPLIED, INCLUDING
BUT NOT LIMITED TO WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR
PURPOSE AND WARRANTY OF NON-INFRINGEMENT. IDIOM TECHNOLOGIES, INC. SHALL NOT
BE LIABLE FOR INDIRECT, INCIDENTAL, SPECIAL, COVER, PUNITIVE, EXEMPLARY,
RELIANCE, OR CONSEQUENTIAL DAMAGES (INCLUDING BUT NOT LIMITED TO LOSS OF
ANTICIPATED PROFIT), ARISING FROM ANY CAUSE UNDER OR RELATED TO  OR ARISING
OUT OF THE USE OF OR INABILITY TO USE THE SOFTWARE, EVEN IF IDIOM
TECHNOLOGIES, INC. HAS BEEN ADVISED OF THE POSSIBILITY OF SUCH DAMAGES.

Idiom Technologies, Inc. and its licensors shall not be liable for any
damages suffered by any person as a result of using and/or modifying the
Software or its derivatives. In no event shall Idiom Technologies, Inc.'s
liability for any damages hereunder exceed the amounts received by Idiom
Technologies, Inc. as a result of this transaction.

These terms and conditions supersede the terms and conditions in any
licensing agreement to the extent that such terms and conditions conflict
with those set forth herein.

This file is part of the DITA Open Toolkit project hosted on Sourceforge.net.
See the accompanying license.txt file for applicable licenses.
-->

<xsl:stylesheet xmlns:xsl="http://www.w3.org/1999/XSL/Transform"
    xmlns:fo="http://www.w3.org/1999/XSL/Format" version="2.0">
    <!-- lex 28-01-2015: added attr set tasktable -->
    <xsl:attribute-set name="tasktable" use-attribute-sets="simpletable">
        <xsl:attribute name="width">100%</xsl:attribute>
        <xsl:attribute name="space-before">0pt</xsl:attribute>
        <xsl:attribute name="space-after">0pt</xsl:attribute>
    </xsl:attribute-set>

    <!-- <xsl:attribute-set name="task">
        <!-\-2018-03-06 avdh: for draft opublication color of released modules is blue-\->
        <!-\-2018-06-08 avdh: removed hte colored text-\->
        <xsl:attribute name="color">
            <xsl:choose>
                <xsl:when test="$DRAFTPUB and ancestor::*[contains(@class, 'task/task')]/@props='Released'">blue</xsl:when>
                <xsl:when test="$DRAFTPUB and @props='Released' ">blue</xsl:when>
                <xsl:otherwise>black</xsl:otherwise>
            </xsl:choose>
        </xsl:attribute>
    </xsl:attribute-set>-->

    <xsl:attribute-set name="taskbody" use-attribute-sets="body"> </xsl:attribute-set>

    <xsl:attribute-set name="prelreq">
        <xsl:attribute name="start-indent">0mm</xsl:attribute>		
    </xsl:attribute-set>

    <xsl:attribute-set name="prelreqicon">
        <xsl:attribute name="width">10mm</xsl:attribute>
        <xsl:attribute name="content-width">scale-to-fit</xsl:attribute>
        <!--        <xsl:attribute name="content-width">15mm</xsl:attribute>-->
    </xsl:attribute-set>

    <xsl:attribute-set name="prelreqrow">
        <xsl:attribute name="keep-together.within-page">100</xsl:attribute>
    </xsl:attribute-set>

    <xsl:attribute-set name="prereq" use-attribute-sets="section"> </xsl:attribute-set>
    <xsl:attribute-set name="prereq__content" use-attribute-sets="section__content"> </xsl:attribute-set>
    
    <xsl:attribute-set name="reqconds" use-attribute-sets="section"> </xsl:attribute-set>
    <xsl:attribute-set name="reqconds__content" use-attribute-sets="section__content"> </xsl:attribute-set>
    
    <xsl:attribute-set name="reqcond" use-attribute-sets="section"> </xsl:attribute-set>
    <xsl:attribute-set name="reqcond__content" use-attribute-sets="section__content"> </xsl:attribute-set>

    <xsl:attribute-set name="context" use-attribute-sets="section"> </xsl:attribute-set>
    <xsl:attribute-set name="context__content" use-attribute-sets="section__content"> </xsl:attribute-set>

    <xsl:attribute-set name="cmd"> </xsl:attribute-set>

    <!--avdh 07-12-2017-->
    <xsl:attribute-set name="stepelement">
        <xsl:attribute name="space-before">3pt</xsl:attribute>
        <xsl:attribute name="padding-after">3pt</xsl:attribute>
    </xsl:attribute-set>
    <!--avdh 07-12-2017-->
    <xsl:attribute-set name="stepelementp">
        <xsl:attribute name="space-before">0em</xsl:attribute>
        <xsl:attribute name="space-after">0em</xsl:attribute>
    </xsl:attribute-set>



    <xsl:attribute-set name="result" use-attribute-sets="section"> </xsl:attribute-set>
    <xsl:attribute-set name="result__content" use-attribute-sets="section__content"> </xsl:attribute-set>

    <!-- lex 28-01-2015: changed example to example.task-->
    <xsl:attribute-set name="task.example" use-attribute-sets="example.task"> </xsl:attribute-set>

    <!-- lex 28-01-2015: added italic font-style -->
    <xsl:attribute-set name="task.example__content" use-attribute-sets="example__content">
        <xsl:attribute name="font-style">italic</xsl:attribute>
    </xsl:attribute-set>

    <xsl:attribute-set name="postreq" use-attribute-sets="section"> </xsl:attribute-set>
    <xsl:attribute-set name="postreq__content" use-attribute-sets="section__content"> </xsl:attribute-set>

    <!--added-->
    <xsl:attribute-set name="supequip" use-attribute-sets="section">
        <xsl:attribute name="keep-together.within-column">500</xsl:attribute>
        <!-- lex 30-05-2016: added -->
    </xsl:attribute-set>
    <xsl:attribute-set name="supequip__content" use-attribute-sets="section__content"> </xsl:attribute-set>

    <xsl:attribute-set name="nosupeq" use-attribute-sets="section__content">
        <xsl:attribute name="space-before">6pt</xsl:attribute>
        <xsl:attribute name="space-after">6pt</xsl:attribute>
    </xsl:attribute-set>

    <xsl:attribute-set name="supplies" use-attribute-sets="section">
        <xsl:attribute name="keep-together.within-column">500</xsl:attribute>
        <!-- lex 30-05-2016: added -->
    </xsl:attribute-set>
    <xsl:attribute-set name="supplies__content" use-attribute-sets="section__content"> </xsl:attribute-set>

    <xsl:attribute-set name="nosupply" use-attribute-sets="section__content">
        <xsl:attribute name="space-before">6pt</xsl:attribute>
        <xsl:attribute name="space-after">6pt</xsl:attribute>
    </xsl:attribute-set>

    <xsl:attribute-set name="spares" use-attribute-sets="section">
        <xsl:attribute name="keep-together.within-column">500</xsl:attribute>
        <!-- lex 30-05-2016: added -->
    </xsl:attribute-set>
    <xsl:attribute-set name="spares__content" use-attribute-sets="section__content"> </xsl:attribute-set>

    <xsl:attribute-set name="nospares" use-attribute-sets="section__content">
        <xsl:attribute name="space-before">6pt</xsl:attribute>
        <xsl:attribute name="space-after">6pt</xsl:attribute>
    </xsl:attribute-set>

    <!--Unordered steps-->
    <xsl:attribute-set name="steps-unordered" use-attribute-sets="ul"> </xsl:attribute-set>

    <xsl:attribute-set name="steps-unordered.step" use-attribute-sets="ul.li"> </xsl:attribute-set>

    <xsl:attribute-set name="steps-unordered.step__label" use-attribute-sets="ul.li__label"> </xsl:attribute-set>

    <xsl:attribute-set name="steps-unordered.step__label__content"
        use-attribute-sets="ul.li__label__content"> </xsl:attribute-set>

    <xsl:attribute-set name="steps-unordered.step__body" use-attribute-sets="ul.li__body"> </xsl:attribute-set>

    <xsl:attribute-set name="steps-unordered.step__content" use-attribute-sets="ul.li__content"> </xsl:attribute-set>

    <!--Ordered steps-->
    <xsl:attribute-set name="steps" use-attribute-sets="ol">
        <!--avdh 07-12-2017-->
        <xsl:attribute name="space-before">0pt</xsl:attribute>		
    </xsl:attribute-set>

    <!--avdh 07-12-2017: removed  use-attribute-sets="ol.li" we want no extra space before or after steps-->
    <xsl:attribute-set name="steps.step">
        <!--use-attribute-sets="ol.li"-->
        <xsl:attribute name="space-after">6pt</xsl:attribute>
        <xsl:attribute name="space-before">0pt</xsl:attribute>
        <xsl:attribute name="keep-together.within-page">101</xsl:attribute><!--101-->
    </xsl:attribute-set>

    <xsl:attribute-set name="steps.step__label" use-attribute-sets="ol.li__label"> </xsl:attribute-set>

    <xsl:attribute-set name="steps.step__label__content" use-attribute-sets="ol.li__label__content"> </xsl:attribute-set>

    <xsl:attribute-set name="steps.step__body" use-attribute-sets="ol.li__body"> </xsl:attribute-set>

    <xsl:attribute-set name="steps.step__content" use-attribute-sets="ol.li__content"> </xsl:attribute-set>
    
	<xsl:attribute-set name="stepsection" use-attribute-sets="ul.li">
        <xsl:attribute name="padding-top">2mm</xsl:attribute>
		<xsl:attribute name="space-after">2pt</xsl:attribute>
        <xsl:attribute name="space-before">2pt</xsl:attribute>
        <!--2017-11-03 avdh:-->
        <xsl:attribute name="keep-with-next.within-page">100</xsl:attribute>
        <!-- 20160506 avdh added clear=both -->
        <xsl:attribute name="clear">both</xsl:attribute>
    </xsl:attribute-set>

    <xsl:attribute-set name="stepsection__label" use-attribute-sets="ul.li__label"> </xsl:attribute-set>

    <xsl:attribute-set name="stepsection__label__content" use-attribute-sets="ul.li__label__content"> </xsl:attribute-set>

    <xsl:attribute-set name="stepsection__body" use-attribute-sets="ul.li__body">
        <xsl:attribute name="start-indent">0mm</xsl:attribute>
    </xsl:attribute-set>

    <xsl:attribute-set name="stepsection__content" use-attribute-sets="ul.li__content"> </xsl:attribute-set>

    <!--Substeps-->
    <xsl:attribute-set name="substeps" use-attribute-sets="ol">
        <xsl:attribute name="space-after">3pt</xsl:attribute>
        <xsl:attribute name="space-before">3pt</xsl:attribute>
		<xsl:attribute name="padding-after">3mm</xsl:attribute>
    </xsl:attribute-set>

    <xsl:attribute-set name="substeps.substep" use-attribute-sets="ol.li">
		<xsl:attribute name="keep-together.within-page">102</xsl:attribute>
	</xsl:attribute-set>

    <xsl:attribute-set name="substeps.substep__label" use-attribute-sets="ol.li__label"> </xsl:attribute-set>

    <xsl:attribute-set name="substeps.substep__label__content"
        use-attribute-sets="ol.li__label__content">
        <xsl:attribute name="font-weight">normal</xsl:attribute>
    </xsl:attribute-set>

    <xsl:attribute-set name="substeps.substep__body" use-attribute-sets="ol.li__body"> </xsl:attribute-set>

    <xsl:attribute-set name="substeps.substep__content" use-attribute-sets="ol.li__content"> </xsl:attribute-set>



    <xsl:attribute-set name="choices.choice__label" use-attribute-sets="ul.li__label"> </xsl:attribute-set>

    <xsl:attribute-set name="choices.choice__label__content"
        use-attribute-sets="ul.li__label__content"> </xsl:attribute-set>

    <xsl:attribute-set name="choices.choice__body" use-attribute-sets="ul.li__body"> </xsl:attribute-set>

    <xsl:attribute-set name="choices.choice__content" use-attribute-sets="ul.li__content"> </xsl:attribute-set>

    <xsl:attribute-set name="choicetable" use-attribute-sets="base-font">
        <!-- table__tableframe__all-->
        <!--It is a table container -->
        <xsl:attribute name="width">100%</xsl:attribute>
        <xsl:attribute name="space-before">3pt</xsl:attribute>
        <xsl:attribute name="space-after">0pt</xsl:attribute>
    </xsl:attribute-set>

    <xsl:attribute-set name="choicetable__body"/>

    <xsl:attribute-set name="chhead"/>

    <!--avdh-->
    <xsl:attribute-set name="chhead__row" use-attribute-sets="table__tableframe__all"/>

    <!--avdh-->
    <xsl:attribute-set name="chrow" use-attribute-sets="table__tableframe__all"/>
    <!--avdh added frames and background-color-->
    <xsl:attribute-set name="chhead.choptionhd" use-attribute-sets="table__tableframe__all">
        <xsl:attribute name="background-color">lightgrey</xsl:attribute>
    </xsl:attribute-set>

    <xsl:attribute-set name="chhead.choptionhd__content"
        use-attribute-sets="common.table.body.entry common.table.head.entry"/>
    <!--avdh added frames and background-color-->
    <xsl:attribute-set name="chhead.chdeschd" use-attribute-sets="table__tableframe__all">
        <xsl:attribute name="background-color">lightgrey</xsl:attribute>
    </xsl:attribute-set>

    <xsl:attribute-set name="chhead.chdeschd__content"
        use-attribute-sets="common.table.body.entry common.table.head.entry"/>
    <!--avdh-->
    <xsl:attribute-set name="chrow.choption" use-attribute-sets="table__tableframe__all"/>

    <xsl:attribute-set name="chrow.choption__keycol-content"
        use-attribute-sets="common.table.body.entry common.table.head.entry"/>

    <xsl:attribute-set name="chrow.choption__content" use-attribute-sets="common.table.body.entry"/>
    <!--avdh-->
    <xsl:attribute-set name="chrow.chdesc" use-attribute-sets="table__tableframe__all"/>

    <xsl:attribute-set name="chrow.chdesc__keycol-content"
        use-attribute-sets="common.table.body.entry common.table.head.entry"/>

    <xsl:attribute-set name="chrow.chdesc__content" use-attribute-sets="common.table.body.entry"/>

</xsl:stylesheet>
