﻿<?xml version='1.0'?>
<xsl:stylesheet xmlns:xsl="http://www.w3.org/1999/XSL/Transform"
  xmlns:fo="http://www.w3.org/1999/XSL/Format" version="2.0">

  <xsl:include href="commons-attr.xsl"/>
  <xsl:include href="back-matter-attr.xsl"/>
  <xsl:include href="toc-attr.xsl"/>
  <xsl:include href="layout-masters-attr.xsl"/>
  <xsl:include href="static-content-attr.xsl"/>
  <xsl:include href="front-matter-attr.xsl"/>
  <xsl:include href="hazard-domain-attr.xsl"/>
  <xsl:include href="basic-settings.xsl"/>
  <xsl:include href="ui-domain-attr.xsl"/>
  <xsl:include href="hi-domain-attr.xsl"/>
  <xsl:include href="tables-attr.xsl"/>
  <xsl:include href="tables-attr_fop.xsl"/>
  <xsl:include href="tables-attr_axf.xsl"/>
  <xsl:include href="lists-attr.xsl"/>
  <xsl:include href="task-elements-attr.xsl"/>
  <xsl:include href="attr-set-reflection.xsl"/>

  <!-- lex 28-01-2016: add attr-set-reflection.xsl  -->

  <!--  <xsl:include href="paragraphs.xsl"/>-->

  <!--  <xsl:include href="layout-masters-attr.xsl"/>-->




</xsl:stylesheet>
