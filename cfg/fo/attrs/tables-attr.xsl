﻿<?xml version="1.0"?>

<xsl:stylesheet xmlns:xsl="http://www.w3.org/1999/XSL/Transform"
    xmlns:fo="http://www.w3.org/1999/XSL/Format" version="2.0">

    <!-- contents of table entries or similer structures -->
    <xsl:attribute-set name="common.table.body.entry">
        <xsl:attribute name="space-before">3pt</xsl:attribute>
        <xsl:attribute name="space-before.conditionality">retain</xsl:attribute>
        <xsl:attribute name="space-after">3pt</xsl:attribute>
        <xsl:attribute name="space-after.conditionality">retain</xsl:attribute>
        <xsl:attribute name="start-indent">3pt</xsl:attribute>
        <xsl:attribute name="end-indent">3pt</xsl:attribute>
    </xsl:attribute-set>

    <xsl:attribute-set name="common.table.head.entry">
        <xsl:attribute name="font-weight">bold</xsl:attribute>
        <xsl:attribute name="color">white</xsl:attribute>
    </xsl:attribute-set>

    <xsl:attribute-set name="table.title" use-attribute-sets="base-font common.title">
        <xsl:attribute name="font-weight">bold</xsl:attribute>
        <xsl:attribute name="space-before">12pt</xsl:attribute>
        <xsl:attribute name="space-after">0pt</xsl:attribute>
        <xsl:attribute name="keep-with-next.within-column">always</xsl:attribute>
    </xsl:attribute-set>

    <xsl:attribute-set name="__tableframe__none"/>

    <xsl:attribute-set name="__tableframe__top" use-attribute-sets="common.border__top"> </xsl:attribute-set>

    <xsl:attribute-set name="__tableframe__bottom" use-attribute-sets="common.border__bottom">
        <xsl:attribute name="border-after-width.conditionality">retain</xsl:attribute>
    </xsl:attribute-set>

    <xsl:attribute-set name="thead__tableframe__bottom" use-attribute-sets="common.border__bottom"> </xsl:attribute-set>

    <xsl:attribute-set name="__tableframe__left" use-attribute-sets="common.border__left"> </xsl:attribute-set>

    <xsl:attribute-set name="__tableframe__right" use-attribute-sets="common.border__right"> </xsl:attribute-set>

    <!--changed attributeset-->
    <xsl:attribute-set name="table" use-attribute-sets="base-font-table">
        <!--It is a table container -->
        <xsl:attribute name="space-after">10pt</xsl:attribute>
    </xsl:attribute-set>

    <xsl:attribute-set name="table.tgroup">
        <!--It is a table-->
        <xsl:attribute name="table-layout">fixed</xsl:attribute>
        <xsl:attribute name="width">100%</xsl:attribute>
        <!--xsl:attribute name=&quot;inline-progression-dimension&quot;&gt;auto&lt;/xsl:attribute-->
        <!--        &lt;xsl:attribute name=&quot;background-color&quot;&gt;white&lt;/xsl:attribute&gt;-->
        <xsl:attribute name="space-before">5pt</xsl:attribute>
        <xsl:attribute name="space-after">5pt</xsl:attribute>
    </xsl:attribute-set>
    
    <xsl:attribute-set name="tbody.row.entry.common">
        <xsl:attribute name="font-size">16pt</xsl:attribute>
        <xsl:attribute name="font-weight">bold</xsl:attribute>
        <xsl:attribute name="text-transform">uppercase</xsl:attribute>
    </xsl:attribute-set> 

    <xsl:attribute-set name="tbody.row.entry.danger" use-attribute-sets="tbody.row.entry.common">
        <xsl:attribute name="background-color">rgb(188,0,24)</xsl:attribute>
        <xsl:attribute name="color">white</xsl:attribute>
    </xsl:attribute-set> 

    <xsl:attribute-set name="tbody.row.entry.warning" use-attribute-sets="tbody.row.entry.common">
        <xsl:attribute name="background-color">rgb(227,108,10)</xsl:attribute>
    </xsl:attribute-set>
    
    <xsl:attribute-set name="tbody.row.entry.caution" use-attribute-sets="tbody.row.entry.common">
        <xsl:attribute name="background-color">rgb(255,237,0)</xsl:attribute>
    </xsl:attribute-set> 

    <!-- Lex 23-09-2015: added tbody.row.entry.middle -->
    <xsl:attribute-set name="tbody.row.entry.middle">
        <xsl:attribute name="display-align">center</xsl:attribute>
    </xsl:attribute-set>

    <!-- Lex 30-09-2015: added tbody.row.entry.center -->
    <xsl:attribute-set name="tbody.row.entry.center">
        <xsl:attribute name="text-align">center</xsl:attribute>
    </xsl:attribute-set>

    <!-- lex 28-01-2015: added tasktable set -->
    <xsl:attribute-set name="tasktable">
        <xsl:attribute name="border">solid white 0.5pt</xsl:attribute>
    </xsl:attribute-set>

    <xsl:attribute-set name="table__tableframe__all"
        use-attribute-sets="table__tableframe__topbot table__tableframe__sides"> </xsl:attribute-set>

    <xsl:attribute-set name="table__tableframe__topbot"
        use-attribute-sets="table__tableframe__top table__tableframe__bottom"> </xsl:attribute-set>

    <xsl:attribute-set name="table__tableframe__top" use-attribute-sets="common.border__top"> </xsl:attribute-set>

    <xsl:attribute-set name="table__tableframe__bottom" use-attribute-sets="common.border__bottom"> </xsl:attribute-set>

    <xsl:attribute-set name="table__tableframe__sides"
        use-attribute-sets="table__tableframe__right table__tableframe__left"> </xsl:attribute-set>

    <xsl:attribute-set name="table__tableframe__right" use-attribute-sets="common.border__right"> </xsl:attribute-set>

    <xsl:attribute-set name="table__tableframe__left" use-attribute-sets="common.border__left"> </xsl:attribute-set>

    <xsl:attribute-set name="tgroup.tbody">
        <!--Table body-->
    </xsl:attribute-set>

    <xsl:attribute-set name="tgroup.thead">
        <!--Table head-->
    </xsl:attribute-set>

    <xsl:attribute-set name="tgroup.tfoot">
        <!--Table footer-->
    </xsl:attribute-set>

    <xsl:attribute-set name="thead.row">
        <!--Head row-->
        <xsl:attribute name="keep-together.within-page">100</xsl:attribute>
    </xsl:attribute-set>

    <xsl:attribute-set name="tfoot.row">
        <!--Table footer-->
        <xsl:attribute name="keep-together.within-page">100</xsl:attribute>
    </xsl:attribute-set>

    <xsl:attribute-set name="tbody.row">
        <!--Table body row-->
        <xsl:attribute name="keep-together.within-page">100</xsl:attribute>
    </xsl:attribute-set>

    <xsl:attribute-set name="thead.row.entry">
        <!--head cell-->
        <xsl:attribute name="background-color">rgb(0,150,214)</xsl:attribute>
        <!--antiquewhite-->
    </xsl:attribute-set>

    <xsl:attribute-set name="thead.row.entry__content"
        use-attribute-sets="common.table.body.entry common.table.head.entry">
        <!--head cell contents-->
    </xsl:attribute-set>

    <xsl:attribute-set name="tfoot.row.entry">
        <!--footer cell-->
    </xsl:attribute-set>

    <xsl:attribute-set name="tfoot.row.entry__content"
        use-attribute-sets="common.table.body.entry common.table.head.entry">
        <!--footer cell contents-->
    </xsl:attribute-set>

    <xsl:attribute-set name="tbody.row.entry">
        <!--body cell-->
    </xsl:attribute-set>

    <!-- Lex 30-09-2015: added tbody.row.entry.center -->
    <xsl:attribute-set name="tbody.row.entry.center">
        <xsl:attribute name="text-align">center</xsl:attribute>
    </xsl:attribute-set>

    <xsl:attribute-set name="tbody.row.entry__content" use-attribute-sets="common.table.body.entry">
        <!-- body cell contents-->
        <!-- added attributes for lesser space around text in tables-->
        <xsl:attribute name="space-before">2pt</xsl:attribute>
        <xsl:attribute name="space-after">2pt</xsl:attribute>
        <xsl:attribute name="end-indent">2pt</xsl:attribute>
    </xsl:attribute-set>

    <xsl:attribute-set name="dl">
        <!--DL is a table-->
        <xsl:attribute name="width">100%</xsl:attribute>
        <xsl:attribute name="space-before">5pt</xsl:attribute>
        <xsl:attribute name="space-after">5pt</xsl:attribute>
    </xsl:attribute-set>

    <xsl:attribute-set name="dl__body"> </xsl:attribute-set>

    <xsl:attribute-set name="dl.dlhead"> </xsl:attribute-set>

    <xsl:attribute-set name="dlentry"> </xsl:attribute-set>

    <xsl:attribute-set name="dlentry.dt">
        <xsl:attribute name="relative-align">baseline</xsl:attribute>
    </xsl:attribute-set>

    <xsl:attribute-set name="dlentry.dt__content"
        use-attribute-sets="common.table.body.entry common.table.head.entry"> </xsl:attribute-set>

    <xsl:attribute-set name="dlentry.dd"/>

    <xsl:attribute-set name="dlentry.dd__content" use-attribute-sets="common.table.body.entry"> </xsl:attribute-set>

    <xsl:attribute-set name="dl.dlhead__row"> </xsl:attribute-set>

    <xsl:attribute-set name="dlhead.dthd__cell"> </xsl:attribute-set>

    <xsl:attribute-set name="dlhead.dthd__content"
        use-attribute-sets="common.table.body.entry common.table.head.entry"> </xsl:attribute-set>

    <xsl:attribute-set name="dlhead.ddhd__cell"> </xsl:attribute-set>

    <xsl:attribute-set name="dlhead.ddhd__content"
        use-attribute-sets="common.table.body.entry common.table.head.entry"> </xsl:attribute-set>

    <xsl:attribute-set name="simpletable" use-attribute-sets="base-font">
        <!--It is a table container -->
        <xsl:attribute name="width">100%</xsl:attribute>
        <xsl:attribute name="space-before">8pt</xsl:attribute>
        <xsl:attribute name="space-after">10pt</xsl:attribute>
    </xsl:attribute-set>

    <xsl:attribute-set name="simpletable__body"> </xsl:attribute-set>

    <xsl:attribute-set name="sthead"> </xsl:attribute-set>

    <xsl:attribute-set name="sthead__row"> </xsl:attribute-set>

    <xsl:attribute-set name="strow"> </xsl:attribute-set>

    <xsl:attribute-set name="sthead.stentry"> </xsl:attribute-set>

    <xsl:attribute-set name="sthead.stentry__content"
        use-attribute-sets="common.table.body.entry common.table.head.entry"> </xsl:attribute-set>

    <xsl:attribute-set name="sthead.stentry__keycol-content"
        use-attribute-sets="common.table.body.entry common.table.head.entry">
        <xsl:attribute name="background-color">lightgrey</xsl:attribute>
        <!--antiquewhite-->
    </xsl:attribute-set>

    <xsl:attribute-set name="strow.stentry__content" use-attribute-sets="common.table.body.entry"> </xsl:attribute-set>

    <xsl:attribute-set name="strow.stentry__keycol-content"
        use-attribute-sets="common.table.body.entry common.table.head.entry">
        <xsl:attribute name="background-color">lightgrey</xsl:attribute>
        <!--antiquewhite-->
    </xsl:attribute-set>

    <xsl:attribute-set name="strow.stentry"> </xsl:attribute-set>

    <xsl:attribute-set name="prelreqtable" use-attribute-sets="base-font table__tableframe__all">
        <!--It is a table container -->
        <xsl:attribute name="width">100%</xsl:attribute>
        <xsl:attribute name="space-before">8pt</xsl:attribute>
        <xsl:attribute name="space-after">10pt</xsl:attribute>
    </xsl:attribute-set>

    <xsl:attribute-set name="prelreq.entry"
        use-attribute-sets="__tableframe__bottom __tableframe__right __tableframe__left">
        <xsl:attribute name="padding">4pt</xsl:attribute>

    </xsl:attribute-set>

</xsl:stylesheet>
