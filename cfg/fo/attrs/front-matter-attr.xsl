﻿<?xml version='1.0'?>


<xsl:stylesheet xmlns:xsl="http://www.w3.org/1999/XSL/Transform"
    xmlns:fo="http://www.w3.org/1999/XSL/Format" version="2.0">

    <xsl:attribute-set name="__frontmatter">
        <xsl:attribute name="text-align">center</xsl:attribute>
    </xsl:attribute-set>


    <xsl:attribute-set name="__frontmatter__documenttype" use-attribute-sets="common.title">
        <!--<xsl:attribute name="space-before">30mm</xsl:attribute>-->
        <!--40mm-->
        <!--<xsl:attribute name="space-before.conditionality">retain</xsl:attribute>-->
        <xsl:attribute name="space-after">6mm</xsl:attribute>
        <!--15mm-->
        <xsl:attribute name="font-size">32pt</xsl:attribute>
        <xsl:attribute name="font-weight">bold</xsl:attribute>
        <xsl:attribute name="line-height">140%</xsl:attribute>
        <xsl:attribute name="text-align">center</xsl:attribute>
        <xsl:attribute name="absolute-position">absolute</xsl:attribute>
        <xsl:attribute name="top">37mm</xsl:attribute>
        <!--        <xsl:attribute name="color">red</xsl:attribute>-->
    </xsl:attribute-set>

    <xsl:attribute-set name="__frontmatter__productname" use-attribute-sets="common.title">
        <xsl:attribute name="font-size">22pt</xsl:attribute>
        <xsl:attribute name="font-weight">bold</xsl:attribute>
        <xsl:attribute name="line-height">140%</xsl:attribute>
        <xsl:attribute name="text-align">center</xsl:attribute>
        <xsl:attribute name="color">black</xsl:attribute>
        <!--rgb-icc(#CMYK,0.0,0.85,1.0,0.0)-->
    </xsl:attribute-set>

    <xsl:attribute-set name="__frontmatter__graphic">
        <xsl:attribute name="border">0pt solid black</xsl:attribute>
        <xsl:attribute name="content-height">130mm</xsl:attribute>
        <xsl:attribute name="content-width">scale-to-fit</xsl:attribute>
    </xsl:attribute-set>

    <xsl:attribute-set name="__frontmatter__title">
        <xsl:attribute name="space-before">5mm</xsl:attribute>
        <xsl:attribute name="space-after">5mm</xsl:attribute>
        <xsl:attribute name="font-size">20pt</xsl:attribute>
        <xsl:attribute name="font-weight">bold</xsl:attribute>
        <xsl:attribute name="line-height">140%</xsl:attribute>
        <xsl:attribute name="text-align">center</xsl:attribute>
        <xsl:attribute name="color">rgb(0,63,126)</xsl:attribute>
    </xsl:attribute-set>

    <xsl:attribute-set name="__frontmatter__subtitle1" use-attribute-sets="common.title">
        <xsl:attribute name="space-before">3mm</xsl:attribute>
        <xsl:attribute name="space-after">5mm</xsl:attribute>
        <xsl:attribute name="font-size">26pt</xsl:attribute>
        <xsl:attribute name="font-weight">bold</xsl:attribute>
        <xsl:attribute name="line-height">140%</xsl:attribute>
        <xsl:attribute name="text-align">center</xsl:attribute>
        <xsl:attribute name="color">rgb(0,63,126)</xsl:attribute>
        <!--<xsl:attribute name="space-before">2mm</xsl:attribute>
        <xsl:attribute name="space-after">5mm</xsl:attribute>
        <xsl:attribute name="font-size">20pt</xsl:attribute>
        <xsl:attribute name="font-weight">bold</xsl:attribute>
        <xsl:attribute name="color">rgb(0,63,126)</xsl:attribute>-->
    </xsl:attribute-set>

    <xsl:attribute-set name="__frontmatter__subtitle2" use-attribute-sets="common.title">
        <xsl:attribute name="space-before">15mm</xsl:attribute>
        <xsl:attribute name="space-after">4mm</xsl:attribute>
        <xsl:attribute name="font-size">18pt</xsl:attribute>
        <xsl:attribute name="font-weight">bold</xsl:attribute>
    </xsl:attribute-set>

    <xsl:attribute-set name="__frontmatter__subtitle3" use-attribute-sets="common.title">
        <xsl:attribute name="space-before">4mm</xsl:attribute>
        <xsl:attribute name="space-after">4mm</xsl:attribute>
        <xsl:attribute name="font-size">16pt</xsl:attribute>
        <xsl:attribute name="font-weight">bold</xsl:attribute>
    </xsl:attribute-set>

    <xsl:attribute-set name="__frontmatter__subtitle4" use-attribute-sets="common.title">
        <xsl:attribute name="space-before">4mm</xsl:attribute>
        <xsl:attribute name="space-after">15mm</xsl:attribute>
        <xsl:attribute name="font-size">14pt</xsl:attribute>
        <xsl:attribute name="font-weight">bold</xsl:attribute>
    </xsl:attribute-set>

    <xsl:attribute-set name="__frontmatter__para">
        <xsl:attribute name="font-size">11pt</xsl:attribute>
    </xsl:attribute-set>

    <xsl:attribute-set name="__frontmatter__owner" use-attribute-sets="common.title">
        <xsl:attribute name="space-before">36pt</xsl:attribute>
        <xsl:attribute name="font-size">11pt</xsl:attribute>
        <xsl:attribute name="font-weight">bold</xsl:attribute>
        <xsl:attribute name="line-height">normal</xsl:attribute>
    </xsl:attribute-set>

    <xsl:attribute-set name="__frontmatter__owner__container">
        <xsl:attribute name="position">absolute</xsl:attribute>
        <xsl:attribute name="top">210mm</xsl:attribute>
        <xsl:attribute name="bottom">20mm</xsl:attribute>
        <xsl:attribute name="right">20mm</xsl:attribute>
        <xsl:attribute name="left">20mm</xsl:attribute>
    </xsl:attribute-set>

    <xsl:attribute-set name="__frontmatter__owner__container_content">
        <xsl:attribute name="text-align">center</xsl:attribute>
    </xsl:attribute-set>

    <xsl:attribute-set name="__frontmatter__mainbooktitle">
        <!--<xsl:attribute name=""></xsl:attribute>-->
    </xsl:attribute-set>

    <xsl:attribute-set name="__frontmatter__booklibrary">
        <!--<xsl:attribute name=""></xsl:attribute>-->
    </xsl:attribute-set>

    <xsl:attribute-set name="__frontmatter__imprint">
        <xsl:attribute name="break-before">page</xsl:attribute>
        <!--<xsl:attribute name="padding-before">200mm</xsl:attribute>-->
    </xsl:attribute-set>

    <xsl:attribute-set name="docdata">
        <xsl:attribute name="absolute-position">absolute</xsl:attribute>
        <xsl:attribute name="top">255mm</xsl:attribute>
    </xsl:attribute-set>

    <xsl:attribute-set name="doctype">
        <xsl:attribute name="space-before">2mm</xsl:attribute>
        <xsl:attribute name="space-after">5mm</xsl:attribute>
        <xsl:attribute name="font-size">20pt</xsl:attribute>
        <xsl:attribute name="font-weight">bold</xsl:attribute>
        <xsl:attribute name="color">rgb(0,63,126)</xsl:attribute>
        <xsl:attribute name="text-align">center</xsl:attribute>
<!--        <xsl:attribute name="space-before">3mm</xsl:attribute>
        <xsl:attribute name="space-after">5mm</xsl:attribute>
        <xsl:attribute name="font-size">26pt</xsl:attribute>
        <xsl:attribute name="font-weight">bold</xsl:attribute>
        <xsl:attribute name="line-height">140%</xsl:attribute>
        <xsl:attribute name="text-align">center</xsl:attribute>
        <xsl:attribute name="color">rgb(0,63,126)</xsl:attribute>-->
    </xsl:attribute-set>

    <xsl:attribute-set name="__frontmatter__imprint_title" use-attribute-sets="common.title">
        <!--<xsl:attribute name="space-before">160mm</xsl:attribute>-->
        <xsl:attribute name="space-before.conditionality">retain</xsl:attribute>
        <xsl:attribute name="space-after">7mm</xsl:attribute>
        <xsl:attribute name="font-size">14pt</xsl:attribute>
        <xsl:attribute name="font-weight">bold</xsl:attribute>
    </xsl:attribute-set>

    <xsl:attribute-set name="__frontmatter__imprint_para">
        <xsl:attribute name="font-size">11pt</xsl:attribute>
    </xsl:attribute-set>


    <xsl:attribute-set name="bookmap.summary">
        <xsl:attribute name="font-size">9pt</xsl:attribute>
    </xsl:attribute-set>

</xsl:stylesheet>
