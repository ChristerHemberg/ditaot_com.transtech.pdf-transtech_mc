﻿<?xml version="1.0"?>

<xsl:stylesheet xmlns:xsl="http://www.w3.org/1999/XSL/Transform" version="2.0">
    <!--Frank: 05-09-2017: changed the appearence of the element uicontrol to regular and italic-->
    <!--avdh: 25-06-2018: correction on font-weight: value must be 'normal'-->
    <xsl:attribute-set name="uicontrol">
        <xsl:attribute name="font-weight">normal</xsl:attribute>
        <xsl:attribute name="font-style">italic</xsl:attribute>
    </xsl:attribute-set>

</xsl:stylesheet>
