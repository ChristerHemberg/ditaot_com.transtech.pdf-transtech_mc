﻿<?xml version='1.0'?>
<xsl:stylesheet xmlns:xsl="http://www.w3.org/1999/XSL/Transform"
    xmlns:fo="http://www.w3.org/1999/XSL/Format" version="2.0">

    <xsl:attribute-set name="note" use-attribute-sets="common.block">
        <!--   avdh:     <xsl:attribute name="keep-together.within-page">100</xsl:attribute>-->
    </xsl:attribute-set>

    <xsl:attribute-set name="hazard__table" use-attribute-sets="common.block">
        <xsl:attribute name="keep-together.within-page">always</xsl:attribute>
        <xsl:attribute name="border">solid 0.75pt black</xsl:attribute>
        <xsl:attribute name="space-after">3mm</xsl:attribute>
    </xsl:attribute-set>

    <xsl:attribute-set name="hazard__image__column">
        <xsl:attribute name="column-number">1</xsl:attribute>
        <!--        <xsl:attribute name="column-width">18mm</xsl:attribute> -->
    </xsl:attribute-set>

    <xsl:attribute-set name="hazard__text__column">
        <xsl:attribute name="column-number">2</xsl:attribute>
    </xsl:attribute-set>

    <xsl:attribute-set name="hazard__image__entry">
        <xsl:attribute name="text-align">center</xsl:attribute>
        <xsl:attribute name="padding">10pt</xsl:attribute>
        <xsl:attribute name="start-indent">0pt</xsl:attribute>
        <xsl:attribute name="border-right">solid 0.75pt black</xsl:attribute>
    </xsl:attribute-set>

    <xsl:attribute-set name="hazard__image">
        <!--        <xsl:attribute name="content-width">2.5cm</xsl:attribute>-->
    </xsl:attribute-set>

    <xsl:attribute-set name="hazard__text__entry">
        <xsl:attribute name="padding">10pt</xsl:attribute>
        <xsl:attribute name="font-weight">bold</xsl:attribute>
        <xsl:attribute name="start-indent">0pt</xsl:attribute>
    </xsl:attribute-set>

</xsl:stylesheet>
